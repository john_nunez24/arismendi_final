var app = angular.module('taquillApp');

app.controller('ctrTasa', function(facGlobal,$scope,$filter,$mdToast,$location, $state, localStorageService, srvRestApi){
  $scope.buscarContri = true
  $scope.persona = {};
  $scope.buscar = {};
  $scope.tasaAdministrativa = {};
  $scope.persona.correo = "";
  $scope.persona.telefono = "";
  $scope.permitirGuardar = true;
  $scope.tasasAplicadas = [];
  $scope.tasas = localStorageService.get('lsTasasAdministrativas');
  $scope.editFactor = true;
  $scope.salarioActual = facGlobal.getSalarioActual();

  srvRestApi.getUniTributaria()
  .then(function(response){
    $scope.uTributaria = response.data;
  },function(response){
    console.log('no se puedo obtener unidad tributaria');
  });



  //------------------------------------------------------------------------------
  $scope.agregarContribuyente = function(){
    $scope.persona = {};
    $scope.crearContri = true;
    $scope.buscarContri = false;
    $scope.mostrarContri = false;

  }
  //------------------------------------------------------------------------------
  $scope.cerrarFormulario = function(){
    $scope.crearContri = false;
    $scope.buscarContri = true;
    $scope.mostrarContri = false;

  }
  //------------------------------------------------------------------------------
  $scope.mostrarFicha = function(){
    $scope.crearContri = false;
    $scope.buscarContri = false;
    $scope.mostrarContri = true;

  }
  //------------------------------------------------------------------------------
  $scope.buscarPersona = function(){
    $scope.buscar.id="";
    $scope.buscarContri1 = true;
    console.log($scope.buscar.rif);
    if($scope.buscar.rif != ""){
      srvRestApi.findPersonaOnly($scope.buscar)
      .then(function(response){

        $scope.selContribuyente = response.data;
        console.log($scope.selContribuyente);
        $scope.mostrarFicha();
      },function(response){
        console.log(response.data.error);
        alert(response.data.error);
      });
    }
  }

  //------------------------------------------------------------------------------
  $scope.guardarContribuyente = function(){

    if($scope.persona.correo == undefined)
     $scope.persona.correo = "";
    if($scope.persona.telefono == undefined)
     $scope.persona.telefono = "";
    var datac = {};

    datac['persona'] = $scope.persona;
    datac['usuario'] = localStorageService.get('lsUsuario');


    srvRestApi.nuevoContribuyente(datac)
    .then(function(response){
      $scope.mostrarFicha();
      $scope.selContribuyente = response.data;
    },function(response){

    });
  }
  //------------------john nuñez -----------------------------------------------
  $scope.selObjPersona = function(per){
    $scope.mostrarContri = true;
    $scope.buscarContri = false;
    $scope.persona = per;
    console.log($scope.persona);
  }
  //------------------------------------------------------------------------------
  $scope.guardarRecibo = function(){
    var data = {};
    data.persona = $scope.persona;
    data.usuario =  localStorageService.get('lsUsuario');
    data.lineas = $scope.tasasAplicadas;
    console.log(data);
    srvRestApi.guardarReciboTasa(data)
    .then(function(response){
      alert('La orden de cobro fue creada con exito. ');
      $scope.buscarContri = true
      $scope.persona = {};
      $scope.buscar = {};
      $scope.permitirGuardar = true;
      $scope.tasasAplicadas = [];
      $scope.cerrarFormulario();
      $scope.mostrarContri = false;


    },function(response){
      alert('No se pudo guardar la orden de cobro, comuniquese con el administrador y reporte la insidencia, 1510')
    });


  }
  //------------------------------------------------------------------------------
  $scope.calcularTasa = function(){

    var tasa = $scope.tasas.filter(function(tasa){
      if(tasa.id == $scope.selTasa)
      return tasa;
    });

    $scope.tasaAdministrativa = tasa[0];
    console.log(tasa[0]);
    $scope.subtotal = (tasa[0].salario/100)*$scope.salarioActual.base;
    console.log($scope.subtotal);
    $scope.showAyuda;
    $scope.factor = 1;
    if(tasa[0].tipo == 'Factor'){
      $scope.showAyuda();
      $scope.editFactor = false;
    }
    $scope.totalizarGlobales();
  }
  //------------------------------------------------------------------------------
  $scope.cambiarFactor = function(){
    $scope.subtotal = $scope.factor*($scope.salarioActual.base*($scope.selTasa.salario/100));
  }
  //----------------------------------------------------------------------------
  $scope.agregarTasa = function(){

    if($scope.tasasAplicadas.length == 4)
    {
      alert('Sólo puede incluir cuatro (4) Tasas Administrativas en un recibo')
    }
    else {
      $scope.permitirGuardar = false;
      $scope.factor = 1;
      var existeTasa = false;
      $scope.tasasAplicadas.forEach(function(tasa){
        if(tasa['id']== $scope.selTasa)
        existeTasa = true;
      });



      if(!existeTasa)
      {
        $scope.editFactor = true;
        $scope.tasaAdministrativa.subtotal = $scope.subtotal*1;
        $scope.tasaAdministrativa.factor = $scope.factor;
        $scope.tasaAdministrativa.tipo = 'Tasa';
        $scope.tasasAplicadas.push($scope.tasaAdministrativa);
      }
      $scope.totalizarGlobales();
    }

  }

  //----------------------------------------------------------------------------

  $scope.eliminarTasa = function(tasaBorrar){
    var temTasa = [];
    $scope.tasasAplicadas.forEach(function(tasa){
      if(tasa['id']!= tasaBorrar['id'])
      temTasa.push(tasa);
    });

    $scope.tasasAplicadas = temTasa;
    $scope.totalizarGlobales();
    if($scope.tasasAplicadas.length == 0)
    $scope.permitirGuardar = true;

  }

  //----------------------------------------------------------------------------

  $scope.totalizarGlobales = function(){
    $scope.totalPagar = 0;
    $scope.tasasAplicadas.forEach(function(tasa){

      $scope.totalPagar = $scope.totalPagar*1 + tasa['subtotal']*1;

    });
  }

  //------------------------------------------------------------------------------
  $scope.showAyuda = function() {


    $mdToast.show(
      $mdToast.simple()
      .textContent('Esta tasa administrativa puede multiplicarla por un Factor según el caso')
      .position('bottom right' )
      .hideDelay(5000)
    );
  };




});
