var app = angular.module('taquillApp' );

app.controller('ctrReporte', function($scope, $timeout,$window,$state, localStorageService, srvRestApi, $filter){

  $scope.total = 100;
  $scope.buscando = false;
  $scope.currentPage = 1;
  $scope.step = 5;
  $scope.userU = localStorageService.get('lsUsuario');
  $scope.date = new Date();
  $scope.status = ['Pagada','Pendiente','Anulada'];
  $scope.reporte = localStorageService.get('lsReporte');
  $scope.tipos = ['Tasa','test','Declaracion','Catastro','CatastroManual','Ingieneria','Publicidad','Item','ReciboManual'];
  $scope.reciboTipos = {};
  $scope.arrTotalXtipo = [];

        /* $scope.arrTotalXtipo.Tasa = {};
        $scope.arrTotalXtipo.test = {};
        $scope.arrTotalXtipo.Declaracion = {};
        $scope.arrTotalXtipo.Catastro = {};
        $scope.arrTotalXtipo.CatastroManual = {};
        $scope.arrTotalXtipo.Ingieneria = {};
        $scope.arrTotalXtipo.Publicidad = {};
        $scope.arrTotalXtipo.Item = {};
        $scope.arrTotalXtipo.ReciboManual = {};
*/
        console.log($scope.arrTotalXtipo);
    $scope.colorTable = function(index){
    return (index%2)?true:false;
  }



  $scope.navigateTo = function(goTo){
    localStorageService.set('lsReporte',goTo);
    $state.go(goTo);

  }




  $scope.buscarRecibos = function(data){
    $scope.tipos.forEach(function (tipo){
          $scope.arrTotalXtipo[tipo] = {};
        $scope.arrTotalXtipo[tipo].totalPagar = 0;
        $scope.arrTotalXtipo[tipo].totalInteres = 0;
        $scope.arrTotalXtipo[tipo].totalMulta = 0;
        $scope.arrTotalXtipo[tipo].totalDescuento = 0;
    });
    $scope.buscando = true;
    console.log(data);
    srvRestApi.findRecibos(data)
    .then(function(response){
      $scope.buscando = false;
      $scope.totalPagar = 0;
      $scope.totalInteres = 0;
      $scope.totalMulta = 0;
      $scope.totalDescuento = 0;

      $scope.recibos = response.data.data;
      //$scope.reciboTipos = $scope.recibos;
      console.log($scope.recibos);
      $scope.recibos.forEach(function(recibo){



                $scope.arrTotalXtipo[recibo.tipo].totalPagar = ($scope.arrTotalXtipo[recibo.tipo].totalPagar)*1 + recibo['totalPagar']*1;
                $scope.arrTotalXtipo[recibo.tipo].totalInteres = $scope.arrTotalXtipo[recibo.tipo].totalInteres*1 + recibo['totalInteres']*1;
                $scope.arrTotalXtipo[recibo.tipo].totalMulta = $scope.arrTotalXtipo[recibo.tipo].totalMulta*1 + recibo['totalMulta']*1;
                $scope.arrTotalXtipo[recibo.tipo].totalDescuento = $scope.arrTotalXtipo[recibo.tipo].totalDescuento*1 + recibo['totalDescuento']*1;

                $scope.totalPagar = $scope.totalPagar + recibo['totalPagar']*1;
                $scope.totalInteres = $scope.totalInteres + recibo['totalInteres']*1;
                $scope.totalMulta = $scope.totalMulta + recibo['totalMulta']*1;
                $scope.totalDescuento = $scope.totalDescuento + recibo['totalDescuento']*1;
               });


      });


    console.log($scope.arrTotalXtipo);
  }


  $scope.filtrarRecibos = function(tipo){
    var data = {};
    data.id = "";
    data.paginate = "10000";
    data.status = $scope.selStatus;
    data.fecha = $scope.fecha;
    data.tipoReporte = tipo;

    $scope.buscarRecibos(data);

  }

  $scope.reImprimir = function(data){
  console.log(data);
  localStorageService.set('lsDeclaracion',data);
    window.open('temp/declarar/reciboImpreso.html#?data='+data,'_blank','toolbar=no,scrollbars=yes,resizable=yes,width=800,height=400')

  /*  srvRestApi.reImprimir(data)
    .then(function(response){
      localStorageService.set('lsDeclaracion',response.data);
      $state.go('recibo');
    });*/
  }



  $scope.printIt2 = function(){
    var table = document.getElementById('printArea').innerHTML;
    var myWindow = $window.open('', '', 'width=800, height=600');
    myWindow.document.write(table);
    myWindow.print();



  };


   ///Reporte catastro totales

   $scope.totalCatastro = function(){

     $scope.buscando = true ;
     srvRestApi.totalCatastro({}).then(function(response){
           $scope.totales = response.data;
           $scope.labels = ['Total', 'Anterior', 'Nuevos', 'Actualizado', 'pagado 2018'];


            $scope.data = [
              [$scope.totales.totales, $scope.totales.anteriores, $scope.totales.nuevas,
                 $scope.totales.completas, $scope.totales.pagadas]

            ];
              $scope.buscando = false ;
     });
   }





  //busqueda inicical segun el tipo de reporte
  if($scope.reporte != undefined){
    var data = {};
    if($scope.reporte == 'totalCatastro'){
      $scope.totalCatastro();
    }else{


      data.id = "";
      data.paginate = "15";
      data.status = "";
      $scope.buscarRecibos(data);
    }

    }

  });
