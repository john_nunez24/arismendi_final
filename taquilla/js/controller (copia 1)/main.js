var app = angular.module('taquillApp' , ['ngMaterial', 'chart.js','ja.qr', 'ngMessages', 'fc.paging' ,'ui.router', 'ngAnimate','ngResource','LocalStorageModule']);

app.config(function($stateProvider,localStorageServiceProvider) {
  //$locationProvider.html5mode(true);
  localStorageServiceProvider
  .setPrefix('lsPosApp')
  .setStorageType('sessionStorage')
  .setNotify(true, true);
  var buscar = {
    name: 'declaracion',
    url: '/declaracion',
    templateUrl: 'temp/declarar/buscar.html'
  }

  var login = {
    name: 'login',
    url: '/login',
    templateUrl: 'temp/usuario/login.html'
  }

  var cambiar = {
    name: 'cambiar',
    url: '/cambiar',
    templateUrl: 'temp/usuario/cambiar.html'
  }

  var registrarGestion = {
    name: 'registrarGestion',
    url: '/registrarGestion',
    templateUrl: 'temp/declarar/registrarGestion.html'
  }
  var registrarPago = {
    name: 'registrarPago',
    url: '/registrarPago',
    templateUrl: 'temp/declarar/registrarPago.html'
  }
  var recibo = {
    name: 'recibo',
    url: '/recibo',
    templateUrl: 'temp/declarar/recibo.html'
  }
  var reporte = {
    name: 'reporte',
    url: '/reporte',
    templateUrl: 'temp/reporte/reporte.html'
  }
  var mostrarRecibo = {
    name: 'mostrarRecibo',
    url: '/mostrar/recibo',
    templateUrl: 'temp/declarar/mostrarRecibos.html'
  }
// ----------john nuñez---------------------
var generarRecibo = {
  name: 'generarRecibo',
  url: '/generar/recibo',
  templateUrl: 'temp/vehiculo/generarRecibos.html'
}
//-----------------john nuñez--------------------------

  var tasas = {
    name: 'tasas',
    url: '/registar/tasa',
    templateUrl: 'temp/tasa/index.html'
  }

  var ingTasas = {
    name: 'ingTasas',
    url: '/registar/ingTasas',
    templateUrl: 'temp/ingieneria/index.html'
  }

  var inicio = {
    name: 'inicio',
    url: '/inicio',
    templateUrl: ''
  }

  var catastro = {
    name: 'catastro',
    url: 'catastro',
    templateUrl: 'temp/catastro/buscar.html'
  }

  var ficha = {
    name: 'ficha',
    url: 'ficha/catastral',
    templateUrl: 'temp/catastro/index.html'
  }

  var publicidad = {
    name: 'publicidad',
    url: 'publicidad',
    templateUrl: 'temp/publicidad/buscar.html'
  }

  var verPublicidad = {
    name: 'verPublicidad',
    url: 'verPublicidad',
    templateUrl: 'temp/publicidad/index.html'
  }

  var reportedia = {
    name: 'reportedia',
    url: '/reporte/dia',
    templateUrl: 'temp/reporte/index.html'
  }
  var reporteTipos = {
    name: 'reporteTipos',
    url: '/reporte/tipos',
    templateUrl: 'temp/reporte/reportesXtipos.html'
  }

  var reportemes = {
    name: 'reportemes',
    url: '/reporte/mensual',
    templateUrl: 'temp/reporte/mensual.html'
  }

  var grafico = {
    name: 'grafico',
    url: '/reporte/grafico',
    templateUrl: 'temp/reporte/graficos.html'
  }

  var comparar = {
    name: 'comparar',
    url: '/reporte/comparar',
    templateUrl: 'temp/reporte/comparar.html'
  }

  var totalCatastro = {
    name: 'totalCatastro',
    url: '/reporte/total/catastro',
    templateUrl: 'temp/reporte/catastro.html'
  }

  var rusuario = {
    name: 'rusuario',
    url: '/reporte/usuario',
    templateUrl: 'temp/reporte/usuario.html'
  }

  var rcontribuyente = {
    name: 'rcontribuyente',
    url: '/reporte/contribuyente',
    templateUrl: 'temp/reporte/contribuyente.html'
  }

  var ralerta = {
    name: 'ralerta',
    url: '/reporte/alerta',
    templateUrl: 'temp/reporte/alerta.html'
  }


  var cTributaria = {
    name: 'cTributaria',
    url: '/configuracion/unidad/tributaria',
    templateUrl: 'temp/configuracion/tributaria.html'
  }


  var cInteres = {
    name: 'cInteres',
    url: '/configuracion/tasa/interes',
    templateUrl: 'temp/configuracion/interes.html'
  }

  var sUsuario = {
    name: 'sUsuario',
    url: '/usuario/ver',
    templateUrl: 'temp/configuracion/usuario.html'
  }

  var configuracion = {
    name: 'configuracion',
    url: '/configuracion',
    templateUrl: 'temp/configuracion/index.html'
  }

  var newCatastro = {
    name: 'newCatastro',
    url: '/catastro/nuevo',
    templateUrl: 'temp/catastro/new.html'
  }
  /////////////////////////codigo john nuñez//////////////////////////
  var newDecla = {
    name: 'newDecla',
    url: '/declarar/nuevo',
    templateUrl: 'temp/declarar/new.html'

  }
  /////////////////////////codigo john nuñez//////////////////////////
  var fichaCatastral = {
    name: 'fichaCatastral',
    url: '/catastro/ficha',
    templateUrl: 'temp/catastro/fichaCatastral.html'

  }
/////////////////////////codigo john nuñez//////////////////////////
var contribuyente = {
  name: 'contribuyente',
  url: '/contribuyente/buscar',
  templateUrl: 'temp/contribuyente/buscar.html'

}
////////////////////////codigo john nuñez//////////////////////////
var mostrarContribuyente = {
  name: 'mostrarContribuyente',
  url: '/registar/mostrarContribuyente',
  templateUrl: 'temp/ingieneria/mostrarContribuyente.html'

}
/////////////////////////codigo john nuñez//////////////////////////
  var editCatastro = {
    name: 'editCatastro',
    url: '/catastro/edit',
    templateUrl: 'temp/catastro/edit.html'
  }
  var cobroCatastro = {
    name: 'cobroCatastro',
    url: '/catastro/cobro|',
    templateUrl: 'temp/catastro/cobro.html'
  }
  var licor = {
    name: 'licor',
    url: 'licor',
    templateUrl: 'temp/licor/buscar.html'
  }
//------------------------------john nuñez-------------------------------------
var vehiculo = {
  name: 'vehiculo',
  url: 'vehiculo',
  templateUrl: 'temp/vehiculo/buscar.html'
}
//------------------------------john nuñez-------------------------------------
var agrVehiculo = {
  name: 'agrVehiculo',
  url: 'nuevoVehiculo',
  templateUrl: 'temp/vehiculo/agregarVehiculo.html'
}
//----------------------------------john nuñez---------------------------------

  var verLicor = {
    name: 'verLicor',
    url: 'verLicor',
    templateUrl: 'temp/licor/index.html'
  }

  var verConfiguracion = {
    name: 'verConfiguracion',
    url: 'verConfiguracion',
    templateUrl: 'temp/configuracion/index.html'
  }

  $stateProvider.state(verConfiguracion);
  $stateProvider.state(totalCatastro);
  $stateProvider.state(licor);
  $stateProvider.state(vehiculo);// -----john nuñez
  $stateProvider.state(verLicor);
  $stateProvider.state(editCatastro);
  $stateProvider.state(newCatastro);
  $stateProvider.state(newDecla);/// john nuñez....
  $stateProvider.state(agrVehiculo);/// john nuñez
  $stateProvider.state(fichaCatastral);/// john nuñez....
  $stateProvider.state(mostrarContribuyente);/// john nuñez....
  $stateProvider.state(contribuyente);/// john nuñez....
  $stateProvider.state(cobroCatastro);
  $stateProvider.state(sUsuario);
  $stateProvider.state(configuracion);
  $stateProvider.state(cTributaria);
  $stateProvider.state(cInteres);
  $stateProvider.state(ralerta);
  $stateProvider.state(rcontribuyente);
  $stateProvider.state(rusuario);
  $stateProvider.state(reportemes);
  $stateProvider.state(grafico);
  $stateProvider.state(comparar);
  $stateProvider.state(reportedia);
  $stateProvider.state(reporteTipos);
  $stateProvider.state(verPublicidad);
  $stateProvider.state(publicidad);
  $stateProvider.state(ficha);
  $stateProvider.state(cambiar);
  $stateProvider.state(catastro);
  $stateProvider.state(inicio);
  $stateProvider.state(buscar);
  $stateProvider.state(login);
  $stateProvider.state(reporte);
  $stateProvider.state(registrarGestion);
  $stateProvider.state(tasas);
  $stateProvider.state(ingTasas);
  $stateProvider.state(registrarPago);
  $stateProvider.state(recibo);
  $stateProvider.state(mostrarRecibo);
  $stateProvider.state(generarRecibo);
});





  app.directive('numbersOnly', function () {

    return {
      require: 'ngModel',
      link: function (scope, element, attr, ngModelCtrl) {
        function fromUser(text) {
          if (text) {
            var transformedInput = text.replace(/[^0-9]/g, '');

            if (transformedInput !== text) {
              ngModelCtrl.$setViewValue(transformedInput);
              ngModelCtrl.$render();
            }
            return transformedInput;
          }
          return undefined;
        }
        ngModelCtrl.$parsers.push(fromUser);
      }
    };
  });

  app.directive('decimalsOnly', function ($filter) {
    return {
      require: 'ngModel',
      link: function (scope, element, attr, ngModelCtrl) {
        function fromUser(text) {
          if (text) {

            var transformedInput = '';
            var longitud = text.length;
            var ultimo = text[longitud-1];
            var arrayDecimal = text.split('.');

            if (/^[0-9]*$/.test(ultimo)){//si es numero
              if ((arrayDecimal.length == 2)&&(arrayDecimal[1].length > 2))//si ya hay 2 decimales
              transformedInput = text.slice(0,longitud-1);
              else
              transformedInput = text;//solo numeros


            }else{


            if(/^[.]*$/.test(ultimo)) {//si es un punto
              if (arrayDecimal.length > 2){//si ya hay otro punto

              transformedInput = text.slice(0,longitud-1);

              }else{

              transformedInput = text;}

            }
          else{//si es cualquier caracter distinto a numero y punto
               transformedInput = text.replace(ultimo, '');
            }}


            transformedInput = transformedInput.replace(',','');




            if (transformedInput !== text) {


              ngModelCtrl.$setViewValue(transformedInput);
              ngModelCtrl.$render();
            }

            return transformedInput;
          }
          return undefined;
        }
        ngModelCtrl.$parsers.push(fromUser);
      }
    };
  });


  app.directive('numericOnly2', function(){
      return {

          require: 'ngModel',
          link: function(scope, element, attrs, modelCtrl) {

              var regex = /^[1-9]\d*(((.\d{3}){1})?(\,\d{0,2})?)$/;
              modelCtrl.$parsers.push(function (inputValue) {

                  var transformedInput = inputValue;
                  if (regex.test(transformedInput)) {


                      modelCtrl.$setViewValue(transformedInput);
                      modelCtrl.$render();
                      return transformedInput;
                  } else {


                      transformedInput = transformedInput.substr(0, transformedInput.length-1);
                      modelCtrl.$setViewValue(transformedInput);
                      modelCtrl.$render();
                      return transformedInput;
                  }
              });
          }
      };
  });



  app.directive('tasaOnly', function () {
    return {
      require: 'ngModel',
      link: function (scope, element, attr, ngModelCtrl) {
        function fromUser(text) {
          if (text) {
            var transformedInput = '';
            var longitud = text.length;
            var ultimo = text[longitud-1];
            var arrayDecimal = text.split('.');

            if (/^[0-9]*$/.test(ultimo)){//si es numero
              if ((arrayDecimal.length == 2)&&(arrayDecimal[1].length > 2))//si ya hay 2 decimales
              transformedInput = text.slice(0,longitud-1);
              else
              transformedInput = text;//solo numeros
            }else
            if (/^[.]*$/.test(ultimo)) {//si es un punto
              if (arrayDecimal.length > 2)//si ya hay otro punto
              transformedInput = text.slice(0,longitud-1);
              else
              transformedInput = text;

            }else//si es cualquier caracter distinto a numero y punto
            transformedInput = text.replace(ultimo, '');



            if (transformedInput !== text) {
              ngModelCtrl.$setViewValue(transformedInput);
              ngModelCtrl.$render();
            }

            return transformedInput;
          }
          return undefined;
        }
        ngModelCtrl.$parsers.push(fromUser);
      }
    };
  });
