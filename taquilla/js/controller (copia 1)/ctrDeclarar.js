var app = angular.module('taquillApp' );




app.controller('ctrDeclarar', function($scope, $rootScope,$mdDialog, $state, $filter, localStorageService, srvUtils, srvRestApi, $filter,facGlobal){
  $scope.patente    =    localStorageService.get('lspatente');
  $scope.usuarioSys =    localStorageService.get('lsUsuario');
  $scope.serverDate =    localStorageService.get('lsDate');
  $scope.recibo =        localStorageService.get('lsRecibo');
  $scope.diaHabil =      localStorageService.get('lsDiaHabil');
  $scope.actualPayDate = localStorageService.get('lsActualPayDate');
  $scope.globalParams =  localStorageService.get('lsGlobalParams');
  $scope.declaracion = {};
  $scope.mesBloqueado = true;
  $scope.bloquearAgregar = true;
  $scope.bloquearVenta = true;
  $scope.utributaria = localStorageService.get('lsUtributariaActiva');
  $scope.newPatente = {};
$scope.selActividad = "";
  $scope.actividades = {};
  $scope.actividades = facGlobal.getActividadEconomica();
  $scope.actividadBase = facGlobal.getActividadBase();
  $scope.preFijos = ['V','J','G','E','P','C'];
  $scope.salarioActual = facGlobal.getSalarioActual();
  $scope.salarioViejo = 18000;
  $scope.newactividad = [];


   

  $scope.monto = [];
  $scope.editRif = false;
  $scope.editTaquilla = false;
  $scope.lineTotalPagar = [];
  $scope.lineTotalSub = [];
  $scope.lineTotalDescuento = [];
  $scope.lineTotalInteres = [];
  $scope.lineTotalMulta = [];
  $scope.actividad = [];



  if(($scope.usuarioSys.nivel=="gestion")||($scope.usuarioSys.nivel=="catastro"))
  $scope.editRif = true;

  if(($scope.usuarioSys.nivel=="taquilla"))
  {
    $scope.editTaquilla = true;

  }
/*
  if(($scope.patente.tipo =="comercio"))
  $scope.editComercio = true;
  if(($scope.patente.tipo =="inmobiliaria"))
  { $scope.editInmobiliaria = true;
    $scope.declaracion.monto = $scope.patente.alicuota;
  }
*/


  //--------------------------------------------------------------------
  $scope.guardarContribuyente = function(){
    var data = {};
    data.patente = $scope.patente;
    srvRestApi.updatePersonaPatente(data)
    .then(function(response){

      alert('Se actualizaron los datos del contribuyente');
      localStorageService.set('lspatente',response.data.patente);
    },
    function(response){
      alert('No se pudo Actualizar los datos del contribuyente');
    });
  }



  var baseMeses = [{value:'1',label:'Enero'},
  {value:'2',label:'Febrero'},
  {value:'3',label:'Marzo'},
  {value:'4',label:'Abril'},
  {value:'5',label:'Mayo'},
  {value:'6',label:'Junio'},
  {value:'7',label:'Julio'},
  {value:'8',label:'Agosto'},
  {value:'9',label:'Septiembre'},
  {value:'10',label:'Octubre'},
  {value:'11',label:'Noviembre'},
  {value:'12',label:'Diciembre'}];

  $scope.anos = [];


  for (var i = 2017; i <= $scope.serverDate.ano; i++) {
    $scope.anos.push(i);
  }
  //------------------------------------------------------------------------------
  $scope.getNombreActividad = function(acti_id){
    var actividad = $scope.actividades
    .filter(function(acti){
      if(acti.patente_id == acti_id){
        return acti;
      }
    });
    return actividad[0].nombre;
  }
  //----------------------codigo john nuñez--------------------------------------------------------
  $scope.formCrearPropiedad = function(){
    localStorageService.set('lspatente',undefined);
    //localStorageService.set('lsFicha',undefined);

    $state.go('newDecla');
  }
  console.log('*********************************************************************************');
  //---------------------codigo john nuñez.............
 $scope.agregarNewActividad = function(){
   console.log('enrique esta aqui');
   //console.log($scope.actividades);

 //console.log( $scope.newactividad);
 console.log( $scope.selActividad1);
var i = 0;
var sw = true;
       while ((i < $scope.newactividad.length) && (sw)) {
         console.log($scope.newactividad[i].id+' == '+ $scope.selActividad1.id);
          if ($scope.newactividad[i].id == $scope.selActividad1.id){
              sw = false;
          }i++;
        }
            if (sw){
              $scope.newactividad.push($scope.selActividad1);
            }
              console.log( $scope.newactividad);
        };
//------------------------codigo john nuñez---------------------




  //---------------------codigo john nuñez.............
$scope.eliminarActividad1 = function(a){
  var x = a.id;
  $scope.newactividad = $scope.newactividad.filter(function(a) {
    if (a.id != x)
      return a;
    }
  )};



  //---------------------codigo john nuñez.............
  //codigo modificado por john------------
    $scope.eliminarActividad2 = function(){
       var data = {};


       data.actividad = $rootScope.rsEliminarActividad;
       data.comentario = $scope.comentario;
       data.usuario = localStorageService.get('lsUsuario');
       console.log($rootScope.rsEliminarActividad);

      srvRestApi.eliminarActividad(data)
          .then(function(response){

            $scope.actividadPatentes = data.actividad;
            // $scope.actividadPatentes =  srvUtils.getActividadesEconomicas($rootScope.rsEliminarActividad.actividadPatente.patente_id);

            },function(response){

           });
           $scope.cancel();
    }
  //codigo modificado por john------------
  $scope.crearNewPatente = function(){

    var data = {};
    data.patente = $scope.newPatente;
    data.comentario = $scope.comentario;
    data.usuario = localStorageService.get('lsUsuario');
    data.actividades = $scope.newactividad;
    console.log(data.patente);
    if($scope.newPatente.correo == undefined)
    data.patente.correo = "";

    srvRestApi.crearNewPatente(data)
    .then(function(response){
      alert('Se Actualizaron  los datos correctamente');
      $scope.patente = {};
      localStorageService.set('lspatente',undefined);
      $state.go('buscar');
      //$state.go('login');

    },function(response){

      alert(response.data.error);

    });
  }
  //---------------------codigo john nuñez.............

  $scope.mostrarAgregarActividad1 = function(){
    $scope.swAgregarActividad = true;
  }
  //---------------------codigo john nuñez.............
  //*-----------------------------------------------------------------------------
  $scope.selAno = function(){

    $scope.declaracion.imes = undefined;

    if($scope.declaracion.iano == $scope.serverDate.ano){

      $scope.meses = baseMeses.filter(function(mes){
        if((mes.value*1) < ($scope.serverDate.mes*1))
        return mes;
      });

    }else{
      $scope.meses = baseMeses;
    }
    $scope.mesBloqueado = false;
  }

  //-----------------------------------------------------------------------------
  if(($scope.recibo != undefined)&&($scope.recibo != null)){
    $scope.declaracion = $scope.recibo.lineas[0];
    var imes = $scope.declaracion.imes;
    $scope.selAno();
    $scope.declaracion.imes = imes;
    $scope.bloquearVenta = false;

  }

  //---------------------------------------------------------------------------

  $scope.getBaseImponible = function(linea_id){
    var decla = [];
    decla[0] = undefined;
    if(($scope.recibo != undefined)&&($scope.recibo != null)){
      var decla = $scope.recibo.lineas.filter(function(dec){

        if((dec.actividad_id == actividad_id)&&(dec.id == recibo_id)){
          return dec;
        }
      });}

      return decla[0].base;
    }
    //---------------------------------------------------------------------------

    $scope.getBaseImponible = function(linea_id){
      var decla = [];
      decla[0] = undefined;
      if(($scope.recibo != undefined)&&($scope.recibo != null)){
        var decla = $scope.recibo.lineas.filter(function(dec){

          if((dec.id == linea_id)){
            return dec;
          }
        });}
        if(decla[0] == undefined)
        return 0
        else
        return decla[0].base;
      }
      //---------------------------------------------------------------------------

      $scope.getSubTotal = function(linea_id){
        var decla = [];
        decla[0] = undefined;
        if(($scope.recibo != undefined)&&($scope.recibo != null)){
          var decla = $scope.recibo.lineas.filter(function(dec){
            if((dec.id == linea_id)){
              return dec;
            }
          });}
          if(decla[0] == undefined)
          return 0
          else
          return decla[0].subTotal;
        }
        //---------------------------------------------------------------------------

        $scope.getDescuento = function(linea_id){
          var decla = [];
          decla[0] = undefined;
          if(($scope.recibo != undefined)&&($scope.recibo != null)){
            var decla = $scope.recibo.lineas.filter(function(dec){

              if((dec.id == linea_id)){
                return dec;
              }
            });}
            if(decla[0] == undefined)
            return 0
            else
            return decla[0].descuento;
          }

          //---------------------------------------------------------------------------

          $scope.getMulta = function(linea_id){
            var decla = [];
            decla[0] = undefined;
            if(($scope.recibo != undefined)&&($scope.recibo != null)){
              var decla = $scope.recibo.lineas.filter(function(dec){

                if((dec.id == linea_id)){
                  return dec;
                }
              });}
              if(decla[0] == undefined)
              return 0
              else
              return decla[0].multa;
            }

            //---------------------------------------------------------------------------

            $scope.getDateCreatedLine= function(linea_id){
              var decla = [];
              decla[0] = undefined;
              if(($scope.recibo != undefined)&&($scope.recibo != null)){
                var decla = $scope.recibo.lineas.filter(function(dec){

                  if((dec.id == linea_id)){
                    return dec;
                  }
                });}
                if(decla[0] == undefined)
                return 0
                else
                return decla[0].multa;
              }

              //---------------------------------------------------------------------------

              $scope.getInteres = function(linea_id){
                var decla = [];
                decla[0] = undefined;
                if(($scope.recibo != undefined)&&($scope.recibo != null)){
                  var decla = $scope.recibo.lineas.filter(function(dec){

                    if((dec.id == linea_id)){
                      return dec;
                    }
                  });}
                  if(decla[0] == undefined)
                  return 0
                  else
                  return decla[0].interes;
                }

                //---------------------------------------------------------------------------

                $scope.getTotalPagar = function(linea_id){
                  var decla = [];
                  decla[0] = undefined;
                  if(($scope.recibo != undefined)&&($scope.recibo != null)){
                    decla = $scope.recibo.lineas.filter(function(dec){
                      if((dec.id == linea_id)){
                        return dec;
                      }
                    });
                  }

                  if(decla[0] == undefined)
                  return 0
                  else
                  return decla[0].total;
                }
                //------------------------------------------------------------------------------




                $scope.calcularMulta2 = function(){

                  if($scope.declaracion.hano == '2017')
                  return $scope.totalInteres = ($scope.declaracion.monto )*(0.5);
                  else {
                    return 0;
                  }
                };

                $scope.calcularDescuento2 = function(){

                  if(($scope.declaracion.hano == '2018')&&($scope.declaracion.iano == '2018')&&($scope.declaracion.hmes == '1'))
                  return $scope.totalDescuento = ($scope.declaracion.monto )*(0.05);
                  else {
                    return 0;
                  }
                };

                $scope.calcularInteres2 = function(){

                  if(($scope.declaracion.iano == '2017'))
                  return 0;
                  else {
                    return 0;
                  }
                };
                $scope.calcularMulta = function(){

                  if($scope.declaracion.hano == '2017'){
                    if($scope.patente.tipo == 'comercio')
                    return $scope.totalInteres = ($scope.declaracion.monto * ($scope.patente.alicuota/100))*(0.5);
                    else
                    return $scope.totalInteres = $scope.declaracion.monto *(0.5);
                  }
                  else {
                    return 0;
                  }
                };
                //-------------------------------------------------------------
                $scope.calcularMulta = function(id,base){

                  var pMulta = 0;
                  var diasTrampa = 1;
                  console.log($scope.declaracion.iano +" == "+ $scope.actualPayDate['ano']);
                  console.log($scope.declaracion.imes+ " == 1");
                  console.log($scope.serverDate['dia'] +" >= 15");

                  if ( ($scope.declaracion.iano < $scope.actualPayDate['ano'])||
                
                  ($scope.serverDate['dia'] >= 18)|| 
                       (($scope.declaracion.imes < $scope.serverDate['mes'])&&
                         ($scope.diaHabil > $scope.globalParams['maxHabil']))
                        || 
                       ($scope.declaracion.imes <= $scope.serverDate['mes']-2) 
                       )  {
                    pMulta = 50;

                  }

// esto es por la PANDEMIA no se cobrara multa OJOOOO PANDEMIA CORONAVIRUS
 pMulta = 0;

                 if( ( ($scope.declaracion.iano == $scope.actualPayDate['ano'])&&
                      (($scope.declaracion.imes == 10) ||($scope.declaracion.imes == 9) ||($scope.declaracion.imes == 8) ||($scope.declaracion.imes == 7) ||($scope.declaracion.imes == 6) ||($scope.declaracion.imes == 5) || ($scope.declaracion.imes <= 2))
                       
                      )
                      ||
                      ($scope.declaracion.iano < $scope.actualPayDate['ano']) ) {
                    pMulta = 50;

                  }
// HASTA AQUI LA PANDEMIA
                  console.log("MULTA____________________");
                  console.log(pMulta);
                  console.log(base);
                  console.log(base * (pMulta/100));
                  return base * (pMulta/100);
            };

            //--------------------------------------------------------------------
            $scope.actividadById = function(id){

              var act = $scope.actividadBase.filter(function(actividad){
                if (actividad.actividad_id == id)
                return actividad;
              });


              return act[0];
            }
            //------------------------------------------------------
            $scope.calcularDescuento = function(){

              if( ($scope.diaHabil <= $scope.globalParams['maxHabil'])&&
              ($scope.declaracion.iano == $scope.actualPayDate['ano'])&&
              ($scope.declaracion.imes == $scope.actualPayDate['mes']))
              {
                if($scope.patente.tipo == 'comercio')
                return $scope.totalDescuento = ($scope.declaracion.monto * ($scope.patente.alicuota/100))*(0.05);
                else
                return $scope.totalDescuento = 0;//$scope.declaracion.monto*(0.05);

              }
              else {
                return 0;
              }
            };

            //------------------------------------------------------
            $scope.calcularDescuento = function(id,base){

              if( ($scope.diaHabil <= $scope.globalParams['maxHabil'])&&
              ($scope.declaracion.iano == $scope.actualPayDate['ano'])&&
              ($scope.declaracion.imes == $scope.actualPayDate['mes']))
              {
                return  base * (0.05);//CUARENTENA OJO CUARENTENA PANDEMIA
              }
              else {
                return 0;
              }
            };

            $scope.calcularInteres = function(id,base){
           console.log($scope.declaracion.iano)
           console.log($scope.actualPayDate['ano']);
           console.log($scope.declaracion.imes)
           console.log($scope.actualPayDate['mes']);
              if ((($scope.declaracion.iano == 2018 )&&($scope.declaracion.imes == 12))||(($scope.declaracion.iano != $scope.actualPayDate['ano'])&&($scope.declaracion.imes != $scope.actualPayDate['mes']))||
              (($scope.declaracion.iano == $scope.actualPayDate['ano'])&&($scope.declaracion.imes < $scope.actualPayDate['mes'])))
              return srvUtils.calcularIntereses($scope.declaracion.imes,
                $scope.declaracion.iano,
                $scope.actualPayDate['mes'],
                $scope.actualPayDate['ano'],
                base );
                else {
                  return 0;
                }
              };


             $scope.cambia = function(_actividad){

                $scope.declaracion.hmes = $scope.declaracion.imes;
                $scope.declaracion.hano = $scope.declaracion.iano;
                var id = _actividad.id;
                var actSel = $scope.actividadById(_actividad.id);
                console.log('1eee');
                console.log(id);
                console.log(_actividad);
                if((($scope.monto[id] != "")&&($scope.monto[id] != undefined)))
                {
                  console.log('2');
                  var montoLinea = $scope.monto[id];

                  montoLinea = montoLinea * 1 ;
                  
                  //console.log('........................');
                  var alicuota = actSel.porcentaje;
                  var minimo = actSel.minimo;

                  var sNuevo = 40000.000000;//TODO: tomas datos de la base de datos JESUS GOMEZ 01/11/2019
                  
                  
                  if(($scope.declaracion.imes>=10)&&($scope.declaracion.iano==2019))
                     sNuevo = 150000.000000;

                  if(($scope.declaracion.imes==12)&&($scope.declaracion.iano==2019))
                     sNuevo = 300000.000000;//$scope.salarioActual.base;

                 if(($scope.declaracion.imes<5)&&($scope.declaracion.iano==2020))
                     sNuevo = 750000.000000;//$scope.salarioActual.base;
                 
                 if(($scope.declaracion.imes>=5)&&($scope.declaracion.iano==2020))
                     sNuevo = 1200000.000000;
                 if(($scope.declaracion.imes>=11)&&($scope.declaracion.iano==2020))
                     sNuevo = 1800000.000000;
console.log('JUNIO 2020');
console.log(sNuevo);

                  var sViejo = $scope.salarioViejo;

                  //Calculo normal
                  var mMinimo = (((sNuevo) * (minimo/100)));


                  // cuando es el mes del cabio de salario y se prorratea
                  if(( $scope.declaracion.imes == 4)&&($scope.declaracion.iano<2020))
                     mMinimo = ((((sNuevo/2)+(sViejo/2)) * (minimo/100)));

                  //cuando es menor al cambio de salario minimo
                  if(( $scope.declaracion.imes < 4)&&($scope.declaracion.iano==2019))
                    mMinimo = (((sViejo) * (minimo/100)));;


                  $scope.bloquearAgregar = false;
                  if($scope.patente.tipo == 'comercio'){

                    $scope.lineTotalSub[id] = montoLinea * (alicuota/100);
                    //minimo tributario

  console.log('subtotal');
  console.log($scope.lineTotalSub[id] );
  console.log(mMinimo);
                    if($scope.lineTotalSub[id] < mMinimo)
                    {
                        //console.log('en la condicion++++++++++++++++++++');
                      $scope.lineTotalSub[id] = mMinimo;
                      $scope.lineTotalDescuento[id] = (0);
                    }
                    $scope.lineTotalDescuento[id] = $scope.calcularDescuento(id,$scope.lineTotalSub[id]);
                    $scope.lineTotalMulta[id] =$scope.calcularMulta(id,$scope.lineTotalSub[id]);
                    $scope.lineTotalInteres[id] = $scope.calcularInteres(id,$scope.lineTotalSub[id]);

                    $scope.lineTotalPagar[id] = $scope.lineTotalSub[id] - $scope.lineTotalDescuento[id] + $scope.lineTotalInteres[id]+$scope.lineTotalMulta[id];
                  }

                }else {
                  $scope.monto[id] = undefined;
                }
                $scope.totalizarGlobales();

              }

              $scope.cambiaFecha = function(){
                if(($scope.declaracion.iano == undefined)||($scope.declaracion.imes == undefined)){
                  $scope.bloquearVenta = true;
                  $scope.bloquearAgregar = true;
                  $scope.monto = [];
                }else{
                  $scope.bloquearVenta = false;

                  $scope.actividadPatentes.forEach(function(acti){
                    $scope.cambia(acti);
                  });
                }
              }

              //------------------------------------------------------------------------------

              $scope.cambiaEdit = function(_actividad){

                $scope.declaracion.hmes = $scope.declaracion.imes;
                $scope.declaracion.hano = $scope.declaracion.iano;
                var id = _actividad.id;
                var actSel = $scope.actividadById(_actividad.actividad_id);

                if((($scope.monto[id] != "")&&($scope.monto[id] != undefined)))
                {

                  var montoLinea = $scope.monto[id];

                  montoLinea = montoLinea * 1 ;

                  console.log('........................');
                  var alicuota = actSel.porcentaje;
                  var minimo = actSel.minimo;


                  $scope.bloquearAgregar = false;
                  if($scope.patente.tipo == 'comercio'){

                    $scope.lineTotalSub[id] = montoLinea * (alicuota/100);
                    //minimo tributario


                    if($scope.lineTotalSub[id]<(4500 * (minimo/100)))
                    {
                      $scope.lineTotalSub[id] = 4500 * (minimo/100);
                      $scope.lineTotalDescuento[id] = (0);
                    }
                    $scope.lineTotalDescuento[id] = $scope.calcularDescuento(id,$scope.lineTotalSub[id]);
                    $scope.lineTotalMulta[id] = 0;//$scope.calcularMulta(id,$scope.lineTotalSub[id]);
                    $scope.lineTotalInteres[id] = $scope.calcularInteres(id,$scope.lineTotalSub[id]);

                    $scope.lineTotalPagar[id] = $scope.lineTotalSub[id] - $scope.lineTotalDescuento[id] + $scope.lineTotalInteres[id]+$scope.lineTotalMulta[id];
                  }

                }else {
                  $scope.monto[id] = undefined;
                }
                $scope.totalizarGlobales();

              }


              $scope.cambiaFechaEdit = function(){
                if(($scope.declaracion.iano == undefined)||($scope.declaracion.imes == undefined)){
                  $scope.bloquearVenta = true;
                  $scope.bloquearAgregar = true;
                  $scope.monto = [];
                }else{
                  $scope.bloquearVenta = false;

                  $scope.recibo.lineas.forEach(function(linea){
                    $scope.cambiaEdit(linea);
                  });
                }
              }

              $scope.totalizarGlobales = function(){
                $scope.totalPagar = 0;
                $scope.totalSub = 0;
                $scope.totalDescuento = 0;
                $scope.totalInteres = 0;
                $scope.totalMulta = 0;

                $scope.actividadPatentes.forEach(function(acti){

                  if($scope.monto[acti.id] != undefined ){
                    $scope.totalPagar = $scope.totalPagar + $scope.lineTotalPagar[acti.id];
                    $scope.totalSub = $scope.totalSub + $scope.lineTotalSub[acti.id] ;
                    $scope.totalDescuento = $scope.totalDescuento + $scope.lineTotalDescuento[acti.id];
                    $scope.totalInteres = $scope.totalInteres + $scope.lineTotalInteres[acti.id];
                    $scope.totalMulta = $scope.totalMulta + $scope.lineTotalMulta[acti.id];
                  }
                });


              }

              $scope.cambia2 = function(){

                if((($scope.declaracion.monto != "")&&($scope.declaracion.monto != undefined)))
                {
                  $scope.totalMulta = $scope.calcularMulta2();
                  $scope.totalInteres = $scope.calcularInteres2();
                  $scope.totalDescuento = $scope.calcularDescuento2();
                  $scope.totalSub = $scope.declaracion.monto
                  $scope.totalPagar = $scope.totalSub - $scope.totalDescuento + $scope.totalInteres;
                  /// $scope.totalPagar = $filter('currency')($scope.totalPagar, '');
                }
              }

              $scope.buscarPatente = function(){
                if(($scope.numero != undefined)&&($scope.numero != ""))
                {

                  $scope.patente = {};
                  $scope.patente.tipo = 'todo';
                  $scope.patente.patente = $scope.numero;
                  $scope.patente.direccion = '';
                  $scope.patente.nombre = '';
                  srvRestApi.findPatente($scope.patente)
                  .then(function(response){
                    $scope.patentes = response.data.data;
                  },function(response){

                  });
                }
              }


              //---------------------------------------------------------------------------

              $scope.selPatente = function(patente){

                /* if($scope.us.nivel == 'taquilla')
                $state.go('registrarPago');
                else
                $state.go('registrar',patente);
                */

              }

              //-----------------------------------------------------------------------------
              $scope.agregarActividad = function(){
                var data = {};
                data.patente_id = $scope.patente['id'];
                data.actividad_id = $scope.selActividad;
                srvRestApi.asociarActividad(data)
                .then(function(response){

                  $scope.actividadPatentes = response.data;

                  data = {};
                  data.id = $scope.patente['id'];
                  srvRestApi.findActividadPatente(data)
                  .then(function(response){
                    $scope.actividadPatentes = response.data;
                  })

                },function(response){

                });
              }

              //---------------------------------------------------------------------------
              $scope.guardar = function(){
                $scope.dataDec = {};
                $scope.dataDec.patente = $scope.patente ;
                $scope.dataDec.actividad = $scope.actividadPatentes;
                $scope.dataDec.total = $scope.lineTotalPagar ;
                $scope.dataDec.sub = $scope.lineTotalSub ;
                $scope.dataDec.descuento = $scope.lineTotalDescuento ;
                $scope.dataDec.interes = $scope.lineTotalInteres;
                $scope.dataDec.multa = $scope.lineTotalMulta ;
                $scope.dataDec.base = $scope.monto;
                $scope.dataDec.fecha = $scope.declaracion;
                $scope.dataDec.usuario = localStorageService.get('lsUsuario');

                if(($scope.patente.rif != undefined)&&($scope.patente.rif.length>=5)){

                  srvRestApi.declarar($scope.dataDec)
                  .then(
                    function(response){
                      alert('Se registro la declararación correctamente');
                      localStorageService.set('lsDeclaracion',response.data);
                      $scope.cancel();


                    },
                    function(response){

                      alert('Al parecer no se pudo guardar los datos, debido a que existe una declaracion para la actividad economica en el periodo seleccionado');


                    }
                  );
                }else {
                  alert('debe ingresar el RIF del contribuyente');
                }
              }
              //------------------------------------------------------------------------------
              $scope.cancel = function() {
                $mdDialog.cancel();
              };
              //------------------------------------------------------------------------------

              $scope.guardarCambios = function(){

                if(($scope.comentario == null)||($scope.comentario == undefined)){
                  alert('Debe ingresar un comentario justificando el cambio.')
                }else{
                  $scope.dataDec = {};
                  $scope.dataDec.patente = $scope.patente ;
                  $scope.dataDec.actividad = $scope.actividadPatentes;
                  $scope.dataDec.total = $scope.lineTotalPagar ;
                  $scope.dataDec.sub = $scope.lineTotalSub ;
                  $scope.dataDec.descuento = $scope.lineTotalDescuento ;
                  $scope.dataDec.interes = $scope.lineTotalInteres;
                  $scope.dataDec.multa = $scope.lineTotalMulta ;
                  $scope.dataDec.base = $scope.monto;
                  $scope.dataDec.fecha = $scope.declaracion;
                  $scope.dataDec.comentario = $scope.comentario;
                  $scope.dataDec.usuario = localStorageService.get('lsUsuario');
                  $scope.dataDec.recibo = $scope.recibo;

                  srvRestApi.updateDeclaracion($scope.dataDec)
                  .then(
                    function(response){
                      alert('Los cambios fueron realizados correctamente')
                      $scope.cancel();
                    },
                    function(response){

                      alert('Al parecer no se pudo guardar los datos, debido a que existe una declaracion para la actividad economica en el periodo seleccionado');


                    }
                  );
                }
              }


              if($scope.patente != null)
              {
                var data = {};
                data.id = $scope.patente['id'];
                srvRestApi.findActividadPatente(data)
                .then(function(response){
                  $scope.actividadPatentes = response.data;
                  $scope.recibo.lineas.forEach(function(linea){


                    $scope.lineTotalPagar[linea.id] = $scope.getTotalPagar(linea.id);
                    $scope.lineTotalSub[linea.id] = $scope.getSubTotal(linea.id);
                    $scope.lineTotalDescuento[linea.id] = $scope.getDescuento(linea.id);
                    $scope.lineTotalInteres[linea.id] = $scope.getInteres(linea.id);
                    $scope.lineTotalMulta[linea.id] = $scope.getMulta(linea.id);
                    $scope.actividad[linea.id] = $scope.actividadPatentes.filter(function(actividad){
                      if(actividad.id == linea.actividad_id)
                      return actividad;
                    });



                  });
                });

              }



            });
