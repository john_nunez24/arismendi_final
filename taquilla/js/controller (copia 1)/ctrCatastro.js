var app = angular.module('taquillApp');

app.controller('ctrCatastro', function($scope, $rootScope,$mdDialog,facGlobal,$filter,$mdToast,$location, $state, localStorageService, srvRestApi){


  $scope.totalPagarGeneral = 0;
  $scope.valorCatastral = 0;
  $scope.totalDepre = 0;
  $scope.totalPagar = 0;
  $scope.subTotal = 0;
  $scope.totalDescuento = 0;
  $scope.buscarContri = false;
  $scope.crearContri = true;
  $scope.ficha = {};

  $scope.auto = {};
  $scope.auto.monto = 0;
  $scope.auto.descuento = 0;
  $scope.pornerVisible = true;
  $scope.manual = {};
  $scope.items = [];
  $scope.valorTasa = 0;
  $scope.utmari = [];
  $scope.utmari.base = "300000.000000";
  $scope.calculo2020 = false;
  $scope.calculo2019 = false;
  //$scope.valorUmariCierre = 300000;
  $scope.calculoVCC = 0;
  $scope.calculoVCT = 0;
  $scope.lineas =[];
  $scope.totalTotal = 0;
  $scope.manual.monto = 0;
  $scope.tasasAdministrativas = [];
  $scope.tasasCatastro = facGlobal.getTasasAdministrativasCatastro();
  $scope.newZonas = facGlobal.getZonas();
  $scope.newUsos = ['Residencial','Comercial'];
  $scope.newTipologias = facGlobal.getTipoInmueble();
  //$scope.utmari = facGlobal.getSalarioActual();
  $scope.newAlicuotas = facGlobal.getAlicuotas();
  $scope.salarioActual = facGlobal.getSalarioActual();
  $scope.patenAsociadas = [];
      if (localStorageService.get('lspatente') != null )
        $scope.patente = localStorageService.get('lspatente');

      if (localStorageService.get('lsPersona') != null)
        $scope.persona = localStorageService.get('lsPersona');

        if($rootScope.departamentoActual == undefined)
            $rootScope.departamentoActual = {};
         else
            $scope.depActual = $rootScope.departamentoActual;
  if ($scope.autos == undefined){
          $scope.autos = [];
      }
  $scope.usuario = localStorageService.get('lsUsuario');
  $scope.ficha = localStorageService.get('lsFicha');
  $scope.dateServer = localStorageService.get('lsDate');
  $scope.historicoUtributaria= localStorageService.get('lsUtributariaHistorico');
  $scope.activaUtributaria= localStorageService.get('lsUtributariaActiva');
  $scope.historicoIntereses = localStorageService.get('lsIntereses');
  $scope.preFijos = ['V','J','G','E','P','C'];
  $scope.bloquearGuardar = true;
  $scope.mostrarPatente = "";
  $scope.mostrarRIF = "";
  $scope.bnombre = "";
  $scope.brif = "";
  $scope.numero = "";
  $scope.comentario = $rootScope.comentario;
  console.log($scope.ficha);
  //--------------------------------
    $scope.getItemsByPatente = function(id){
        srvRestApi.getItem(id)
           .then(function(resp){
             $scope.items = resp.data;
              console.log($scope.items);
             },function(err){

            });
       }
  //---
  $scope.getItemsByPatente(2);
  console.log($scope.items);
  //------------john nuñez--------18/02/2020----------------------------------------
  $scope.cambiarConcepto = function(item){
        $scope.concepto = item;
     $scope.valorTasa = item.cantidad * item.valorActual.valor;
  }
  //-------------------------
  $scope.agregarLinea = function(){
    var temp = {};
    console.log($scope.patente);
    temp.patente = $scope.patente;
    temp.concepto = $scope.concepto;
    temp.total = $scope.valorTasa;
    $scope.patentePagar.patente = {};
    $scope.concepto = {};
    $scope.valorTasa = 0;
    $scope.lineas.push(temp);
    $scope.totalTotal = 0;
    $scope.lineas.forEach(function(linea){

      $scope.totalTotal += linea.total;
      $scope.bloquearGuardar = false;
    });
    console.log($scope.lineas);


  }
//-----------------------------john nuñez --------------------------------------
$scope.arregloPatentesVinculadas = function (patentes){
var arrgTemp = [];
    console.log(patentes);
    patentes.forEach(function(patente){
      arrgTemp.push(patente);
       if (patente.hijas != null)
          patente.hijas.forEach(function(hija){
            arrgTemp.push(hija);
          });


//list.push(item);
});

  return arrgTemp;

}
//-------------------------------------john nuñez------------------------------------
$scope.cerrarModal = function(){
   $mdDialog.hide();
}

//-------------------------------------john nuñez------------------------------------
  $scope.patenteAsociadas = function(patenteId){
    var data = {};
    $rootScope.patenAsociadas = {};
      data.idHija = patenteId;
      srvRestApi.buscarPatentesPadre(data)
                .then(function(response){
                      $scope.patentes = response.data;
                      $rootScope.patenAsociadas = $scope.arregloPatentesVinculadas($scope.patentes);
                      console.log($rootScope.patenAsociadas);

                },function(response){

                });


  }
//-----------------------------------john nuñez----------------------------------

$scope.patentesContribuyente = function(persona){
  console.log(persona);
  $rootScope.patenteContribuyente = {};
  var data = {};
  data.personas = persona;
      if (data.personas != ''){
       srvRestApi.patentesPersona(data)
                    .then(function(response){
                            console.log(response.data);
                            $rootScope.patenteContribuyente = response.data;
                            },
                              function(response){


                              });
    }
      else
          alert('no se encontro registro de contribuyente');

}
//----------------------john nunez ---------------------------------------------------

    //------------------------------------john nunez 15-02-2020----------------------------------------



        $scope.agregarCatastroItems = function(ev){
          if(($scope.patente.persona == null)
          ||($scope.patente.persona.rif==null)
          ||($scope.patente.persona.rif==undefined)
          ||($scope.patente.persona.rif=="")){
            alert('Atencion, Ingrese un numero de RIF valido antes de agregar una de declaración')
          }else {
              //if ($scope.patenAsociadas != null)
                //$scope.patente.patenteAsociadas = $scope.patenAsociadas;
            $rootScope.patenteActual = $scope.patente;
            console.log($scope.patente);
            localStorageService.set('lsRecibo',undefined);
            $mdDialog.show({

              templateUrl: 'temp/catastro/nuevoCatastroItems.html',
              parent: angular.element(document.body),
              targetEvent: ev,
              clickOutsideToClose:true,
              fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            }).then(function(){
              $scope.econtrarRecibos();
            },function(){
              $scope.econtrarRecibos();
            });
          }
        }


    //------------------------------------john nunez 15-02-2020

  //separar rif y patente
  if(($scope.patente != undefined)&&($scope.patente != {})){
    $scope.mostrarPatente = $scope.patente.patente;
    var temp = $scope.patente.patente.split('-');
    $scope.patente.catastro = temp[1];

    $scope.mostrarRIF = $scope.patente.rif;
    var temp = $scope.patente.rif.split('-');
    $scope.patente.rif = temp[1];
    $scope.patente.prefijo = temp[0];

    $scope.catastroActivo = ($scope.patente.status=='Activo')?true:false;
  }


  try{

  if($scope.ficha == null){
    $scope.ficha = {};
  }else /*{
    $scope.alicuotaValor = {};

    $scope.valorVCT = facGlobal.getValorVCT($scope.ficha.zona_id,$scope.ficha.uso_id);
    console.log($scope.valorVCT);
    if($scope.valorVCT != undefined)
    console.log($scope.valorVCT);
    //$scope.totalVCT =   $scope.valorVCT.valor * $scope.ficha.terreno; viejo
    $scope.totalVCT =   $scope.valorUmariCierre * $scope.ficha.terreno;

    $scope.valorVCC = facGlobal.getValorVCC($scope.ficha.zona_id,$scope.ficha.uso_id);
    $scope.tipologiaValor = facGlobal.getTipologiaValor($scope.ficha.zona_id,$scope.ficha.tipologia);
    $scope.depreciacionValor = facGlobal.getDepreciacionValor($scope.ficha.ano);
    if($scope.valorVCC != undefined)
    $scope.totalVCC = ($scope.ficha.construccion * $scope.valorUmariCierre) * ($scope.tipologiaValor.porcentaje/100) * ($scope.depreciacionValor.porcentaje/100);
    if($scope.totalVCI == 'NaN')
      $scope.totalVCI = 0;

    $scope.alicuotaValor = facGlobal.getAlicuotaById($scope.ficha.alicuota_id);
    if($scope.alicuotaValor != undefined)
    $scope.totalVCI = ($scope.totalVCC + $scope.totalVCT)* ($scope.alicuotaValor.porcentaje/100);
    console.log('EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEee');
    console.log($scope.totalVCI );

  }*/ //viejo
  //nuevo 19-02-2020-----------------------------------------------------------
  {
    $scope.alicuotaValor = {};

    $scope.valorVCT = facGlobal.getValorVCT($scope.ficha.zona_id,$scope.ficha.uso_id);
    console.log($scope.valorVCT);
    $scope.valorVCT.valor_2 =  ($scope.valorVCT.porcentaje / 100) * $scope.utmari.base;
    if($scope.valorVCT != undefined){
      $scope.totalVCT_2 =   $scope.valorVCT.valor_2 * $scope.ficha.terreno;
      $scope.totalVCT =   $scope.valorVCT.valor * $scope.ficha.terreno;
    }
    $scope.valorVCC = facGlobal.getValorVCC($scope.ficha.zona_id,$scope.ficha.uso_id);
    console.log($scope.valorVCC);
    $scope.tipologiaValor = facGlobal.getTipologiaValor($scope.ficha.zona_id,$scope.ficha.tipologia);
    $scope.valorVCC.valor_2 = ($scope.valorVCC.porcentaje / 100) * $scope.utmari.base;
    $scope.depreciacionValor = facGlobal.getDepreciacionValor($scope.ficha.ano);
    if($scope.valorVCC != undefined){
      $scope.totalVCC_2 = ($scope.ficha.construccion * $scope.valorVCC.valor_2) * ($scope.tipologiaValor.porcentaje/100) * ($scope.depreciacionValor.porcentaje/100);
      $scope.totalVCC = ($scope.ficha.construccion * $scope.valorVCC.valor) * ($scope.tipologiaValor.porcentaje/100) * ($scope.depreciacionValor.porcentaje/100);
     }

    if($scope.totalVCI == 'NaN')
      $scope.totalVCI = 0;

if($scope.totalVCI_2 == 'NaN')
      $scope.totalVCI_2 = 0;

    $scope.alicuotaValor = facGlobal.getAlicuotaById($scope.ficha.alicuota_id);
    console.log($scope.alicuotaValor);
    if($scope.alicuotaValor != undefined)
    $scope.totalVCI = ($scope.totalVCC + $scope.totalVCT)* ($scope.alicuotaValor.porcentaje/100);
    $scope.totalVCI_2 = ($scope.totalVCC_2 + $scope.totalVCT_2)* ($scope.alicuotaValor.porcentaje/100);
    console.log('EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEee');
    console.log($scope.totalVCI );

  }
}
catch(error){
  console.log("ERROR:  "  + error);
}


  $scope.depreciacion = localStorageService.get('lsDepreciacion');


  $scope.buscarRecibos = function(data){


    srvRestApi.findRecibos(data)
    .then(function(response){

      $scope.recibos = response.data.data.filter(function(recibo){
        if(recibo.patente_linea.patente_id == $scope.patente['id'])
        return recibo;
      });;


    });
  }




  if(( $scope.patente!=null)&&($scope.patente.persona_id !=0)){

    var data = {};
    data.id = $scope.patente.persona_id;
    data.paginate = "200";
    data.status = $scope.selStatus;
    data.fecha = $scope.fecha;
    $scope.buscarRecibos(data);
  }

  $scope.usos = [
    {id:'1',codigo:'01',nombre:'Residencial',factor:'1',alicuota:'1'},
    {id:'2',codigo:'02',nombre:'Comercial',factor:'1.5',alicuota:'2'},
    {id:'3',codigo:'03',nombre:'Turistico o 2da.',factor:'1.5',alicuota:'1.5'},
    {id:'4',codigo:'04',nombre:'Mixto',factor:'1.5',alicuota:'1.5'}
  ];

  $scope.estados = [
    'Excelente',
    'Buena',
    'Regular',
    'Mala',
    'No Aplica'
  ];
  $scope.anos = [];
  $scope.anos.push('No Aplica');
  for(var i=$scope.dateServer.ano; i>=1900; i--){
    $scope.anos.push(i);
  }

  $scope.pagaAnos = [];

  var hastaAno = $scope.dateServer.ano - 6;

  /*  if(hastaAno < $scope.ficha.ano){
  if($scope.ficha != null )
  hastaAno = $scope.ficha.ano;
}*/


for(var y=$scope.dateServer.ano; y>=2018; y--){
  $scope.pagaAnos.push(y);
}




$scope.zonas = [

  {id:'2',  codigo:'CAT-S01-01', sector: 'S01', subsector:'01', nombre:'BOULEVARD 5 DE JULIO', unidad: 20},
  {id:'3',  codigo:'CAT-S01-02', sector: 'S01', subsector:'02', nombre:'C/ MATASIETE', unidad: 12},
  {id:'4',  codigo:'CAT-S01-03', sector: 'S01', subsector:'03', nombre:'C/ CEDEÑO', unidad: 12},
  {id:'5',  codigo:'CAT-S01-04', sector: 'S01', subsector:'04', nombre:'C/ RODULFO', unidad: 12},
  {id:'6',  codigo:'CAT-S01-05', sector: 'S01', subsector:'05', nombre:'C/ VIRGEN DEL CARMEN', unidad: 15},
  {id:'7',  codigo:'CAT-S01-06', sector: 'S01', subsector:'06', nombre:'C/ UNIÓN', unidad: 15},
  {id:'8',  codigo:'CAT-S01-07', sector: 'S01', subsector:'07', nombre:'C/ INDEPENDENCIA', unidad: 10},
  {id:'9',  codigo:'CAT-S01-08', sector: 'S01', subsector:'08', nombre:'C/ PAZ', unidad: 10},
  {id:'10', codigo:'CAT-S01-09', sector: 'S01', subsector:'09', nombre:'C/ GONZÁLEZ', unidad: 12},
  {id:'11', codigo:'CAT-S01-10', sector: 'S01', subsector:'10', nombre:'C/ RUIZ', unidad: 10},
  {id:'12', codigo:'CAT-S01-11', sector: 'S01', subsector:'11', nombre:'C/ FERMÍN', unidad: 10},
  {id:'13', codigo:'CAT-S01-12', sector: 'S01', subsector:'12', nombre:'C/ LAREZ', unidad: 10},
  {id:'14', codigo:'CAT-S01-13', sector: 'S01', subsector:'13', nombre:'C/ LIBERTAD', unidad: 10},
  {id:'15', codigo:'CAT-S01-14', sector: 'S01', subsector:'14', nombre:'C/ GÓMEZ', unidad: 10},
  {id:'16', codigo:'CAT-S01-15', sector: 'S01', subsector:'15', nombre:'C/ TENÍAS', unidad: 10},
  {id:'18', codigo:'CAT-S02-01', sector: 'S02', subsector:'01', nombre:'C/ MARGARITA', unidad: 10},
  {id:'19', codigo:'CAT-S02-02', sector: 'S02', subsector:'02', nombre:'C/ SALAZAR', unidad: 10},
  {id:'20', codigo:'CAT-S02-03', sector: 'S02', subsector:'03', nombre:'C/ LA CEIBA', unidad: 10},
  {id:'21', codigo:'CAT-S02-04', sector: 'S02', subsector:'04', nombre:'C/ MONTANER', unidad: 10},
  {id:'22', codigo:'CAT-S02-05', sector: 'S02', subsector:'05', nombre:'C/ LA NORIA', unidad: 12},
  {id:'23', codigo:'CAT-S02-06', sector: 'S02', subsector:'06', nombre:'C/ FIGUEROA', unidad: 10},
  {id:'24', codigo:'CAT-S02-07', sector: 'S02', subsector:'07', nombre:'C/ ROJAS', unidad: 10},
  {id:'25', codigo:'CAT-S02-08', sector: 'S02', subsector:'08', nombre:'C/ GIRARDOT', unidad: 15},
  {id:'26', codigo:'CAT-S02-09', sector: 'S02', subsector:'09', nombre:'AV. JUAN CANCIO R.', unidad: 15},
  {id:'27', codigo:'CAT-S02-10', sector: 'S02', subsector:'10', nombre:'AV. 31 DE JULIO', unidad: 20},
  {id:'28', codigo:'CAT-S02-11', sector: 'S02', subsector:'11', nombre:'EL MAMEY', unidad: 10},
  {id:'30', codigo:'CAT-S03-01', sector: 'S03', subsector:'01', nombre:'LAS HUERTAS', unidad: 12},
  {id:'31', codigo:'CAT-S03-02', sector: 'S03', subsector:'02', nombre:'PALOSANO', unidad: 10},
  {id:'32', codigo:'CAT-S03-03', sector: 'S03', subsector:'03', nombre:'LA PORTADA', unidad: 10},
  {id:'33', codigo:'CAT-S03-04', sector: 'S03', subsector:'04', nombre:'LA GUARINA', unidad: 12},
  {id:'34', codigo:'CAT-S03-05', sector: 'S03', subsector:'05', nombre:'CAMORUCO', unidad: 10},
  {id:'36', codigo:'CAT-S04-01', sector: 'S04', subsector:'01', nombre:'CANTARRANA', unidad: 8},
  {id:'37', codigo:'CAT-S04-02', sector: 'S04', subsector:'02', nombre:'BUENOS AIRES-TERRAZAS 95', unidad: 10},
  {id:'38', codigo:'CAT-S04-03', sector: 'S04', subsector:'03', nombre:'CERRO EL CASTILLO', unidad: 8},
  {id:'39', codigo:'CAT-S04-04', sector: 'S04', subsector:'04', nombre:'SANTA LUCIA', unidad: 12},
  {id:'40', codigo:'CAT-S04-05', sector: 'S04', subsector:'05', nombre:'AV. BOLÍVAR (CONSTITUCIÓN)', unidad: 20},
  {id:'42', codigo:'CAT-S05-01', sector: 'S05', subsector:'01', nombre:'SECTOR EL COPEY', unidad: 8},
  {id:'44', codigo:'CAT-S06-01', sector: 'S06', subsector:'01', nombre:'LA OTRABANDA', unidad: 8},
  {id:'45', codigo:'CAT-S06-02', sector: 'S06', subsector:'02', nombre:'EL SAMAN', unidad: 8},
  {id:'46', codigo:'CAT-S06-03', sector: 'S06', subsector:'03', nombre:'EL GUAYABAL', unidad: 8},
  {id:'47', codigo:'CAT-S06-04', sector: 'S06', subsector:'04', nombre:'LAS CASITAS', unidad: 8},
  {id:'48', codigo:'CAT-S06-05', sector: 'S06', subsector:'05', nombre:'EL SACO', unidad: 8},
  {id:'49', codigo:'CAT-S06-06', sector: 'S06', subsector:'06', nombre:'EL PORTACHUELO', unidad: 8},
  {id:'51', codigo:'CAT-S07-01', sector: 'S07', subsector:'01', nombre:'LAS TAPIAS', unidad: 8},
  {id:'52', codigo:'CAT-S07-02', sector: 'S07', subsector:'02', nombre:'EL CHUARE', unidad: 8},
  {id:'53', codigo:'CAT-S07-03', sector: 'S07', subsector:'03', nombre:'COCHEIMA (AV. 31 DE JULIO)', unidad: 20},
  {id:'55', codigo:'CAT-S08-01', sector: 'S08', subsector:'01', nombre:'COCHEIMA', unidad: 20},
  {id:'56', codigo:'CAT-S08-02', sector: 'S08', subsector:'02', nombre:'SANTA ISABEL', unidad: 10},
  {id:'57', codigo:'CAT-S08-03', sector: 'S08', subsector:'03', nombre:'SALAMANCA', unidad: 8},
  {id:'58', codigo:'CAT-S08-04', sector: 'S08', subsector:'04', nombre:'AV. 31 DE JULIO', unidad: 20},
  {id:'60', codigo:'CAT-S09-01', sector: 'S09', subsector:'01', nombre:'GUATAMARE', unidad: 15},
  {id:'61', codigo:'CAT-S09-02', sector: 'S09', subsector:'02', nombre:'LA AGUADA', unidad: 8},
  {id:'62', codigo:'CAT-S09-03', sector: 'S09', subsector:'03', nombre:'ATAMO NORTE', unidad: 10},
  {id:'63', codigo:'CAT-S09-04', sector: 'S09', subsector:'04', nombre:'SABANA DE GUACUCO', unidad: 15},
  {id:'64', codigo:'CAT-S09-05', sector: 'S09', subsector:'05', nombre:'PLAYA GUACUCO', unidad: 30},
  {id:'65', codigo:'CAT-S09-06', sector: 'S09', subsector:'06', nombre:'ATAMO SUR', unidad: 10},
  {id:'66', codigo:'CAT-S09-07', sector: 'S09', subsector:'07', nombre:'EL HATO', unidad: 10}
];


$scope.guardarRecibo = function(){
  if(confirm('Esta Seguro de generar la orden de cobro?')){
    var ficha = localStorageService.get('lsFicha');
    if((ficha == null)||(ficha == undefined))
    {
      alert('Antes de crear una orden de cobro debe completar los datos basicos para el calculo catastral');
    }else {
      var data = {};
      data.patente = $scope.patente;
      data.usuario = localStorageService.get('lsUsuario');
      data.lineas = $scope.totalCalculos;


      srvRestApi.guardarReciboCatastro(data)
      .then(function(response){
        $scope.bloquearGuardar = true;
        alert('Se Registro con exito la orden de cobro');
        $scope.totalCalculos = [];
        var data = {};

        data.id = response.data.recibo.persona_id;
        data.paginate = "200";
        data.status = "";
        data.fecha = "";
        $scope.buscarRecibos(data);
      },function(response){
        alert(response.data.error);
      });
    }
  }
}
//----------------------------------------------------------------------------

$scope.verDetalle = function(ev,recibo){
  localStorageService.set('lsRecibo',recibo);
  $mdDialog.show({

    templateUrl: 'temp/declarar/verDeclaracion.html',
    parent: angular.element(document.body),
    targetEvent: ev,
    clickOutsideToClose:true,
    fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
  });

}
//-----------------------john nuñez-----------------------------------------------------
$scope.eliminarCatastro = function(patente , ev){
  var data = {};
  var confirm = $mdDialog.prompt()
        .title('Elimina de Patente N°: ' + patente.patente)
        .textContent('Ingrese un comentario del porque se va a Eliminar')
        .placeholder('Comentario')
        .ariaLabel('Comentario')
        .targetEvent(ev)
        .required(true)
        .ok('Eliminar')
        .cancel('No Eliminar');

      $mdDialog.show(confirm).then(function(result) {
        data.patente = patente;
        data.comentario = result;
        srvRestApi.eliminarCatastro(data).
                  then(function(response){
                        alert('La eliminacion se Realizo Correctamente');
                        //$state.go('ficha');
                        $scope.catastroActivo = false;
                      //  alert($scope.patente);
                        //$scope.patente = localStorageService.get('lspatente');
                        },function(response){


                        });
      }, function() {
           alert('malo');
      });


}

//*********************JESUS GOMEZ*************************************

/*$scope.format_fecha = function(fecha){
   var temp = [];
if (fecha != undefined) {
   temp = fecha.split(' ');
   temp = temp[0].split('-');
   return temp[2]+'/'+temp[1]+'/'+temp[0];
 }
 else {
    return "no posee pago"
 }


}
*/


//********************JESUS GOMEZ***************************************
$scope.verFicha = function(ev){
  $mdDialog.show({
    templateUrl: 'temp/catastro/fichaImpresa.html',
    parent: angular.element(document.body),
    targetEvent: ev,
    clickOutsideToClose:true,
    fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
    //preserveScope: true
  })
  .then(function(answer) {
    console.log('**paso1***');

    //$scope.status = 'You said the information was "' + answer + '".';
  }, function() {
    console.log('**paso2***');

  });
}
//-------------------john nuñez-------------------------------------------------
$scope.cargaManualRecibo = function(recibo, patente){
  console.log(patente);
  console.log(recibo);
  if(recibo.dateHasta == undefined){
       recibo.dateHasta = recibo.dateDesde;
    console.log('esto es lo que busco');
    console.log(recibo);
  }
  var data = {};
  data.patente =  patente;
  data.reciboManual = recibo;
  data.status = 'PagoManual';

  srvRestApi.cargaReciboManual(data).then(function(response){
        alert('Se Reguistro exitosamente la Operacion');
        $scope.cerrarModal();

  },function(response){});
}
//-------------------john nuñez-------------------------------------------------
$scope.cargaManualCobro = function(recibo, patente){

  if(recibo.dateHasta == undefined){
       recibo.dateHasta = recibo.dateDesde;
    console.log('esto es lo que busco');
    console.log(recibo);
  }
  recibo.reciboNumero = null;
  var data = {};
  data.patente =  patente;
  data.reciboManual = recibo;
  data.status = 'Pendiente';

  srvRestApi.cargaReciboManual(data).then(function(response){
        alert('Se Reguistro exitosamente la Operacion');
        $scope.cerrarModal();
        $scope.econtrarRecibos();

  },function(response){});
}
//-------------------john nuñez-------------------------------------------------
$scope.cambiaAutoFecha = function(){
  $scope.totalcalculoVCC = 0;
  $scope.totalCalculoVCT = 0;
  $scope.totalcalculoVCI = 0;

     if($scope.auto != {}){
       if(($scope.auto.anno == 2019 )||($scope.auto.anno == 2018)){
          //$scope.calculo2019 = true;
            $scope.calculoVCT = $scope.valorVCT.valor;
            $scope.calculoVCC = $scope.valorVCC.valor;
            $scope.totalcalculoVCC = $scope.totalVCC;
            $scope.totalCalculoVCT = $scope.totalVCT;
            $scope.totalcalculoVCI = $scope.totalVCI;
          //alert('ATENCIÓN: La fecha de desde siempre debe ser menor o igual que la fecha hasta.')

        }
          if($scope.auto.anno == 2020){
          //  $scope.calculo2020 = true;
            console.log($scope.valorVCT);
            console.log($scope.valorVCC);
            $scope.calculoVCT = $scope.utmari.base *($scope.valorVCT.porcentaje/100);
            $scope.calculoVCC= $scope.utmari.base *($scope.valorVCC.porcentaje/100);
            $scope.totalcalculoVCC = $scope.totalVCC_2;
            $scope.totalCalculoVCT = $scope.totalVCT_2;
            $scope.totalcalculoVCI = $scope.totalVCI_2;
          }

        $scope.auto.submonto = $scope.totalcalculoVCI;
        $scope.auto.descuento = $scope.auto.submonto * 0;
        $scope.auto.monto = $scope.auto.submonto ;

     }

}
//-------------------john nunez-----26-02-2020----------------------------
$scope.agregarAnno = function (linea){
  console.log(linea);
  console.log($scope.autos);
    if( ($scope.autos == undefined) || ($scope.autos == null) ||($scope.autos.length == 0) ){
        console.log('paso 1');
        $scope.autos.push(linea);
        $scope.auto ={};
    }else {
       $scope.autos.forEach(function(auto){
         if (auto.anno != linea.anno){
           console.log('paso2');
           $scope.autos.push($scope.auto);
           $scope.auto ={};
         } else {
           alert('Ese Año ya se agrego a la lista');
         }
       });
    }



}


$scope.toggle = function (item, list) {
  var idx = list.indexOf(item);
  if (idx > -1) {
    list.splice(idx, 1);
  }
  else {
    list.push(item);
  }

  list.forEach(function(li){

      $scope.totalTasas = (li.salario/100) * $scope.salarioActual.base;
  });
};

$scope.exists = function (item, list) {
  return list.indexOf(item) > -1;
};

$scope.guardarOrden = function(){
  if(confirm('Esta Seguro de generar la orden de cobro?')){
    var ficha = localStorageService.get('lsFicha');
    if((ficha == null)||(ficha == undefined))
    {
      alert('Antes de crear una orden de cobro debe completar los datos basicos para el calculo catastral');
    }else {
      var data = {};
      data.patente = $scope.patente;
      data.usuario = localStorageService.get('lsUsuario');
      data.manual = $scope.manual;
      data.auto = $scope.autos;
      data.tasas = $scope.lineas;
      console.log($scope.auto);
      console.log('*****************************');
      console.log($scope.lineas);
      srvRestApi.guardarOrdenCatastro(data)
      .then(function(response){
        $scope.bloquearGuardar = true;
        alert('Se Registro con exito la orden de cobro');
        $scope.totalCalculos = [];
        var data = {};

        data.id = response.data.recibo.persona_id;
        data.paginate = "200";
        data.status = "";
        data.fecha = "";
        $scope.buscarRecibos(data);
          $state.go('ficha');
      },function(response){
        alert(response.data.error);
      });
    }
  }
}




$scope.factorPorUso= function(id){
  var result = {};
  $scope.usos.forEach(function(uso){
    if(uso.id == id)
    result = uso;
  });
  return result;
}


$scope.unidadesPorZona = function(id){
  var result = {};
  $scope.zonas.forEach(function(zona){
    if(zona.id == id)
    result = zona;
  });
  return result;
}

$scope.unidadTributariaAno = function(anoDeclarar){
  var result = $scope.activaUtributaria;
  $scope.historicoUtributaria.forEach(function(ut){
    if(ut.ano == anoDeclarar)
    result = ut;
  });

  return result;

}

$scope.depreciacionPorEstado = function(edad,estado){
  var result = 100;
  $scope.depreciacion.forEach(function(dep){
    if(edad == dep.edad){
      if(estado == 'Excelente')
      result = dep.excelente;
      if(estado == 'Buena')
      result = dep.bueno;
      if(estado == 'Regular')
      result = dep.regular;
      if(estado == 'Mala')
      result = dep.malo;

    }
  });
  return result;
}

//----------------------------------------------------------------------------

$scope.calcularIntereses = function(anoDeclarar, deuda ){

  var interesTotal = 0;
  var temp = 0;
  var aux = 0;
  $scope.historicoIntereses.forEach(function(interes){
    if(interes.ano >= anoDeclarar){

      temp = (interes.tasa * 1.2)/100;
      temp = temp.toFixed(2);
      aux = (deuda * temp)/12;

      interesTotal = (interesTotal*1) + (aux.toFixed(2))*1;
    }
  });

  return interesTotal;

}

//----------------------------------------------------------------------------


$scope.calculaAnual= function(anoDeclarar,ficha){
  var cal = {};
  cal.valorCatastral = 0;
  cal.totalDepre = 0;
  cal.totalPagar = 0;
  cal.totalDescuento = 0;
  cal.valorCatastralTotal = 0;

  cal.depre = $scope.depreciacionPorEstado((anoDeclarar-ficha.ano),ficha.estado);
  cal.unidadZona = $scope.unidadesPorZona(ficha.zona);
  cal.proUso = $scope.factorPorUso(ficha.uso);
  cal.uTributaria = $scope.unidadTributariaAno(anoDeclarar);

  cal.ano = anoDeclarar;
  cal.valorCatastralTerreno = ((ficha.terreno*1)*((cal.unidadZona.unidad*cal.proUso.factor)*cal.uTributaria.base));

  //calcular solo el año de la construccion a partir de que es declarado o construido
  if(anoDeclarar >= ficha.ano){
    cal.valorCatastralConstruccion = ((ficha.construccion*1)*((cal.unidadZona.unidad*cal.proUso.factor)*cal.uTributaria.base));
    cal.totalDepre = cal.valorCatastralConstruccion * (1-(cal.depre/100));
  }else {
    cal.valorCatastralConstruccion = 0;
    cal.totalDepre = 0;
  }
  cal.valorCatastral = (cal.valorCatastralTerreno + cal.valorCatastralConstruccion);
  cal.valorCatastralTotal = (cal.valorCatastralTerreno + cal.valorCatastralConstruccion) - cal.totalDepre;
  cal.subTotal = cal.valorCatastralTotal *((cal.proUso.alicuota*1)/100);


  if(anoDeclarar == $scope.dateServer.ano){
    if($scope.dateServer.mes == 1 )
    cal.totalDescuento = cal.subTotal * 0.15;
    if($scope.dateServer.mes == 2 )
    cal.totalDescuento = cal.subTotal * 0.10;
    if($scope.dateServer.mes == 3 )
    cal.totalDescuento = cal.subTotal * 0.05;
  }
  cal.intereses = 0;
  if( anoDeclarar != $scope.dateServer.ano )
  cal.intereses = $scope.calcularIntereses(anoDeclarar, cal.subTotal);
  cal.totalPagar = (cal.subTotal + cal.intereses) - cal.totalDescuento;
  $scope.totalPagarGeneral = $scope.totalPagarGeneral + cal.totalPagar;

  return cal;

}
//------------------------------------------------------------------------------
$scope.calcular = function(){
  var  ficha = localStorageService.get('lsFicha');
  $scope.totalCalculos = [];
  $scope.totalPagarGeneral = 0;

  for (var i = $scope.selAno; i <= $scope.dateServer.ano; i++) {
    $scope.totalCalculos.push($scope.calculaAnual(i,ficha));
  }
  $scope.bloquearGuardar = false;
}

//----------------------------------------------------------------------------

$scope.calcular2 = function(anoConstruccion,anoDeclarar){
  var cal = {};
  cal.valorCatastral = 0;
  cal.totalDepre = 0;
  cal.totalPagar = 0;
  cal.totalDescuento = 0;
  cal.valorCatastralTotal = 0;
  var ficha = localStorageService.get('lsFicha');
  if((ficha == null)||(ficha == undefined))
  {
    alert('Antes de crear una orden de cobro debe completar los datos basicos para el calculo catastral');
  }else{
    $scope.depre = $scope.depreciacionPorEstado(($scope.selAno-ficha.ano),ficha.estado);
    $scope.unidadZona = $scope.unidadesPorZona(ficha.zona);
    $scope.proUso = $scope.factorPorUso(ficha.uso);
    //$scope.factorUso = $scope.factorPorUso(ficha.uso);

    $scope.valorCatastralTerreno = ((ficha.terreno*1)*(($scope.unidadZona.unidad*$scope.proUso.factor)*300));
    $scope.valorCatastralConstruccion = ((ficha.construccion*1)*(($scope.unidadZona.unidad*$scope.proUso.factor)*300));
    $scope.totalDepre = $scope.valorCatastralConstruccion * (1-($scope.depre/100));
    $scope.valorCatastral = ($scope.valorCatastralTerreno + $scope.valorCatastralConstruccion);
    $scope.valorCatastralTotal = ($scope.valorCatastralTerreno + $scope.valorCatastralConstruccion) - $scope.totalDepre;
    $scope.subTotal = $scope.valorCatastralTotal *(($scope.proUso.alicuota*1)/100);


    if($scope.selAno == '2018')
    $scope.totalDescuento = $scope.subTotal * 0.15;

    $scope.totalPagar = $scope.subTotal - $scope.totalDescuento;


  }
}




$scope.guardarFicha = function(){
  if(($scope.patente.rif=="")||($scope.patente.nombre=="")
  ||($scope.patente.direccion=="")||($scope.ficha.zona=="")
  ||($scope.ficha.uso=="")||($scope.ficha.terreno=="")
  ||($scope.ficha.construccion=="")||($scope.patente.rif==undefined)
  ||($scope.patente.nombre==undefined)
  ||($scope.patente.direccion==undefined)||($scope.ficha.zona==undefined)
  ||($scope.ficha.uso==undefined)||($scope.ficha.terreno==undefined)
  ||($scope.ficha.construccion==undefined)){
    alert('ingrese los campos obligatorios');
  }else{
    var data = {};
    data.patente = $scope.patente;
    data.ficha = $scope.ficha;
    data.usuario = localStorageService.get('lsUsuario');

    srvRestApi.guardarFicha(data)
    .then(function(response){

      localStorageService.set('lsFicha',response.data);
      $scope.ficha = localStorageService.get('lsFicha');
      $scope.calcular();
      alert('Se Actualizaron  los datos correctamente');
    },function(response){


    });
  }

}


//------------------------------------------------------------------------------
$scope.formCrearFicha = function(){
  localStorageService.set('lspatente',undefined);
  localStorageService.set('lsFicha',undefined);

  $state.go('newCatastro');
}
//------------------------------------------------------------------------------

$scope.crearFicha = function(){

  var data = {};
  data.patente = $scope.patente;
  data.ficha = $scope.ficha;
  data.comentario = $scope.comentario;
  data.usuario = localStorageService.get('lsUsuario');

  if($scope.patente.correo == undefined)
  data.patente.correo = "";

  srvRestApi.crearFicha(data)
  .then(function(response){
    alert('Se Actualizaron  los datos correctamente');
    $scope.patente = {};
    localStorageService.set('lspatente',undefined);

    $scope.ficha = {};
    localStorageService.set('lsFicha',undefined);
    $state.go('catastro');
  },function(response){

    alert(response.data.error);
  });
}
//------------------------------john nunez-------------------------------------
$scope.guardarFichaCatastral = function(){

      $scope.comentario = "Edicion por Secretaria de la ficha Catastral N°:" + $scope.patente.patente;
      var data = {};
      data.patente = $scope.patente;
      data.ficha = $scope.ficha;
      data.comentario = $scope.comentario;
      data.usuario = localStorageService.get('lsUsuario');
      srvRestApi.editarFichaCatastral(data)
              .then(function(response){
                alert('Se Actualizaron  los datos correctamente');
                localStorageService.set('lspatente',response.data.patente);
                localStorageService.set('lsFicha',response.data.catastro);
                $state.go('fichaCatastral');
                      },function(response){

                      });
}
//------------------------------------------------------------------------------

$scope.editarFicha = function(){

  var data = {};
  data.patente = $scope.patente;
  data.ficha = $scope.ficha;
  data.comentario = $scope.comentario;
  data.usuario = localStorageService.get('lsUsuario');

  console.log(data);
  srvRestApi.editarFicha(data)
  .then(function(response){
    alert('Se Actualizaron  los datos correctamente');
    localStorageService.set('lspatente',response.data.patente);
    localStorageService.set('lsFicha',response.data.catastro);
    $state.go('ficha');
  },function(response){
    alert(response.data.error);
  });
}


//------------------------------------------------------------------------------

$scope.buscarPatente = function(){
  //if(($scope.numero != undefined)&&($scope.numero != ""))
  {

    $scope.patente = {};
    $scope.patente.tipo = 'inmobiliaria';
    $scope.patente.patente = $scope.numero;
    $scope.patente.nombre = $scope.bnombre;
    $scope.patente.rif = $scope.brif;
    $scope.patente.direccion = '';
    //-----------------------------------
    srvRestApi.findPatente($scope.patente)
    .then(function(response){
      $scope.patentes = response.data.data;
    },function(response){

    });

  }
}
//----------------------------john nunez------------------------------------------------
//---------------------

$scope.econtrarRecibos = function(){
  $scope.visualizarVinculacion = false;
  $scope.mostrarVinculacion = false;

  if ($scope.patente!=null){
    var data = {};
    data.id = $scope.patente.persona.id;
    data.paginate = "200";
    data.status = $scope.selStatus;
    data.fecha = $scope.fecha;
    data.tipo = "Declaracion";
    $scope.checkIcono = '../images/add.svg';
    $scope.color = 'Grey';

    srvRestApi.findRecibos(data)
    .then(function(response){
      $scope.swTieneRecibos = true;
      $scope.recibos = response.data.data;
      if($scope.recibos.length == 0){
        $scope.swTieneRecibos = false;
      }
    });
    console.log($scope.patente);
    /* var data1 = {};
    data1.id = $scope.patente.id;
    srvRestApi.patentesAsociadas(data1)
                .then(
                      function(response){
                        $scope.patenAsociadas = response.data;
                        console.log($scope.patenAsociadas);
                      },
                        function(response){
                          console.log('no hay');
                        }); */

    }

    else {
    if ($scope.patente == null){
      var data = {};
      data.id = $scope.persona.id;
      data.paginate = "200";
      data.status = $scope.selStatus;
      data.fecha = $scope.fecha;
      data.tipo = "Declaracion";
      $scope.checkIcono = '../images/add.svg';
      $scope.color = 'Grey';

      srvRestApi.findRecibos(data)
      .then(function(response){
        $scope.swTieneRecibos = true;
        $scope.recibos = response.data.data;
        if($scope.recibos.length == 0){
          $scope.swTieneRecibos = false;
        }
      });
    }
  }

}
//-----------------------john nuñez------------------------------------------------------------
    $scope.agregarCatatroItems = function(ev){
      if(($scope.patente.persona == null)
      ||($scope.patente.persona.rif==null)
      ||($scope.patente.persona.rif==undefined)
      ||($scope.patente.persona.rif=="")){
        alert('Atencion, Ingrese un numero de RIF valido antes de agregar una de declaración')
      }else {

        $scope.patente.patenteAsociadas = $scope.patenAsociadas;
          if ($scope.patente.patenteAsociadas==undefined)
                $scope.patente.patenteAsociadas= [];
        console.log($scope.patente.patenteAsociadas);
        $rootScope.patenteActual = $scope.patente;
        console.log($scope.patente);
        localStorageService.set('lsRecibo',undefined);
        $mdDialog.show({

          templateUrl: 'temp/declarar/nuevaDeclaracionItems.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true,
          fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        }).then(function(){
          $scope.econtrarRecibos();
        },function(){
          $scope.econtrarRecibos();
        });
      }
    }

//--------------------------------john nuñez-------------------------------------
$scope.ActivarCatastro = function(patente , ev) {
  var data = {};
  var confirm = $mdDialog.prompt()
        .title('Activacion de Patente N°: ' + patente.patente)
        .textContent('Ingrese un comentario porque se hace la Activacion')
        .placeholder('Comentario')
        .ariaLabel('Comentario')
        .initialValue('')
        .targetEvent(ev)
        .required(true)
        .ok('Activar')
        .cancel('No Activar');

      $mdDialog.show(confirm).then(function(result) {
        data.patente = patente;
        data.comentario = result;
        srvRestApi.activarCatastro(data).then(function(response){
                              alert('La Activacion se Realizo Correctamente');
                            //  $state.go('ficha');
                              $scope.catastroActivo = true;

                            },function(response){});
      }, function() {

      });

}
//---------------------------------john nuñez----------------------------------------
$scope.editarFichaCatastral = function(){
  $scope.pornerVisible = false;
}
//---------------------------------john nunez---------------------------------------------
$scope.infoPatenteGestion = function(patente, ev){
  $scope.patenteGestion = patente;
        $mdDialog.show({
          templateUrl: 'temp/catastro/infoPatenteGestion.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true,
          fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                }).then(function(){
                  //$scope.econtrarRecibos();
                },function(){
                  //$scope.econtrarRecibos();
                });

}
//---------------------------------john nunez---------------------------------------------
$scope.reciboManual = function(ev , patente){
  $scope.patente = patente;
        $mdDialog.show({
          templateUrl: 'temp/catastro/reciboManual.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true,
          fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                }).then(function(){
                  //$scope.econtrarRecibos();
                },function(){
                  //$scope.econtrarRecibos();
                });

}
//----------------------john nuñez-----------------------------------------------
//---------------------------------john nunez---------------------------------------------
$scope.cobroManual = function(ev , patente){
  $scope.patente = patente;
        $mdDialog.show({
          templateUrl: 'temp/catastro/cobroManual.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true,
          fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                }).then(function(){
                  //$scope.econtrarRecibos();
                },function(){
                  //$scope.econtrarRecibos();
                });

}
//----------------------john nuñez-----------------------------------------------
$scope.verificarData = function(fecha){

    if (fecha == undefined){

        fecha = 'No tiene Pago';
    }
    return fecha;
}
//---------------------------------------------------------------------------

$scope.selObj = function(patente,tipo){
  localStorageService.set('lspatente',null);
  localStorageService.set('lsPersona',null);
  localStorageService.set('lsFicha',null);
  localStorageService.set('lspatente',patente);
  $scope.patentesContribuyente(patente.persona_id);
  console.log($scope.usuario);

  var data ={};
  var data1 = {};
  data1.patente_id = patente.id;
  data.patente_id = patente.id;
    srvRestApi.comentarioPatenteAnulada(data1)
              .then(function(response){
                console.log('paso 1****');
                console.log(response);
                $rootScope.comentario=response.data.comentario;
              },function(response){
                console.log('paso 2****');
              });

          srvRestApi.findFichaByPatente(data)
          .then(function(response){
            localStorageService.set('lsFicha',response.data);
            if ( $scope.usuario.nivel == 'secrecatastro'){
              $state.go('fichaCatastral');
            }
              else{
                $state.go('ficha');
                $rootScope.patenAsociadas = $scope.patenteAsociadas(patente.id);
              console.log($rootScope.patenAsociadas);
            }

          },function(response){
            if ( $scope.usuario == 'secrecatastro'){
              $state.go('fichaCatastral');
            }
              else{
            localStorageService.set('lsFicha',null);
            $state.go('ficha');
          }
          });

}
//----------------------------john nunez------12-02-2020------------------------------------------
//----------------------john nunez ---------------------------------------------------
$scope.eliminarVinculo = function(patHija,ev){
  console.log(patHija);
  console.log($scope.patente);
  var data = {};
  var data1 = {};
  data1.id = $scope.patente.id;
  data.padreId = $scope.patente.id;
  data.hijaId = patHija.id;
  var confirm = $mdDialog.prompt()
      .title('Ingrese el Comentario')
      .textContent('Motivo por el cual elimina la Vinculacion  de las Patentes.')
      .placeholder('Comentario')
      .ariaLabel('Comentario')
      .initialValue('')
      .targetEvent(ev)
      .required(true)
      .ok('Eliminar')
      .cancel('Cancelar');

    $mdDialog.show(confirm).then(function(result) {
      //$scope.status = 'You decided to name your dog ' + result + '.';
              console.log(result);
              data.comentario = result;
                srvRestApi.eliminarVinculacion(data)
                          .then(
                                function(response){

                                  srvRestApi.patentesAsociadas(data1)
                                              .then(
                                                    function(response){
                                                      $scope.patenAsociadas = response.data;
                                                      console.log($scope.patenAsociadas);
                                                    },
                                                      function(response){
                                                        console.log('no hay');
                                                      });
                                  },
                                    function(response){

                                     });
      }, function() {
      //$scope.status = 'You didn\'t name your dog.';
      });
}
//------------John nuñez------------------------------------------------------------------
$scope.vinculacion = function(ev) {
  var data1 = {};
  data1.id = $scope.patente.id;
    $mdDialog.show({

      templateUrl: 'temp/catastro/dialogoVincular.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
    })
    .then(function(answer) {
        var data1 = {};
        data1.id = $scope.patente.id;
        srvRestApi.patentesAsociadas(data1)
                  .then(
                      function(response){
                        console.log("exito debe mostar");
                        $scope.patenAsociadas = response.data;
                        console.log($scope.patenAsociadas);
                       },
                        function(response){
                        console.log('no hay');
                      });
     }, function() {




    });
};
//---------------------john nuñez ---------------------------------------------------
$scope.verVinculacion = function () {
  $scope.visualizarVinculacion = false;
  $scope.mostrarVinculacion = true;

}
//------------------------ john nuñez ------------------------------------------
$scope.addVinculacion = function (obj, ev){
var data = {};
  console.log(obj);
  console.log($scope.patente['id']);
var data1 = {};
  data1.id = $scope.patente.id;
  data.idPadre = $scope.patente['id'];
  data.idhija = obj.id;
  /*var confirm = $mdDialog.prompt()
      .title('Ingrese el Comentario')
      .textContent('Motivo por el cual se hace la Vinculacion  de las Patentes.')
      .placeholder('Comentario')
      .ariaLabel('Comentario')
      .initialValue('')
      .targetEvent(ev)
      .required(true)
      .ok('Vincular')
      .cancel('Cancelar');

    $mdDialog.show(confirm).then(function(result) {
      //$scope.status = 'You decided to name your dog ' + result + '.';
              console.log(result);*/
              data.comentario = "Agregado";
                srvRestApi.vinculacionPatentes(data)
                      .then(function(response){
                               $mdDialog.hide();
                             },function(response){

                             });
      /*}, function() {
        //$scope.status = 'You didn\'t name your dog.';
      });*/
}

//-----------------------john nuñez-----------------------------------------------
 $scope.cancel = function() {
   $mdDialog.cancel();
 };

//--------------------------------john nuñez -------- 12-02-2020----------------

//---------------------------------john nuñez-----------------------------------
$scope.selObjVinculado = function(patente, tipo, ev){
  console.log(patente);

  if (patente.tipo == 'comercio'){
      $scope.infoPatenteGestion(patente, ev);

    }
  if (patente.tipo == 'inmobiliaria')
      $scope.selObj(patente, tipo);

}


});
