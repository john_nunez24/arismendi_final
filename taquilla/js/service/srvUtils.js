angular.module("taquillApp")
   .service("srvUtils", function($rootScope, $filter, localStorageService,srvRestApi){

     //----------------------------------------------------------------------------

     this.calcularIntereses = function(imes, iano, hmes, hano, base ){
       var historicoIntereses = localStorageService.get('lsIntereses');
       var interesTotal = 0;
       var temp = 0;
       var aux = 0;
       var sw = true;
       while (sw) {
           //busca interes segun año y mes
            var inte = historicoIntereses.filter(function(interes){
                                   if((interes.ano == iano)&&(interes.mes == imes))
                                     return interes;
                                 });
                                 console.log(inte);
          //----------------> si no encuentra el interes se usara el mas reciente
          if( inte.length == 0 )
              inte = historicoIntereses;
              console.log(inte);
          //--------------------> se calcula y se acumula
          interesTotal += ( base * (inte[0].tasa /100) )/12;
           //-----------> condicion de parada parada
          if((imes == hmes)&&(iano == hano))
             sw = false;

          //---------------> incrementamos el mes
          imes++;
          //---------------> si llega hasta 13 incrementamos año y reseteamos mes a 1
          if (imes == 13){
            imes = 1;
            iano++;
          }
        }

        return interesTotal;
     }

//// obtener actividades economicas de una patente dada por el parametro patente_id

     this.getActividadesEconomicas = function(patente_id){
       var data = {};
       var actividadPatentes = [];
       console.log('------------------------------------------------------>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
       console.log(patente_id);
       data.id = patente_id;
      return srvRestApi.findActividadPatente(data);

     }



});
