
angular.module("taquillApp")
.service("srvRestApi", function($rootScope,facGlobal, $http,$q, localStorageService,$location){


  prepareJSON = function(data){

    //-- preparar linea con el JSON para crear una nueva linea
    return data = JSON.stringify(data,
      function( key, value ) {
        if( key === "$$hashKey" ) {
          return undefined;
        }
        return value;
      });
      // --- fin de preparacion de JSON
    }

    //--------------------------------------------------------------------------
    this.postSend = function(data,srvName){
      var url = $location.protocol()+"://"+$location.host()+':'+$location.port()+'/al/public/api/v1/';
      var user = localStorageService.get('lsUsuario');

      if((user == null)||( user == undefined )){
        user = {};
      }
      data.user = user;
      return $http({
        method: 'POST',
        url: url+srvName,
        data: data,
        headers: {
          'Content-Type': 'application/json',
          'token':user.api_token
        }
      });
    }
    //--------------------------------------------------------------
    consult_GetApi = function(txtApi){

      return $http.get($location.protocol()+"://"+$location.host()+':'+$location.port()+'/al/public/'+txtApi);

    };
    //--------------------------------------------------------------
    //------------------------------------------------------------------------------
    this.getSalarioActivo = function(){
      var defered = $q.defer();
      var promise = defered.promise;
      consult_GetApi("api/v1/salario/minimo/actual")
      .then(function success(response) {
        facGlobal.setSalarioActual(response.data);
      }, function error(response) {
        defered.reject(response.data);
      });
      return promise;
    };
    //------------------------------------------------------------------------------
    this.getActividadBase = function(){
      var defered = $q.defer();
      var promise = defered.promise;
      consult_GetApi("api/v1/actividad/base")
      .then(function success(response) {
        facGlobal.setActividadBase(response.data);
      }, function error(response) {
        defered.reject(response.data);
      });
      return promise;
    };

    //--------------valor utmari 19-01-2021 john nunez--------------------------------
    this.getUmariBase = function(){
      var defered = $q.defer();
      var promise = defered.promise;
      consult_GetApi("api/v1/utmari/base")
      .then(function success(response) {
        console.log(response.data);
        facGlobal.setUmariBase(response.data);
      }, function error(response) {
        defered.reject(response.data);
      });
      return promise;
    };
    //------------------------------------------------------------------------------

    //------------------------------------------------------------------------------
    this.getActividadEconomica = function(){
      var defered = $q.defer();
      var promise = defered.promise;
      consult_GetApi("api/v1/actividad/economica")
      .then(function success(response) {
        facGlobal.setActividadEconomica(response.data);
      }, function error(response) {
        defered.reject(response.data);
      });
      return promise;
    };
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    this.getItems = function(){
      var defered = $q.defer();
      var promise = defered.promise;
      consult_GetApi("api/v1/configuracion/items")
      .then(function success(response) {
        facGlobal.setItems(response.data);
        localStorageService.set('lsItems',response.data);
      }, function error(response) {
        defered.reject(response.data);
      });
      return promise;
    };
    //------------------------------------------------------------------------------
    this.getZonas = function(){
      var defered = $q.defer();
      var promise = defered.promise;
      consult_GetApi("api/v1/catastro/zona")
      .then(function success(response) {
        facGlobal.setZonas(response.data);
      }, function error(response) {
        defered.reject(response.data);
      });
      return promise;
    };
    //------------------------------------------------------------------------------
    this.getTipologia = function(){
      var defered = $q.defer();
      var promise = defered.promise;
      consult_GetApi("api/v1/catastro/tipologia")
      .then(function success(response) {
        facGlobal.setTipologias(response.data);
      }, function error(response) {
        defered.reject(response.data);
      });
      return promise;
    };
    //---------------------john nuñez---------------------------------------------------------
    this.getConceptoProcedimiento = function(){
      var defered = $q.defer();
      var promise = defered.promise;
      consult_GetApi("api/v1/ingieneria/procedimientos")
      .then(function success(response) {
        facGlobal.setConceptoProcedimiento(response.data);
      }, function error(response) {
        defered.reject(response.data);
      });
      return promise;
    };
    //---------------------john nuñez---------------------------------------------------------
    this.getCostosConstruccion = function(){
      var defered = $q.defer();
      var promise = defered.promise;
      consult_GetApi("api/v1/ingieneria/costos")
      .then(function success(response) {
        facGlobal.setCostosConstruccion(response.data);
      }, function error(response) {
        defered.reject(response.data);
      });
      return promise;
    };
    //---------------------john nuñez---------------------------------------------------------
    this.getEnumColumnValues = function(tabla,campo){

      return consult_GetApi("api/v1/utilidad/enum/"+tabla+"/"+campo);


    };
   //--------------------------john nuñez --------------------------------------
    this.guardarOrdenIngenieria = function(data){
        return this.postSend(data,'ingieneria/guardar/cobro');
    }

    //------------------------------------------------------------------------------
    this.getDepreciaciones = function(){
      var defered = $q.defer();
      var promise = defered.promise;
      consult_GetApi("api/v1/catastro/depreciaciones")
      .then(function success(response) {
        facGlobal.setDepreciaciones(response.data);
      }, function error(response) {
        defered.reject(response.data);
      });
      return promise;
    };
    //------------------------------------------------------------------------------
    this.getValorSuelos = function(){
      var defered = $q.defer();
      var promise = defered.promise;
      consult_GetApi("api/v1/catastro/valorsuelo")
      .then(function success(response) {
        facGlobal.setValorSuelos(response.data);
      }, function error(response) {
        defered.reject(response.data);
      });
      return promise;
    };
    //------------------------------------------------------------------------------
    this.getAlicuotas = function(){
      var defered = $q.defer();
      var promise = defered.promise;
      consult_GetApi("api/v1/catastro/alicuota")
      .then(function success(response) {
        facGlobal.setAlicuotas(response.data);
      }, function error(response) {
        defered.reject(response.data);
      });
      return promise;
    };

    //------------------------------------------------------------------------------
    this.getDepartamentos = function(){

      return consult_GetApi("api/v1/configuracion/departamento");


    };

    //------------------------------------------------------------------------------
    this.getItem = function(id){

      return consult_GetApi("api/v1/configuracion/item/"+id);

    };
    //------------------------------------------------------------------------------
    this.getUnidad = function(id){

      return consult_GetApi("api/v1/configuracion/unidad/"+id);

    };
    //------------------------------------------------------------------------------
    this.getUnidadCobro = function(){

      return consult_GetApi("api/v1/configuracion/unidadcobro");


    };

    //------------------------------------------------------------------------------
    this.getUnidadValor = function(id){

      return consult_GetApi("api/v1/configuracion/unidadvalor/"+id);


    };
    //------------------------------------------------------------------------------
    this.getDepartamentos = function(){

      return consult_GetApi("api/v1/configuracion/departamento");
    };

    //--------------------------------------------------------------------------

    this.guardarItem = function(data){

      return this.postSend(data,'configuracion/guardar/item');
    }
    //-----------------------------john nuñez------------------------------------------
    this.modificarUsuario = function(data){
      return this.postSend(data,'configuracion/modificar/usuario');
    }
    //-----------------------------john nuñez------------------------------------------
    this.eliminarUsuario = function(data){
      return this.postSend(data,'configuracion/eliminar/usuario');
    }
    //-----------------------------john nuñez------------------------------------------
    this.eliminarCatastro = function(data){
      return this.postSend(data,'catastro/patente/eliminar');
    }
    //--------------------------------john nuñez---------------------------------------

    this.usuarios = function(){

      return consult_GetApi("api/v1/configuracion/traer/usuarios");
    }
    //--------------------------------------------------------------------------

    this.guardarLineasItems = function(data){

      return this.postSend(data,'declarar/guardar/item');
    }

    //--------------------------------------------------------------------------

    this.declarar = function(data){

      return this.postSend(data,'declara/old');
    }

    //--------------------------------------------------------------------------

    this.guardarReciboPublicidad2 = function(data){

      return this.postSend(data,'publicidad/recibo/guardar');
    }
    //-------------------------john nunez 13-02-2020-------------------------------------------------

    this.nuevoMontoUnidad = function(data){

      return this.postSend(data,'configuracion/unidad/nuevoMonto'); 
    }

    //--------------------------------------------------------------------------

    this.getPublicidades = function(data){

      return this.postSend(data,'publicidad/patente');
    }
    //--------------------------------------------------------------------------

    this.agregarPublicidad = function(data){

      return this.postSend(data,'publicidad/patente/agregar');
    }

    //--------------------------------------------------------------------------

    this.agregarLicores = function(data){

      return this.postSend(data,'licores/patente/agregar');
    }
    //--------------------------------------------------------------------------
    this.getLicores = function(data){

      return this.postSend(data,'licores/patente');
    }
    //---------------------------------------------------------------
    this.updatePersonaPatente = function(data){

      return this.postSend(data,'persona/patente/guardar');
    }

    //---------------------------------------------------------------
    this.updateDeclaracion = function(data){

      return this.postSend(data,'declara/update');
    }

    //---------------------------------------------------------------
    this.totalCatastro = function(data){

      return this.postSend(data,'catastro/reporte/totales');
    }
    //--------------------------------------------------------------
    this.getIngTasas = function(){

      var url = $location.protocol()+"://"+$location.host()+':'+$location.port()+'/al/public';
      //var user = localStorageService.get('lsakweb');

      return $http({
        method: 'POST',
        url: url+'/api/v1/tasa/ingieneria',
        data: {},
        headers: {
          'Content-Type': 'application/json',

        }
      });

    }

    //--------------------------------------------------------------
    this.guardarFicha = function(data){
      return this.postSend(data,'catastro/guardar');
    }

    //--------------------------------------------------------------
    this.findFicha = function(data){
      return this.postSend(data,'catastro/find');
    }
    //--------------------------------------------------------------
    this.findFichaByPatente = function(data){
      return this.postSend(data,'catastro/patente/find');
    }
    //--------------------------------------------------------------
    this.depreciacion = function(data){
      var url = $location.protocol()+"://"+$location.host()+':'+$location.port()+'/al/public';
      var user = localStorageService.get('lsakweb');

      return $http({
        method: 'POST',
        url: url+'/api/v1/catastro/depreciacion',
        data: data,
        headers: {
          'Content-Type': 'application/json'
        }
      });
    }
    //--------------------------------------------------------------
    this.getUniTributaria = function(data){
      var url = $location.protocol()+"://"+$location.host()+':'+$location.port()+'/al/public';
      var user = localStorageService.get('lsakweb');

      return $http({
        method: 'POST',
        url: url+'/api/v1/unidad/tributaria',
        data: data,
        headers: {
          'Content-Type': 'application/json'
        }
      });
    }
    //--------------------------------------------------------------
    this.guardarReciboCatastro = function(data){
      return this.postSend(data,'catastro/recibo/guardar');
    }
    //----------------------john nuñez----------------------------------------
    this.cargaReciboManual = function(data){
      return this.postSend(data,'catastro/recibo/manual');
    }
    //--------------------------------------------------------------
    this.guardarOrdenCatastro = function(data){
      return this.postSend(data,'catastro/orden/guardar');
    }
    //----tasa/publicidad/recibo/guardar----------------------------------------------------------
    this.guardarReciboTasa = function(data){
      return this.postSend(data,'tasa/administrativa/recibo/guardar');
    }
    //--------------------------------------------------------------
    this.guardarReciboPublicidad = function(data){
      return this.postSend(data,'tasa/publicidad/recibo/guardar');
    }
    //--------------------------------------------------------------
    this.guardarReciboLicores = function(data){
      return this.postSend(data,'licores/recibo/guardar');
    }
    //---------------------------- ----------------------------------
    this.reImprimir = function(data){
      return this.postSend(data,'recibo/reimprimir');
    }
    //----------------------------------------------------------

    this.tasas = function(){
      var url = $location.protocol()+"://"+$location.host()+':'+$location.port()+'/al/public';
      var user = localStorageService.get('lsUsuario');

      return $http({
        method: 'POST',
        url: url+'/api/v1/tasa/administrativa',
        data: {},
        headers: {
          'Content-Type': 'application/json'
        }
      });
    }

    //----------------------------------------------------------

    this.pubTasas = function(){
      var url = $location.protocol()+"://"+$location.host()+':'+$location.port()+'/al/public';
      var user = localStorageService.get('lsUsuario');

       $http({
        method: 'POST',
        url: url+'/api/v1/tasa/publicidad',
        data: {},
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then(function(res){facGlobal.setTasaPublicidad(res.data);

       });
    }
    //----------------------------------------------------------

    this.diaHabil = function(){
      var url = $location.protocol()+"://"+$location.host()+':'+$location.port()+'/al/public';
      var user = localStorageService.get('lsUsuario');

      return $http({
        method: 'POST',
        url: url+'/api/v1/day/work',
        data: {},
        headers: {
          'Content-Type': 'application/json'
        }
      });
    }
    //----------------------------------------------------------

    this.actualPayDate = function(){
      var url = $location.protocol()+"://"+$location.host()+':'+$location.port()+'/al/public';
      var user = localStorageService.get('lsUsuario');

      return $http({
        method: 'POST',
        url: url+'/api/v1/actual/pay/period',
        data: {},
        headers: {
          'Content-Type': 'application/json'
        }
      });
    }
    //----------------------------------------------------------

    this.globalParams = function(){
      var url = $location.protocol()+"://"+$location.host()+':'+$location.port()+'/al/public';
      var user = localStorageService.get('lsUsuario');

      return $http({
        method: 'POST',
        url: url+'/api/v1/global/params',
        data: {},
        headers: {
          'Content-Type': 'application/json'
        }
      });
    }
    //----------------------------------------------------------

    this.licTasas = function(){
      var url = $location.protocol()+"://"+$location.host()+':'+$location.port()+'/al/public';
      var user = localStorageService.get('lsUsuario');

      return $http({
        method: 'POST',
        url: url+'/api/v1/tasa/licor',
        data: {},
        headers: {
          'Content-Type': 'application/json'
        }
      });
    }
    //------------------------------------------------------------------------------
    this.actividadesEconomicas = function(data){
      return this.postSend(data,'actividad/economica');
    }

    //------------------------------------------------------------------------------
    this.cambiarClave = function(data){
      return this.postSend(data,'cambiar/clave');
    }
    //------------------------------------------------------------------------------
    this.asociarActividad = function(data){
      return this.postSend(data,'actividad/economica/asociar');
    }

    //------------------------------------------------------------------------------
    this.findActividadPatente = function(data){
      return this.postSend(data,'actividad/economica/find');
    }
    //------------------------------------------------------------------------------

    this.findPatente = function( data){
      return this.postSend(data,'patente/old');
    }

    //------------------------------------------------------------------------------

    this.findPersona = function( data){
      return this.postSend(data,'persona/find');
    }
    //------------------------------------------------------------------------------

    this.findPersonaOnly = function( data){
      return this.postSend(data,'persona/find/only');
    }
    //------------------------------------------------------------------------------

    this.pagar = function( data){
      return this.postSend(data,'recibo/pagar');
    }

    //------------------------------------------------------------------------------

    this.nuevoContribuyente = function( data){

      return this.postSend(data,'contribuyente/guardar');
    }

    //------------------------------------------------------------------------------

    this.findRecibos = function( data){
      return this.postSend(data,'declara/find/recibo');
    }
    //------------------------------------------------------------------------------

    this.eliminarActividad = function( data){
      return this.postSend(data,'actividad/economica/eliminar');
    }

    //------------------------------------------------------------------------------
    this.eliminarRecibo = function( data){
      return this.postSend(data,'declara/eliminar/recibo');
    }
    //--------------------------john nuñez --------------------------------------
    this.crearNewUser = function(data){
        return this.postSend(data,'configuracion/crear/usuario');
    }
   //--------------------------john nuñez --------------------------------------
    this.reporteRecaudacionPeriodo = function(data){
        return this.postSend(data,'reporte');
    }
    //--------------------------john nuñez --------------------------------------
    this.guardarOrdenIngenieria = function(data){
        return this.postSend(data,'ingieneria/guardar/cobro');
    }
    //--------------------------john nunez--------------------------------------
    //--------------------------john nunez--------------------------------------
    this.findDepartamento = function(data){
        return this.postSend(data,'contribuyente/departamentos');
    }

    //--------------------------john nuñez --------------------------------------
    this.buscarPatenteVincular = function(data){
      return this.postSend(data,'catastro/buscarVincular')
    }
    //---------------john nuñez ------------------------------------------------
    this.patentesAsociadas = function(data){
      return this.postSend(data,'declara/vinculacion/Patentevinculada')
    }
    //---------------john nuñez ------------------------------------------------
    this.patentesContrIngeneria = function(data){
      return this.postSend(data,'ingieneria/contribuyente/Patente')
    }
    //---------------john nuñez ------------------------------------------------
    this.vinculacionPatentes = function(data){
       return this.postSend(data,'declara/vinculacion/patente')
    }
    //---------------john nuñez ------------------------------------------------
    this.eliminarVinculacion = function(data){
       return this.postSend(data,'declara/vinculacion/eliminar/patente')
    }
  //---------------------------john nuñez----------------------------------------
  this.patentesPersona = function(data){
      return this.postSend(data,'contribuyente/buscar/patente')
  }
  //---------------------------john nuñez---------------------------------------------------
  this.buscarContribuyente = function(data){
      return this.postSend(data,'contribuyente/buscar')
  }
  //----------------------------john nuñez --------------------------------------
  this.agregarVehiculo = function(data){
      return this.postSend(data, 'vehiculo/agregar')
  }
  //---------------------------john nuñez----------------------------------------
    this.crearFicha = function( data){
      return this.postSend(data,'catastro/crear');
    }
    //---------------------------john nuñez---------------------------------------------------

    this.crearNewPatente = function( data){
      return this.postSend(data,'declara/crear');//ojo cambiar ruta
    }
    //---------------------------john nuñez---------------------------------------------------

    this.comentarioPatenteAnulada = function( data){
      return this.postSend(data,'catastro/patente/eliminar/comentario');//creado por john nuñez
    }
//---------------------------john nuñez---------------------------------------------------

    this.activarCatastro = function( data){
      return this.postSend(data,'catastro/patente/activar');//creado por john nuñez
    }
//-----------------------------------john nuñez ---------------------------------------
this.buscarPersonaId = function(data){
      return this.postSend(data, 'contribuyente/buscar/persona' );
}
//-----------------------------john nuñez-------------------------------------------------
this.buscarNumeroPatente = function(data){
    return this.postSend(data, 'buscar/linea/numeroPatente')
}
//---------------------------------john nuñez-----------------------------------
this.buscarPatentesPadre = function(data){
      return this.postSend(data, 'catastro/buscar/patentePadres')
}
//---------------------------------john nuñez-----------------------------------
    this.editarFicha = function( data){
      return this.postSend(data,'catastro/editar');
    }
    //---------------------------------john nuñez-----------------------------------
        this.editarFichaCatastral = function( data){
          return this.postSend(data,'catastro/editar/fichaCatastral');
        }
    //--------------------------------------------------------------------------
    this.findLinea = function( data){
      return this.postSend(data,'declara/find/recibo/linea');
    }

    this.dateServer = function( dat){
      var url = $location.protocol()+"://"+$location.host()+':'+$location.port()+'/al/public';
      //var user = localStorageService.get('lsakweb');

      return $http({
        method: 'GET',
        url: url+'/date',
        data: dat,
        headers: {
          'Content-Type': 'application/json',
          'token': 'user.api_token'
        }
      });
    }

    this.historicoUtributaria = function( dat){
      return this.postSend(dat,'unidad/tributaria/historico');
    }

    this.logout = function( dat){
      return this.postSend(dat,'logout');
    }

    this.getIntereses = function( dat){
      var url = $location.protocol()+"://"+$location.host()+':'+$location.port()+'/al/public';
      //var user = localStorageService.get('lsakweb');

      return $http({
        method: 'POST',
        url: url+'/api/v1/interes/bcv',
        data: dat,
        headers: {
          'Content-Type': 'application/json',
          'token': 'user.api_token'
        }
      });
    }



    this.getToken = function(dat){
      var url = $location.protocol()+"://"+$location.host()+':'+$location.port()+'/al/public';

      return $http({
        method: 'POST',
        url: url+'/api/v1/loginSystem',
        data: dat,
        headers: {
          'Content-Type': 'application/json'
        }
      });
    }

  });
