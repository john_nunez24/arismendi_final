var app = angular.module('taquillApp');

app.controller('ctrPubTasa', function($scope,$mdDialog, $filter,$mdToast,$location, $state, localStorageService, srvRestApi){
  $scope.buscarContri = true
  $scope.persona = {};
  $scope.buscar = {};
  $scope.persona.correo = "";
  $scope.persona.telefono = "";
  $scope.permitirGuardar = true;
  $scope.tasasAplicadas = [];
  $scope.patente = localStorageService.get('lspatente');
  $scope.tasas = localStorageService.get('lsTasasPublicidad');
  $scope.uTributaria = localStorageService.get('lsUtributariaActiva');
  $scope.pubContribuyente = localStorageService.get('lsPubContribuyente');
  $scope.editFactor = true;
  $scope.tasaIngSelected = {};
  $scope.etiqueta = "";
  $scope.tasaPublicidad = {};
  $scope.pagaAnos = ['2017','2018'];
  $scope.grandTotal = localStorageService.get('lsPubGrandTotal');



  //$scope.pubContribuyente = {};
  $scope.tasaPublicidesAnual = $scope.tasas.filter(function(tas){
    if(tas.tipo == 'Publicidad')
    return tas;
  });

  //----------------------------------------------------------------------------

  $scope.addPublicidad = function(ev){

    $mdDialog.show({
      templateUrl: 'temp/publicidad/agregarPublicidad.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
    }).then(function(){
        $scope.getPublicidadesPatente();
    },function(){
        $scope.getPublicidadesPatente();
    });

  }


  //------------------------------------------------------------------------------
  $scope.guardarRecibo = function(){
    var data = {};
    data.patente = $scope.patente;
    data.usuario =  localStorageService.get('lsUsuario');
    data.lineas = $scope.tasasAplicadas;

    srvRestApi.guardarReciboPublicidad(data)
    .then(function(response){
      alert('La orden de cobro fue creada con exito. ');
      $scope.buscarContri = true
      $scope.persona = {};
      $scope.buscar = {};
      $scope.permitirGuardar = true;
      $scope.tasasAplicadas = [];
      $scope.cerrarFormulario();
      $scope.mostrarContri = false;


    },function(response){
      alert('No se pudo guardar la orden de cobro, comuniquese con el administrador y reporte la insidencia, 1510')
    });


  }

//------------------------------------------------------------------------------
$scope.getNombreTasa = function(id){
    var temp = $scope.tasas.filter(function(tasa){
                   if(tasa.id == id)
                   return tasa;
                  });
   return temp[0].nombre;
}

//------------------------------------------------------------------------------
$scope.getUtTasa = function(id){
    var temp = $scope.tasas.filter(function(tasa){

                   if(tasa.id == id)
                   return tasa;
                  });

   return temp[0].ut;
}

//------------------------------------------------------------------------------
$scope.getEtiquetaTasa = function(id){
    var temp = $scope.tasas.filter(function(tasa){

                   if(tasa.id == id)
                   return tasa;
                  });

   return temp[0].etiqueta;
}
  //------------------------------------------------------------------------------
  $scope.calcularTasa = function(){

    var tasa = $scope.tasas.filter(function(tasa){
      if(tasa.id == $scope.selTasa)
      return tasa;
    });
    $scope.editFactor = true;
    $scope.etiqueta = "";
    $scope.tasaPublicidad = tasa[0];
    if(tasa[0].etiqueta == 'UT'){
      $scope.subtotal = tasa[0].ut*$scope.uTributaria.base;
    }else {
      $scope.etiqueta = " - "+tasa[0].etiqueta ;
      $scope.showAyuda;
      $scope.factor = 1;

      $scope.showAyuda();
      $scope.editFactor = false;
    }
    $scope.totalizarGlobales();
  }

  //------------------------------------------------------------------------------
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  //------------------------------------------------------------------------------
  $scope.cambiarFactor = function(){

    $scope.subtotal = (Math.ceil($scope.factor*($scope.tasaPublicidad.factor*1))*$scope.tasaPublicidad.ut)*$scope.uTributaria.base;
  }


  $scope.agregarPublicidad = function(){

    $scope.editFactor = true;

    var publicidad = {};
    publicidad.pubtasa_id = $scope.selTasa;
    publicidad.factor = $scope.factor;
    publicidad.direccion = $scope.direccion;
    publicidad.tipo = 'Publicidad';
    publicidad.patente = $scope.patente;
    srvRestApi.agregarPublicidad(publicidad)
    .then(function(response){
      if(confirm('Se agrego la Publicidad correctamente, desea agregar otra publicidad?')){
        $scope.factor = undefined;
        $scope.direccion = undefined;
        $scope.selTasa = undefined;
      }else{
        //------------------------------------------------------------------------------
        $scope.cancel();
      }

    },function(response){
      alert('No se pudo agregar Publicidad')
    });



  }

  //----------------------------------------------------------------------------

  $scope.eliminarTasa = function(tasaBorrar){
    var temTasa = [];
    $scope.tasasAplicadas.forEach(function(tasa){
      if(tasa['id']!= tasaBorrar['id'])
      temTasa.push(tasa);
    });

    $scope.tasasAplicadas = temTasa;
    $scope.totalizarGlobales();
    if($scope.tasasAplicadas.length == 0)
    $scope.permitirGuardar = true;

  }

  //----------------------------------------------------------------------------

  $scope.totalizarGlobales = function(){
    $scope.totalPagar = 0;
    $scope.tasasAplicadas.forEach(function(tasa){

      $scope.totalPagar = $scope.totalPagar*1 + tasa['subtotal']*1;

    });
  }

  //------------------------------------------------------------------------------
  $scope.showAyuda = function() {


    $mdToast.show(
      $mdToast.simple()
      .textContent('Debe ingresar un Factor según cada caso')
      .position('bottom right' )
      .hideDelay(5000)
    );
  };

  $scope.buscarPatente = function(){
    if(($scope.numero != undefined)&&($scope.numero != ""))
    {

      $scope.patente = {};
      $scope.patente.tipo = 'comercio';
      $scope.patente.patente = $scope.numero;
      $scope.patente.direccion = '';
      $scope.patente.nombre = '';
      //-----------------------------------
      srvRestApi.findPatente($scope.patente)
      .then(function(response){
        $scope.patentes = response.data.data;
      },function(response){

      });
    }}

    //----------------------------------------------------------------------------

    $scope.getPublicidadesPatente = function(pat){
      var data ={};
      data.patente = localStorageService.get('lspatente');
      srvRestApi.getPublicidades(data)
      .then(
        function(response){
          localStorageService.set('lsPubContribuyente',response.data);
          $scope.pubContribuyente = response.data;
          var auxPub = response.data.filter(function(pub){
                           if(pub.status =='Activa'){
                             pub.subtotal = (pub.factor*1)*($scope.getUtTasa(pub.pubtasa_id)*1)*$scope.uTributaria.base;
                             $scope.grandTotal = $scope.grandTotal + pub.subtotal;}
                          });



          localStorageService.set('lsPubContribuyente',response.data);
          localStorageService.set('lsPubGrandTotal',$scope.grandTotal);
        },
        function(response){

        });
      }

      //----------------------------------------------------------------------------

      $scope.selObj = function(obj){
        $scope.grandTotal = 0;
        var data ={};
        data.patente = obj;
        srvRestApi.getPublicidades(data)
        .then(
          function(response){

            localStorageService.set('lspatente',null);
            localStorageService.set('lspatente',obj);

            var auxPub = response.data.filter(function(pub){
                             if(pub.status =='Activa'){
                               pub.subtotal = (pub.factor*1)*($scope.getUtTasa(pub.pubtasa_id)*1)*$scope.uTributaria.base;
                               $scope.grandTotal = $scope.grandTotal + pub.subtotal;}
                            });



            localStorageService.set('lsPubContribuyente',response.data);
            localStorageService.set('lsPubGrandTotal',$scope.grandTotal);
            $state.go('verPublicidad');
          },
          function(response){

          });

        }

        //----------------------------------------------------------------------

        $scope.guardarReciboCobro =function(){
          var data = {};
          data.publicidades = localStorageService.get('lsPubContribuyente');
          data.ano = $scope.selAno;
          data.patente = $scope.patente;

          srvRestApi.guardarReciboPublicidad2(data)
                    .then(function(response){
                       alert(response.data.success);
                       $scope.cancel();

                    },
                      function(response){
                       alert(response.data.error);
                      });
        }
        //----------------------------------------------------------------------------

        $scope.editarContribuyente = function(ev){

          $mdDialog.show({

            templateUrl: 'temp/declarar/editarContribuyente.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
          }).then(function(){
            $scope.patente = localStorageService.get('lspatente');

          },function(){
            $scope.patente = localStorageService.get('lspatente');
          });

        }

        //----------------------------------------------------------------------------

        $scope.generarRecibo = function(ev){

          $mdDialog.show({

            templateUrl: 'temp/publicidad/crearOrden.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
          }).then(function(){
            $scope.patente = localStorageService.get('lspatente');

          },function(){
            $scope.patente = localStorageService.get('lspatente');
          });

        }
      });
