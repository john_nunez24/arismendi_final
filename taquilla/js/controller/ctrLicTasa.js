var app = angular.module('taquillApp');

app.controller('ctrLicTasa', function($scope,$mdDialog, $filter,$mdToast,$location, $state, localStorageService, srvRestApi){
  $scope.buscarContri = true
  $scope.persona = {};
  $scope.buscar = {};
  $scope.persona.correo = "";
  $scope.persona.telefono = "";
  $scope.permitirGuardar = true;
  $scope.tasasAplicadas = [];
  $scope.patente = localStorageService.get('lspatente');
  $scope.tasas = localStorageService.get('lsTasasLicor');
  $scope.uTributaria = localStorageService.get('lsUtributariaActiva');
  $scope.licContribuyente = localStorageService.get('lsLicContribuyente');
  $scope.editFactor = true;
  $scope.tasaIngSelected = {};
  $scope.etiqueta = "";
  $scope.tasaPublicidad = {};
  $scope.pagaAnos = ['2017','2018'];
  $scope.grandTotal = localStorageService.get('lsLicGrandTotal');



  //$scope. = {};
  $scope.tasaLicoresAnual = $scope.tasas.filter(function(tas){
    if(tas.tipo == 'Licor')
    return tas;
  });

  //----------------------------------------------------------------------------

  $scope.addLicores = function(ev){

    $mdDialog.show({
      templateUrl: 'temp/licor/agregarLicor.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
    }).then(function(){
      console.log('del bien');
        $scope.getLicoresPatente();
    },function(){
      console.log('del error');
        $scope.getLicoresPatente();
    });

  }


  //------------------------------------------------------------------------------
  $scope.guardarRecibo = function(){
    var data = {};
    data.patente = $scope.patente;
    data.usuario =  localStorageService.get('lsUsuario');
    data.lineas = $scope.tasasAplicadas;

    srvRestApi.guardarReciboPublicidad(data)
    .then(function(response){
      alert('La orden de cobro fue creada con exito. ');
      $scope.buscarContri = true
      $scope.persona = {};
      $scope.buscar = {};
      $scope.permitirGuardar = true;
      $scope.tasasAplicadas = [];
      $scope.cerrarFormulario();
      $scope.mostrarContri = false;


    },function(response){
      alert('No se pudo guardar la orden de cobro, comuniquese con el administrador y reporte la insidencia, 1510')
    });


  }

//------------------------------------------------------------------------------
$scope.getNombreTasa = function(id){
    var temp = $scope.tasas.filter(function(tasa){
                   if(tasa.id == id)
                   return tasa;
                  });
   return temp[0].nombre;
}
//------------------------------------------------------------------------------
$scope.getFactorTasa = function(id){
    var temp = $scope.tasas.filter(function(tasa){
                   if(tasa.id == id)
                   return tasa;
                  });
   return temp[0].factor;
}

//------------------------------------------------------------------------------
$scope.getUtTasa = function(id){
    var temp = $scope.tasas.filter(function(tasa){
                   if(tasa.id == id)
                   return tasa;
                  });
  return temp[0].ut;
}

//------------------------------------------------------------------------------
$scope.getEtiquetaTasa = function(id){
    var temp = $scope.tasas.filter(function(tasa){
                   if(tasa.id == id)
                   return tasa;
                  });
   return temp[0].etiqueta;
}
  //------------------------------------------------------------------------------
  $scope.calcularTasa = function(){

    var tasa = $scope.tasas.filter(function(tasa){
      if(tasa.id == $scope.selTasa)
      return tasa;
    });
    $scope.editFactor = true;
    $scope.etiqueta = "";
    $scope.tasaPublicidad = tasa[0];
    console.log($scope.tasaPublicidad);
    if(tasa[0].etiqueta == 'UT'){
      $scope.subtotal = tasa[0].ut*$scope.uTributaria.base;
      console.log($scope.subtotal);
    }else {
      $scope.etiqueta = " - "+tasa[0].etiqueta ;
      $scope.showAyuda;
      $scope.factor = 1;

      $scope.showAyuda();
      $scope.editFactor = false;
    }
    $scope.totalizarGlobales();
  }

  //------------------------------------------------------------------------------
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  //------------------------------------------------------------------------------
  $scope.cambiarFactor = function(){
    console.log($scope.tasaPublicidad);
    $scope.subtotal = (Math.ceil($scope.factor*($scope.tasaPublicidad.factor*1))*$scope.tasaPublicidad.ut)*$scope.uTributaria.base;
  }


  $scope.agregarLicores= function(){

    $scope.editFactor = true;

    var licores = {};
    licores.lictasa_id = $scope.selTasa;
    licores.factor = $scope.factor;

    licores.tipo = 'Licores';
    licores.patente = $scope.patente;
    srvRestApi.agregarLicores(licores)
    .then(function(response){
      if(confirm('Se agrego tasa de licores correctamente, desea agregar otra tasa?')){
        $scope.factor = undefined;
        $scope.direccion = undefined;
        $scope.selTasa = undefined;
      }else{
        //------------------------------------------------------------------------------
        $scope.cancel();
      }

    },function(response){
      alert('No se pudo agregar Publicidad')
    });



  }

  //----------------------------------------------------------------------------

  $scope.eliminarTasa = function(tasaBorrar){
    var temTasa = [];
    $scope.tasasAplicadas.forEach(function(tasa){
      if(tasa['id']!= tasaBorrar['id'])
      temTasa.push(tasa);
    });licor.lictasa_id

    $scope.tasasAplicadas = temTasa;
    $scope.totalizarGlobales();
    if($scope.tasasAplicadas.length == 0)
    $scope.permitirGuardar = true;

  }

  //----------------------------------------------------------------------------

  $scope.totalizarGlobales = function(){
    $scope.totalPagar = 0;
    $scope.tasasAplicadas.forEach(function(tasa){

      $scope.totalPagar = $scope.totalPagar*1 + tasa['subtotal']*1;

    });
  }

  //------------------------------------------------------------------------------
  $scope.showAyuda = function() {


    $mdToast.show(
      $mdToast.simple()
      .textContent('Debe ingresar un Factor según cada caso')
      .position('bottom right' )
      .hideDelay(5000)
    );
  };

  $scope.buscarPatente = function(){
    if(($scope.numero != undefined)&&($scope.numero != ""))
    {

      $scope.patente = {};
      $scope.patente.tipo = 'comercio';
      $scope.patente.patente = $scope.numero;
      $scope.patente.direccion = '';
      $scope.patente.nombre = '';
      //-----------------------------------
      srvRestApi.findPatente($scope.patente)
      .then(function(response){
        $scope.patentes = response.data.data;
      },function(response){

      });
    }}

    //----------------------------------------------------------------------------

    $scope.getLicoresPatente = function(pat){
      var data ={};
      console.log('llega aqui');
      data.patente = localStorageService.get('lspatente');
      srvRestApi.getLicores(data)
      .then(
        function(response){

          localStorageService.set('lsLicContribuyente',response.data);
          $scope.licContribuyente = response.data;
          response.data.filter(function(lic){
                           if(lic.status =='Activa'){
                             $factor = $scope.getFactorTasa(lic.lictasa_id);
                             if( $factor == 0 )
                                lic.subtotal = ($scope.getUtTasa(lic.lictasa_id)*1)*$scope.uTributaria.base;
                              else
                                lic.subtotal = ($factor*lic.factor*1)*($scope.getUtTasa(lic.lictasa_id)*1)*$scope.uTributaria.base;
                             $scope.grandTotal = $scope.grandTotal +lic.subtotal;}
                          });



          localStorageService.set('lsLicGrandTotal',$scope.grandTotal);
        },
        function(response){

        });
      }

      //----------------------------------------------------------------------------

      $scope.selObj = function(obj){
        $scope.grandTotal = 0;
        var data ={};
        data.patente = obj;
        srvRestApi.getLicores(data)
        .then(
          function(response){

            localStorageService.set('lspatente',null);
            localStorageService.set('lspatente',obj);

            var auxLic= response.data.filter(function(lic){
                             if(lic.status =='Activa'){
                               $factor = $scope.getFactorTasa(lic.lictasa_id);
                               if( $factor == 0 )
                                  lic.subtotal = ($scope.getUtTasa(lic.lictasa_id)*1)*$scope.uTributaria.base;
                                else
                                  lic.subtotal = ($factor*lic.factor*1)*($scope.getUtTasa(lic.lictasa_id)*1)*$scope.uTributaria.base;
                               $scope.grandTotal = $scope.grandTotal + lic.subtotal;}
                            });



            localStorageService.set('lsLicContribuyente',response.data);
            localStorageService.set('lsLicGrandTotal',$scope.grandTotal);
            $state.go('verLicor');
          },
          function(response){

          });

        }

        //----------------------------------------------------------------------

        $scope.guardarReciboCobro =function(){
          var data = {};
          data.licores = localStorageService.get('lsLicContribuyente');
          data.ano = $scope.selAno;
          data.patente = $scope.patente;

          srvRestApi.guardarReciboLicores(data)
                    .then(function(response){
                       alert(response.data.success);
                       $scope.cancel();

                    },
                      function(response){
                       alert(response.data.error);
                      });
        }
        //----------------------------------------------------------------------------

        $scope.editarContribuyente = function(ev){

          $mdDialog.show({

            templateUrl: 'temp/declarar/editarContribuyente.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
          }).then(function(){
            $scope.patente = localStorageService.get('lspatente');

          },function(){
            $scope.patente = localStorageService.get('lspatente');
          });

        }

        //----------------------------------------------------------------------------

        $scope.generarRecibo = function(ev){

          $mdDialog.show({

            templateUrl: 'temp/licor/crearOrden.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
          }).then(function(){
            $scope.patente = localStorageService.get('lspatente');

          },function(){
            $scope.patente = localStorageService.get('lspatente');
          });

        }
      });
