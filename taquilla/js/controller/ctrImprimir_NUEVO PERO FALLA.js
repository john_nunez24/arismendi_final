var app = angular.module('taquillApp' );

app.controller('ctrImprimir', function($scope, $timeout,$window,$state, localStorageService, srvRestApi, $filter){
  var declaracion = localStorageService.get('lsDeclaracion');
$scope.mostrarPeriodo = false;
$scope.mostrarPeriodo2 = false;
$scope.esCatastro = false;
$scope.esAgrupado = false;
  $scope.patente = declaracion.patente;
  $scope.pago = declaracion.pago;
  $scope.created_at = $scope.pago['created_at'];
  $scope.recibo = declaracion.recibo;
  $scope.persona = declaracion.persona;
  $scope.lineas = declaracion.lineas;
  $scope.usuario = declaracion.usuario;
  $scope.allPatentes = declaracion.allPatentes;

 if($scope.allPatentes.length){
   $scope.mostrarPeriodo = true;
   $scope.esAgrupado = true;
 }


$scope.multa = 0;
$scope.descuento = 0;
$scope.interes = 0;
$scope.vigente = 0;
$scope.vigentePublicidad = 0;
$scope.vigenteLicores = 0;
$scope.moroso = 0;
$scope.tbase = 0;
$scope.subTotal = 0;
$scope.totalTotal = 0;

$scope.tasas = localStorageService.get('lsTasasAdministrativas');
$scope.ingtasas = localStorageService.get('lsTasasIngieneria');
$scope.pubtasas = localStorageService.get('lsTasasPublicidad');
$scope.lictasas = localStorageService.get('lsTasasLicor');
$scope.getNombreTasa = function (linea){

  var tasaLista = [];

  if(linea.tipo == 'Tasa')
     tasaLista = $scope.tasas;
  if(linea.tipo == 'Ingieneria')
     tasaLista = $scope.ingtasas;
  if(linea.tipo == 'Publicidad')
        tasaLista = $scope.pubtasas;
  if(linea.tipo == 'Licores')
         tasaLista = $scope.pubtasas;

  var tasa = tasaLista.filter(function(tasa){

    if(tasa.id == linea.tasa_id)
    return tasa;
  });

  return tasa[0].nombre;
}





$scope.lineas.forEach(function(linea){
     $scope.multa = $scope.multa + linea['multa']*1;
     $scope.interes = $scope.interes + linea['interes']*1;
     $scope.descuento = $scope.descuento + linea['descuento']*1;
     $scope.subTotal = ($scope.subTotal*1) + (linea['subTotal']*1);
     $scope.totalTotal = (($scope.totalTotal*1) + (linea['total']*1));
     $scope.tbase = ($scope.tbase*1 + linea['base']*1);

     if((linea['tipo'] == 'Declaracion')&&(linea['interes']=='0')&&(linea['multa']=='0'))
         $scope.vigente = $scope.vigente*1 + linea['subTotal']*1;
     if((linea['tipo'] == 'Declaracion')&&((linea['interes']!='0')||(linea['multa']!='0')))
             $scope.moroso = $scope.moroso*1 + linea['subTotal']*1;
    if(linea['tipo'] == 'Declaracion'){
      $scope.desde = linea['imes'] +'-'+linea['iano'];
      $scope.hasta = linea['hmes'] +'-'+linea['hano'];
      $scope.mostrarPeriodo = true;

    }
    
    if(linea['tipo'] == 'Catastro'){

      if(!$scope.esCatastro )
        $scope.desde = linea['imes'] +'-'+linea['iano'];
      $scope.hasta = linea['hmes'] +'-'+linea['hano'];
      $scope.esCatastro = true;
      $scope.mostrarPeriodo = true;
    }
    if(linea['tipo'] == 'Publicidad'){
      $scope.mostrarPeriodo = true;
      $scope.vigentePublicidad = $scope.vigentePublicidad*1 + linea['subTotal']*1;
      if(!$scope.esCatastro )
        $scope.desde = linea['imes'] +'-'+linea['iano'];
      $scope.hasta = linea['hmes'] +'-'+linea['hano'];

      $scope.mostrarPeriodo2 = true;
    }

    if(linea['tipo'] == 'Licores'){
      $scope.mostrarPeriodo = true;
      $scope.vigenteLicores = $scope.vigenteLicores*1 + linea['subTotal']*1;
      if(!$scope.esCatastro )
        $scope.desde = linea['imes'] +'-'+linea['iano'];
      $scope.hasta = linea['hmes'] +'-'+linea['hano'];

      $scope.esLicor = true;
    }
});





//  $scope.ramo = ($scope.patente.tipo == 'comercio')?'Actividades Economicas':'Propiedad inmobiliaria';

   $scope.printIt = function(){
     html2canvas(document.getElementById('printArea'), {
         onrendered: function (canvas) {
             var data = canvas.toDataURL();
             var docDefinition = {

                 content: [{
                     image: data,
                     width: 500,
                     height:400,

                 }]
             };
             pdfMake.createPdf(docDefinition).open("test.pdf");
         }
     });

   };

 $scope.printIt2 = function(){
   var table = document.getElementById('printArea').innerHTML;
     var myWindow = $window.open('', '', 'width=800, height=600');
     myWindow.document.write(table);
     myWindow.print();

     $state.go('declaracion');

 };


});
