var app = angular.module('taquillApp' );

app.controller('ctrImpEstadoCuenta', function($scope, $timeout,$window,$state, localStorageService, srvRestApi, $filter,facGlobal){

console.log('hola');
$scope.items = localStorageService.get('lsItems');
$scope.estadoCuenta = localStorageService.get('lsEstadoCuenta');
console.log($scope.items);
console.log($scope.estadoCuenta);
$scope.actividadBase = facGlobal.getActividadBase();
console.log($scope.actividadBase);
$scope.actividades = [];
$scope.actividades = facGlobal.getActividadEconomica();
console.log($scope.actividades);
$scope.tasas = localStorageService.get('lsTasasAdministrativas');
$scope.ingtasas = localStorageService.get('lsTasasIngieneria');
$scope.pubtasas = localStorageService.get('lsTasasPublicidad');
$scope.lictasas = localStorageService.get('lsTasasLicor');
$scope.actividades = localStorageService.get('lsActividades');
console.log($scope.actividades);
//------------------------------------------------------------------------------

$scope.auxArray = [];
$scope.auxObj = {};
$scope.totales = {};
$scope.totales.pendientes = 0;
$scope.totales.pagas = 0;
$scope.totales.general = 0;
$scope.arryEstadoCuenta = [];
$scope.auxObj.concepto ='';
$scope.auxObj.recibo = '';
$scope.auxObj.fecha = '';
$scope.auxObj.pendiente = 0;
$scope.auxObj.pagada = 0;
//-----------------------------------------------------------------------------
$scope.buscarConcepto = function(linea){
 var concepto = '';
  if (linea.tipo == 'Item' )
      $scope.items.forEach(function(item){
        if (item.id == linea.item_id)
          concepto = item.concepto;
      });
  if (linea.tipo == 'Tasas' )
      $scope.tasas.forEach(function(tasa){
        if (tasa.id == linea.tasa_id)
            concepto = tasa.concepto;
      });
  if (linea.tipo == 'Declaracion'){

            concepto = 'Declaracion';
    
  }
  return concepto;
}
//----------------funciones de tiporecibo-------------------------------------
$scope.traerPendientes = function(){
  console.log('enpieza la function');
  if ($scope.estadoCuenta.ordenado.cantidad == 0){
    console.log('entro en cantidad 0');
    $scope.auxArray.forEach(function(aux){
      if (aux.pendiente != 0){
            $scope.arryEstadoCuenta.push(aux);
            $scope.totales.pendientes = $scope.totales.pendientes + aux.pendiente;
            $scope.totales.general = $scope.totales.general + aux.totalGeneral;
    }
   });
  }else {
    if($scope.estadoCuenta.ordenado.cantidad >=5){
      console.log('entro en cantidad' + $scope.estadoCuenta.ordenado.cantidad);
      var i=0;
      $scope.auxArray.forEach(function(aux){
        if (aux.pendiente != 0 && i<$scope.estadoCuenta.ordenado.cantidad){
            $scope.arryEstadoCuenta.push(aux);
            i++;
            $scope.totales.general = $scope.totales.general + aux.totalGeneral;
            console.log($scope.arryEstadoCuenta);
            $scope.totales.pendientes = $scope.totales.pendientes + aux.pendiente;
        }
      });

  }
 }

}
//-----------------------------------------------------------------------------
$scope.traerPagadas = function(){
  console.log('enpieza la function');
  if ($scope.estadoCuenta.ordenado.cantidad == 0){

    $scope.auxArray.forEach(function(aux){
      if (aux.pagada != 0){
            $scope.arryEstadoCuenta.push(aux);
            $scope.totales.pagas = $scope.totales.pagas + aux.pagada;
            $scope.totales.general = $scope.totales.general + aux.totalGeneral;
          }
    });
  }else {
    if($scope.estadoCuenta.ordenado.cantidad >=5){
      console.log('entro en cantidad' + $scope.estadoCuenta.ordenado.cantidad);
      var i=0;
      $scope.auxArray.forEach(function(aux){
        if (aux.pagada != 0 && i<$scope.estadoCuenta.ordenado.cantidad){
            $scope.arryEstadoCuenta.push(aux);
            $scope.totales.general = $scope.totales.general + aux.totalGeneral;
            i++;
            $scope.totales.pagas = $scope.totales.pagas + aux.pagada;
            console.log($scope.arryEstadoCuenta);
        }
      });

  }
 }

}
//----------------------------------------------------------------------------
$scope.traerAmbas = function(){
  console.log('enpieza la function');
  if ($scope.estadoCuenta.ordenado.cantidad == 0){
    $scope.auxArray.forEach(function(aux){
            $scope.arryEstadoCuenta.push(aux);
            $scope.totales.general = $scope.totales.general + aux.totalGeneral;
            if (aux.pendiente>0){
                  $scope.totales.pendientes = $scope.totales.pendientes + aux.pendiente;
            }
            if (aux.pagada>0){
                  $scope.totales.pagas = $scope.totales.pagas + aux.pagada;
            }
    });

  }else {
    if($scope.estadoCuenta.ordenado.cantidad >=5){
      var length = $scope.estadoCuenta.ordenado.cantidad;
      for (var i = 0; i < length; i++) {
        $scope.arryEstadoCuenta.push($scope.auxArray[i]);
        $scope.totales.general = $scope.totales.general + $scope.auxArray[i].totalGeneral;
        if ($scope.auxArray[i].pendiente>0){
              console.log('entro en la I ' + i);
              $scope.totales.pendientes = $scope.totales.pendientes + $scope.auxArray[i].pendiente;
              console.log($scope.totales.pendientes);
        }
        if ($scope.auxArray[i].pagada>0){
              console.log('entro en la I ' + i);
              $scope.totales.pagas = $scope.totales.pagas + $scope.auxArray[i].pagada;
              console.log($scope.totales.pagas);
        }
    }
  }
 }

}
//-----------------------------------------------------------------------------
$scope.estadoCuenta.recibos.forEach(function(recibo){
  $scope.auxObj = {};
      $scope.auxObj.concepto = $scope.buscarConcepto(recibo.patente_linea);
      if (recibo.pago == null){
          $scope.auxObj.recibo = recibo.id;
      }else{
            $scope.auxObj.recibo = recibo.pago.pago_id;
          }
      $scope.auxObj.fecha = recibo.created_at.slice(0,-9);
      if(recibo.patente_linea.status == 'Pagada' ){
          $scope.auxObj.pendiente = 0;
          $scope.auxObj.pagada = recibo.totalPagar *1;
      }
      if(recibo.patente_linea.status == 'Pendiente' ){
          $scope.auxObj.pendiente = recibo.totalPagar *1;
          $scope.auxObj.pagada = 0;
      }
      $scope.auxObj.totalGeneral = recibo.totalPagar *1;
      $scope.auxArray.push($scope.auxObj);

});
//----------------------------------------------------------------------------
 if ($scope.estadoCuenta.ordenado.tiposRecibo == 'Pendientes'){
      console.log('paso pendiente');
      $scope.traerPendientes();
 };

 if ($scope.estadoCuenta.ordenado.tiposRecibo == 'Pagadas'){
    $scope.traerPagadas();
 }
 if ($scope.estadoCuenta.ordenado.tiposRecibo == 'Ambos'){
    $scope.traerAmbas();
 }
//--
//------------------------------------------------------------------------------
$scope.printIt2 = function(){
  var table = document.getElementById('printArea').innerHTML;
    var myWindow = $window.open('', '', 'width=800, height=600');
    myWindow.document.write(table);
    myWindow.print();

    //$state.go('declaracion');

};

});
