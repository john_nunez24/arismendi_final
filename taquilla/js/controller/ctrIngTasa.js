var app = angular.module('taquillApp');

app.controller('ctrIngTasa', function($scope, $rootScope,$mdDialog,facGlobal,$filter,$mdToast,$location, $state, localStorageService, srvRestApi){
  $scope.buscarContri = true
  $scope.persona = {};
  $scope.buscar = {};
  $scope.calculos = {};
  $scope.lineas = [];
  $scope.totales = 0;
  $scope.persona.correo = "";
  $scope.persona.telefono = "";
  $scope.verLineas = false;
  //$scope.iccManual = "";
  $scope.permitirGuardar = true;
  $scope.avilitarConstruccion = false;
  $scope.avilitarConsulta = false;
      $scope.fichaVisible = $rootScope.rsFichaVisible;
    console.log($scope.fichaVisible);
  $scope.tasasAplicadas = [];
  //$scope.editarCosto = false;
  $scope.tasas = localStorageService.get('lsTasasAdministrativas');
  $scope.ingtasas = localStorageService.get('lsTasasIngieneria');
      if ($rootScope.rsPatente != null){
          $scope.patente = $rootScope.rsPatente;
          $scope.fichas = $scope.patente.ficha;
        }
      console.log($scope.patente);
      if ($rootScope.rsCalculos != null)
              $scope.calculos = $rootScope.rsCalculos;
      console.log($scope.calculos);
      $scope.tasasIngienieria = facGlobal.getTasasAdministrativasIngieneria();
      $scope.conceptoProcedimientos = facGlobal.getConceptoProcedimiento();
      console.log($scope.conceptoProcedimientos);
      $scope.tipoProcedimientos = [{codigo:'Edificaciones'},{codigo:'Urbanismo'}];

  $scope.costosContruccion= facGlobal.getCostosConstruccion();

srvRestApi.getEnumColumnValues('indice_construccion','tipo')
                                .then(function success(response) {
                                  $scope.tiposCostos = response.data;
                                  console.log($scope.tiposCostos);
                                }, function error(response) {

                                });

  $scope.editFactor = true;
  $scope.tasaIngSelected = {};

  srvRestApi.getUniTributaria()
  .then(function(response){
    $scope.uTributaria = response.data;
  },function(response){
    console.log('no se puedo obtener unidad tributaria');
  });

  if($rootScope.departamentoActual == undefined)
      $rootScope.departamentoActual = {};
   else
      $scope.depActual = $rootScope.departamentoActual;

      //--------------------------------
        $scope.getItemsByPatente = function(id){
            srvRestApi.getItem(id)
               .then(function(resp){
                 $scope.items = resp.data;
                  console.log($scope.items);
                 },function(err){

                });
           }
      //---
      $scope.getItemsByPatente(7);
      console.log($scope.items);
      //------------john nuñez--------18/02/2020----------------------------------------
      $scope.cambiarConcepto = function(item){
            $scope.concepto = item;
         $scope.valorTasa = item.cantidad * item.valorActual.valor;
      }

      //-------------------------
      $scope.agregarLinea = function(){
        var temp = {};
        console.log($scope.patente);
        temp.patente = $scope.patente;
        temp.concepto = $scope.concepto;
        temp.total = $scope.valorTasa;
        //$scope.patentePagar.patente = {};
        $scope.concepto = {};
        $scope.valorTasa = 0;
        $scope.lineas.push(temp);
        $scope.totalTotal = 0;
        $scope.lineas.forEach(function(linea){

          $scope.totalTotal += linea.total;
          $scope.bloquearGuardar = false;
        });

        $scope.totales +=  $scope.totalTotal*1;
        console.log($scope.totales);
        $scope.permitirGuardar = false;

      }
    //-----------------------------john nuñez --------------------------------------

  //------------------------------------------------------------------------------
  $scope.agregarContribuyente = function(){
    $scope.persona = {};
    $scope.crearContri = true;
    $scope.buscarContri = false;
    $scope.mostrarContri = false;

  }
  //------------------------------------------------------------------------------
  $scope.cerrarFormulario = function(){
    $scope.crearContri = false;
    $scope.buscarContri = true;
    $scope.mostrarContri = false;

  }
  //------------------------------------------------------------------------------
  $scope.mostrarFicha = function(){
    $scope.crearContri = false;
    $scope.buscarContri = false;
    $scope.mostrarContri = true;

  }
  //------------------------------------------------------------------------------
/*  $scope.tiposCostos= function(){
    $scope.arreTemp = [];
    console.log('antes de entrar');
    $scope.costosContruccion.forEach(function(costo){
        if ($scope.arreTemp.length == 0){
            console.log('entre al 1');
            $scope.arreTemp.push(costo.tipo);
          }
      log('ahora toca el 2');
               if (arr != costo.tipo)
                    $scope.arreTemp.push(costo.tipo);
             });


    });
    console.log($scope.arreTemp);
    return $scope.arreTemp;
  }*/

  //-----------------------------john nuñez------------------------------------
  $scope.buscarPersona = function(){
    $scope.buscar.id="";
    if($scope.buscar.rid != ""){
      srvRestApi.findPersonaOnly($scope.buscar)
      .then(function(response){

        $scope.patentes = response.data;
        //$scope.mostrarFicha();
      },function(response){
        console.log(response.data.error);
        alert(response.data.error);
      });
    }
  }
//----------------------john nuñez--------------------------------------------
$scope.buscarPatentesCatasIngen = function(){

 if ($scope.buscar.patente != ""){
    srvRestApi.patentesContrIngeneria($scope.buscar)
          .then(function(response){
                $scope.patentes = response.data;
                console.log(response.data);
            },function(response){

            });
 }
}
//---------------------------------john nuñez-----------------------------------
$scope.patentesContribuyente = function(patente){
  console.log('estamos bien' );
  console.log(patente);
  $rootScope.rsPatente = patente;
      if (patente.ficha.length != 0)
              {$rootScope.rsFichaVisible = true;
              console.log('paso 1');}
          else
            {$rootScope.rsFichaVisible = false;
            console.log('paso 2');}


  //$scope.fichas = patente.ficha;
  $state.go('mostrarContribuyente');
  console.log($scope.rsFichaVisible);

}
//---------------------------john nunez ---------------------------------------
$scope.buscarTipos = function(tipo){
  $scope.construccion = [];
  $scope.costosContruccion.forEach(function(costo){
    if (costo.tipo == tipo)
        $scope.construccion.push(costo);

  });
}
//----------------------john nuñez -------------------------------------------
$scope.calcularResultado = function(costo, metro){

  $scope.calculos.patente = $scope.patente;
  $scope.calculos.metroCuadrado = metro;
  $scope.calculos.costoInmueble = 0;
  $scope.calculos.iccUsado = costo.icc;
  $scope.calculos.costoInmueble = ($scope.calculos.iccUsado * 1) * ($scope.calculos.metroCuadrado * 1);
  $scope.tipoNombre = $scope.calculos.procedimiento.nombre;
  $scope.calculos.resuladoProcedimiento = $scope.calculos.costoInmueble * ($scope.calculos.procedimiento.factor * 1);
  $scope.resultado = $scope.calculos.resuladoProcedimiento;
  console.log($scope.calculos);

}
//----------------john nuñez--------------------------------------------------
$scope.calcularResultadoManual = function(costo, metro){
  console.log(costo);
    $scope.calculos.patente = $scope.patente;
    $scope.calculos.metroCuadrado = metro;
    $scope.calculos.costoInmueble = 0;
    $scope.calculos.iccUsado = costo;
    $scope.calculos.costoInmueble = ($scope.calculos.iccUsado * 1) * ($scope.calculos.metroCuadrado * 1);
    $scope.tipoNombre = $scope.calculos.procedimiento.nombre;
    $scope.calculos.resuladoProcedimiento = $scope.calculos.costoInmueble * ($scope.calculos.procedimiento.factor * 1);
    $scope.resultado = $scope.calculos.resuladoProcedimiento;
    console.log($scope.calculos);
}
//----------------JOHN nuñez -------------------------------------------------
$scope.filtrarTipos= function(tipo){
  console.log(tipo);
  console.log($scope.conceptoProcedimientos);
  $scope.procedimientos = [];

  $scope.conceptoProcedimientos.forEach(function(concepto){
      if (concepto.tipo == tipo)
          $scope.procedimientos.push(concepto);

  });
  console.log($scope.procedimientos);
}
//-----------------------john nuñez---------------------------------------------
$scope.calcularProcedimientos= function(concepto){
  console.log(concepto);
  $scope.calculos.procedimiento = concepto;
   if (concepto.calculo == 'Simple' ){
          $scope.avilitarConsulta = true;
          $scope.procesamiento= concepto;
          $scope.avilitarConstruccion = false;
          $scope.calculos.resuladoProcedimiento = (concepto.factor * 1) * 1;//falta variable de la UT
   }
   if (concepto.calculo == 'Construcion' ){
          $scope.avilitarConstruccion = true;

          $scope.avilitarConsulta = false;
   }

}
//-----------------------------john nuñez------------------------------------------
$scope.generaraRecibo = function(calculos, ev){

  console.log(calculos);
  $scope.calculos = calculos;
  $scope.totales = ($scope.totales*1 + $scope.calculos.resuladoProcedimiento*1);
  console.log($scope.totales);
  $scope.verLineas = true;
  $scope.permitirGuardar = false;
  //$scope.proceIngenieria=

  /*$mdDialog.show({
    templateUrl: 'temp/ingieneria/reciboIngieneria.html',
    parent: angular.element(document.body),
    targetEvent: ev,
    clickOutsideToClose:true,
    fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
    //preserveScope: true
  })
  .then(function(answer) {
    console.log('**paso1***');

    //$scope.status = 'You said the information was "' + answer + '".';
  }, function() {
    console.log('**paso2***');

  });*/
}

  //------------------------------------------------------------------------------
  $scope.guardarContribuyente = function(){

    if($scope.persona.correo == undefined)
     $scope.persona.correo = "";
    if($scope.persona.telefono == undefined)
     $scope.persona.telefono = "";
    var datac = {};

    datac['persona'] = $scope.persona;
    datac['usuario'] = localStorageService.get('lsUsuario');


    srvRestApi.nuevoContribuyente(datac)
    .then(function(response){
      $scope.mostrarFicha();
      $scope.selContribuyente = response.data;
    },function(response){

    });
  }
  //------------------------------------------------------------------------------
  $scope.guardarRecibo = function(){
    if(confirm('Esta Seguro de generar la orden de cobro?')){
      var data = {};
        data.patente = $scope.patente;
        data.usuario = localStorageService.get('lsUsuario');
        data.procedimiento = $scope.calculos;
        data.tasas = $scope.lineas;
        console.log(data);
        srvRestApi.guardarOrdenIngenieria(data)
        .then(function(response){

          alert('Se Registro con exito la orden de cobro');
          //$scope.totalCalculos = [];
          $state.go('ingTasas');
        },function(response){
          alert(response.data.error);
        });
      }
    }

  //------------------------------------------------------------------------------
  $scope.calcularTasa = function(){

    var tasa = $scope.tasas.filter(function(tasa){
      if(tasa.id == $scope.selTasa)
      return tasa;
    });

    $scope.tasaAdministrativa = tasa[0];
    $scope.subtotal = tasa[0].unidad*$scope.uTributaria.base;
    $scope.showAyuda;
    $scope.factor = 1;
    if(tasa[0].tipo == 'Factor'){
      $scope.showAyuda();
      $scope.editFactor = false;
    }
    $scope.totalizarGlobales();
  }

  //------------------------------------------------------------------------------
  $scope.calcularIngTasa = function(){
     $scope.editFactor = true;
    var tasa = $scope.ingtasas.filter(function(tasa){
      if(tasa.id == $scope.selIngTasa)
      return tasa;
    });

    $scope.tasaIngSelected = tasa[0];
    if($scope.tasaIngSelected.factor == '0'){
       $scope.ingsubtotal = (tasa[0].ut*1)*$scope.uTributaria.base;
       $scope.ingfactor = tasa[0].ut;

    }

    if($scope.tasaIngSelected.ut === '0'){

       $scope.ingfactor = 1;
       $scope.ingsubtotal = (tasa[0].factor * 1) * $scope.ingfactor;
       console.log($scope.ingsubtotal);
       $scope.showAyuda();
       $scope.editFactor = false;

    }

    $scope.showAyuda;

    if(tasa[0].tipo == 'Factor'){
      $scope.showAyuda();
      $scope.editFactor = false;
    }
  //  $scope.totalizarGlobales();
  }
  //------------------------------------------------------------------------------
  $scope.cambiarFactor = function(){
    $scope.subtotal = $scope.factor*$scope.uTributaria.base;
  }

  //------------------------------------------------------------------------------
  $scope.cambiarIngFactor = function(){
    $scope.ingsubtotal = $scope.ingfactor*  $scope.tasaIngSelected.factor ;
  }
  //----------------------------------------------------------------------------
  $scope.agregarTasa = function(){

    if($scope.tasasAplicadas.length == 4)
    {
      alert('Sólo puede incluir cuatro (4) Tasas Administrativas en un recibo')
    }
    else {
      $scope.permitirGuardar = false;
      $scope.factor = 1;
      var existeTasa = false;
      $scope.tasasAplicadas.forEach(function(tasa){
        if((tasa['id']== $scope.selTasa)&&(tasa['tipo']=='Tasa'))
        existeTasa = true;
      });



      if(!existeTasa)
      {
        $scope.editFactor = true;
        $scope.tasaAdministrativa.subtotal = $scope.subtotal*1;
        $scope.tasaAdministrativa.factor = $scope.factor;
        $scope.tasaAdministrativa.tipo = 'Tasa';
        $scope.tasasAplicadas.push($scope.tasaAdministrativa);
      }
      $scope.totalizarGlobales();
    }

  }
  //----------------------------------------------------------------------------
  $scope.agregarIngTasa = function(){

    if($scope.tasasAplicadas.length == 4)
    {
      alert('Sólo puede incluir cuatro (4) Tasas Administrativas en un recibo')
    }
    else {
      $scope.permitirGuardar = false;
      $scope.factor = 1;
      var existeTasa = false;
      $scope.tasasAplicadas.forEach(function(tasa){
        if((tasa['id']== $scope.selTasa)&&(tasa['tipo']=='Ingieneria'))
        existeTasa = true;
      });



      if(!existeTasa)
      {
        $scope.editFactor = true;
        $scope.tasaIngSelected.subtotal = $scope.ingsubtotal*1;
        $scope.tasaIngSelected.factor = $scope.ingfactor;
        $scope.tasaIngSelected.tipo = 'Ingieneria';
        $scope.tasasAplicadas.push($scope.tasaIngSelected);
      }
      console.log($scope.tasasAplicadas);
      $scope.totalizarGlobales();
    }

  }
  //----------------------------------------------------------------------------

  $scope.eliminarTasa = function(tasaBorrar){
    var temTasa = [];
    $scope.tasasAplicadas.forEach(function(tasa){
      if(tasa['id']!= tasaBorrar['id'])
      temTasa.push(tasa);
    });

    $scope.tasasAplicadas = temTasa;
    $scope.totalizarGlobales();
    if($scope.tasasAplicadas.length == 0)
    $scope.permitirGuardar = true;

  }

  //----------------------------------------------------------------------------

  $scope.totalizarGlobales = function(){
    $scope.totalPagar = 0;
    $scope.tasasAplicadas.forEach(function(tasa){

      $scope.totalPagar = $scope.totalPagar*1 + tasa['subtotal']*1;

    });
      }

      //------------------------------------------------------------------------------
      $scope.showAyuda = function() {


        $mdToast.show(
          $mdToast.simple()
          .textContent('Esta tasa administrativa puede multiplicarla por un Factor según el caso')
          .position('bottom right' )
          .hideDelay(5000)
    );
  };




});
