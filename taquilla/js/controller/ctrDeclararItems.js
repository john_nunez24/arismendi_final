var app = angular.module('taquillApp');

app.controller('ctrDeclararItems', function($scope , $rootScope , $filter , $mdDialog , srvRestApi){
console.log('items');

  $scope.patente = $rootScope.patenteActual;
  $scope.listaPatentes = [];
  $scope.items = [];
  $scope.lineas =[];
  $scope.totalTotal = 0;
  console.log($scope.patente);
  $scope.bloquearGuardar = true;
  $scope.listaPatentes.push( $scope.patente);
  console.log($scope.listaPatentes);
  if ($scope.patente.patenteAsociadas != undefined)
        $scope.patente.patenteAsociadas.forEach(function( patenteAsociada){
          $scope.listaPatentes.push( patenteAsociada);} );

//--------------------------------
  $scope.getItemsByPatente = function(id){
      srvRestApi.getItem(id)
         .then(function(resp){
           $scope.items = resp.data;
            console.log($scope.items);
           },function(err){

          });
     }
//---
  console.log($scope.listaPatentes[0].departamento_id);
  $scope.getItemsByPatente($scope.listaPatentes[0].departamento_id);

//--------------------------
$scope.cambiarPatente = function(){
  $scope.getItemsByPatente($scope.patentePagar.patente.departamento_id);
}


//--------------------------
$scope.cambiarConcepto = function(){
  $scope.patentePagar.total = $scope.patentePagar.concepto.cantidad * $scope.patentePagar.concepto.valorActual.valor;
}
//-------------------------
$scope.agregarLinea = function(){
  var temp = {};
  temp.patente = $scope.patentePagar.patente;
  temp.concepto = $scope.patentePagar.concepto;
  temp.total = $scope.patentePagar.total;
  $scope.patentePagar.patente = {};
  $scope.patentePagar.concepto = {};
  $scope.patentePagar.total = 0;
  $scope.lineas.push(temp);
  $scope.totalTotal = 0;
  $scope.lineas.forEach(function(linea){

    $scope.totalTotal += linea.total;
    $scope.bloquearGuardar = false;
  });


}
//-------------------------------------------------------------
$scope.closeDialog = function(){
  $mdDialog.hide();
}


//-----------------------------------------------------------------
$scope.guardarLineas = function(){
 console.log($scope.lineas);
 var data = {};
 data.lineas = $scope.lineas;
 data.patente = $scope.patente;
 srvRestApi.guardarLineasItems(data);
 $scope.closeDialog();
}



});
