var app = angular.module('taquillApp' );

app.controller('ctrImprimir', function($scope, $timeout,$window,$state, localStorageService, srvRestApi, $filter,facGlobal){
  var declaracion = localStorageService.get('lsDeclaracion');
console.log(declaracion);

$scope.mostrarPeriodo = false;
$scope.mostrarPeriodo2 = false;
$scope.esCatastro = false;
$scope.esCatastroManual = false;
$scope.esItem = false;
$scope.items = localStorageService.get('lsItems');
console.log("items");
console.log($scope.items);
$scope.patente = declaracion.patente_[0];
$scope.pago = declaracion.pago_[0];
$scope.detalle = declaracion.detalle_;
  $scope.created_at = $scope.detalle['created_at'];
$scope.recibo = declaracion.recibo_;
$scope.persona = declaracion.persona_[0];
$scope.lineas = declaracion.patente_[0].lineas;
$scope.usuario = declaracion.usuario_
$scope.total_total = $scope.pago.monto_total;
console.log($scope.total_total);
$scope.multa = 0;
$scope.descuento = 0;
$scope.interes = 0;
$scope.vigente = 0;
$scope.vigentePublicidad = 0;
$scope.vigenteLicores = 0;
$scope.moroso = 0;
$scope.tbase = 0;
$scope.subTotal = 0;
$scope.totalTotal = 0;
$scope.totalCatastro = 0;
$scope.totalCatastroManual = 0;
$scope.totalReciboManual = 0;
$scope.tasas = localStorageService.get('lsTasasAdministrativas');
$scope.ingtasas = localStorageService.get('lsTasasIngieneria');
$scope.pubtasas = localStorageService.get('lsTasasPublicidad');
$scope.lictasas = localStorageService.get('lsTasasLicor');

//---------------
$scope.getNombreTasa = function (linea){
  console.log('----------------------------------------Y');
  console.log(linea);

  console.log('----------------------------------------R');
  var tasaLista = [];

  if(linea.tipo == 'Tasa')
     tasaLista = $scope.tasas;
  if(linea.tipo == 'Ingieneria')
     tasaLista = $scope.ingtasas;
  if(linea.tipo == 'Publicidad')
        tasaLista = $scope.pubtasas;
  if(linea.tipo == 'Licores')
         tasaLista = $scope.pubtasas;

console.log('++++++++++++++++++++++++++++++++++');
  var tasa = tasaLista.filter(function(tasa){
console.log(tasa.id + '==' + linea.tasa_id);
    if(tasa.id == linea.tasa_id)
    return tasa;
  });
console.log('++++++++++++++++++++++++++++++++++');
  return tasa[0].nombre;
}

//--------------------------
$scope.getNombreItem = function (linea){
console.log("linea");
console.log(linea);
 var item = $scope.items.filter(function(item){

    if(item.id == linea.item_id)
    return item;
  });

  return item[0].concepto;
}


$scope.lineas.forEach(function(linea){
     $scope.multa = $scope.multa + linea['multa']*1;
     $scope.interes = $scope.interes + linea['interes']*1;
     $scope.descuento = $scope.descuento + linea['descuento']*1;
     $scope.subTotal = ($scope.subTotal*1) + (linea['subTotal']*1);
     $scope.totalTotal = (($scope.totalTotal*1) + (linea['total']*1));
     $scope.tbase = ($scope.tbase*1 + linea['base']*1);
    if((linea.tipo == 'Catastro'))
      $scope.totalCatastro = ($scope.totalCatastro*1) + (linea['total']*1);
      if((linea.tipo == 'CatastroManual'))
        $scope.totalCatastroManual = ($scope.totalCatastroManual*1) + (linea['total']*1);
        if((linea.tipo == 'ReciboManual'))
          $scope.totalReciboManual = ($scope.totalReciboManual*1) + (linea['total']*1);

     if((linea['tipo'] == 'Declaracion')&&(linea['interes']=='0')&&(linea['multa']=='0'))
         $scope.vigente = $scope.vigente*1 + linea['subTotal']*1;
     if((linea['tipo'] == 'Declaracion')&&((linea['interes']!='0')||(linea['multa']!='0')))
             $scope.moroso = $scope.moroso*1 + linea['subTotal']*1;
    if(linea['tipo'] == 'Declaracion'){
      $scope.desde = linea['imes'] +'-'+linea['iano'];
      $scope.hasta = linea['hmes'] +'-'+linea['hano'];
      $scope.mostrarPeriodo = true;

    }
    if((linea['tipo'] == 'Catastro')){

      if(!$scope.esCatastro )
        $scope.desde = linea['imes'] +'-'+linea['iano'];
      $scope.hasta = linea['hmes'] +'-'+linea['hano'];
      $scope.esCatastro = true;
      $scope.mostrarPeriodo = true;
    }

    if((linea['tipo'] == 'Item')){

      i
      $scope.hasta = linea['hmes'] +'-'+linea['hano'];
      $scope.esItem = true;
      $scope.mostrarPeriodo = false;
    }

    if((linea.tipo == 'CatastroManual')){

      if(!$scope.esCatastroManual )
        $scope.desde = linea['imes'] +'-'+linea['iano'];
      $scope.hasta = linea['hmes'] +'-'+linea['hano'];
      $scope.esCatastroManual = true;
      $scope.concepto = linea['concepto'];
      $scope.mostrarPeriodo = true;
    }

    if((linea.tipo == 'ReciboManual')){

      if(!$scope.esReciboManual )
        $scope.desde = linea['imes'] +'-'+linea['iano'];
      $scope.hasta = linea['hmes'] +'-'+linea['hano'];
      $scope.esReciboManual = true;
      $scope.concepto = linea['concepto'];
      $scope.mostrarPeriodo = true;
    }

    if(linea['tipo'] == 'Publicidad'){
      $scope.mostrarPeriodo = true;
      $scope.vigentePublicidad = $scope.vigentePublicidad*1 + linea['subTotal']*1;
      if(!$scope.esCatastro )
        $scope.desde = linea['imes'] +'-'+linea['iano'];
      $scope.hasta = linea['hmes'] +'-'+linea['hano'];

      $scope.mostrarPeriodo2 = true;
    }

    if(linea['tipo'] == 'Licores'){
      $scope.mostrarPeriodo = true;
      $scope.vigenteLicores = $scope.vigenteLicores*1 + linea['subTotal']*1;
      if(!$scope.esCatastro )
        $scope.desde = linea['imes'] +'-'+linea['iano'];
      $scope.hasta = linea['hmes'] +'-'+linea['hano'];

      $scope.esLicor = true;
    }
});





//  $scope.ramo = ($scope.patente.tipo == 'comercio')?'Actividades Economicas':'Propiedad inmobiliaria';

   $scope.printIt = function(){
     html2canvas(document.getElementById('printArea'), {
         onrendered: function (canvas) {
             var data = canvas.toDataURL();
             var docDefinition = {

                 content: [{
                     image: data,
                     width: 500,
                     height:400,

                 }]
             };
             pdfMake.createPdf(docDefinition).open("test.pdf");
         }
     });

   };

 $scope.printIt2 = function(){
   var table = document.getElementById('printArea').innerHTML;
     var myWindow = $window.open('', '', 'width=800, height=600');
     myWindow.document.write(table);
     myWindow.print();

     $state.go('declaracion');

 };


});
