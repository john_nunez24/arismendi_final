var app = angular.module('taquillApp');

app.controller('ctrConfiguracion', function(facGlobal,$mdDialog,$rootScope,$scope,$filter,$mdToast,$location, $state, localStorageService, srvRestApi){
$scope.user = $rootScope.rsUser;
$scope.concepto = {};
$scope.depActual = {};
$scope.uniActual = {};
$scope.departametoUsuarios = [];
//$scope.unidad = {};

//$scope.defaulPassword = '$2y$10$vlWYo/PdghoVfWkcuJDCUeXW1rB5cid9MJBrgNUD5LJ.3qk7UYp6a';
if($rootScope.departamentoActual == undefined)
    $rootScope.departamentoActual = {};
 else
    $scope.depActual = $rootScope.departamentoActual;
 if($rootScope.unidadActual == undefined)
        $rootScope.unidadActual = {};
   else
        $scope.uniActual = $rootScope.unidadActual;



//get departamentos
srvRestApi.getDepartamentos()
       .then(function(res){
         $scope.departamentos = res.data;
         $scope.updateItemsByDepartamento(res.data[0].id);
         $scope.concepto.departamento = res.data[0];
         $scope.getUsuario(res.data[0].id);
       },function(err){

       });
//get unidad de cobro
srvRestApi.getUnidadCobro()
       .then(function(res){
         $scope.unidadCobro = res.data;
         console.log($scope.unidadCobro);
         $scope.updateUnidadValor(res.data[0].id);
         $scope.concepto.unidad = res.data[0];

       },function(err){

       });
      //$scope.unidadCobro = $scope.updateUnidadCobro();

//---tipo usuarios------
$scope.nivelUsuarios = [
                {value:'super', name:'Super Usuario'},
                {value:'cliente', name:'Cliente'},
                {value:'consulta', name:'Operador Tipo Consulta'},
                {value:'gestion', name:'Operador de Actividades Economicas'},
                {value:'catastro', name:'Operador Tipo Catastro'},
                {value:'taquilla', name:'Operador Tipo Taquilla'},
                {value:'simple', name:'Operador Tipo Taquilla'},
                {value:'reporte', name:'Operador de Reportes'},
                {value:'ingieneria', name:'Operador para Ingieneria'},
                {value:'camara', name:'Operador para Camara'},
                {value:'sindicatura', name:'Operador para Sindicatura'},
                {value:'dcatastro', name:'Operador Director de Catastro'},
                {value:'auxcatastro', name:'Operador Auxiliar de Catastro'},
                {value:'configuracion', name:'Operador Configuracion del Sistema'},
                {value:'dgestion', name:'Operador Director de Actividades Economicas'}

];
$scope.tiposUsuarios= [
                {value:'cliente', name:'Cliente'},
                {value:'sistema', name:'Usuario del Sistema'}
              ];
//---------------john nuñez----------------------------------------------------------
$scope.getUsuario= function(departamento_id){
    srvRestApi.usuarios()
            .then(function(res){
              $scope.usuarios = res.data;

              console.log('paso1');
              console.log($scope.usuarios);
              $scope.usarioUdateByDepartamento(departamento_id);
            },function(err){
              //$scope.usuarios = res.data;
              console.log('paso2');
            });
};
//--------------------
$scope.usarioUdateByDepartamento = function(id){
    $scope.departametoUsuarios = [];

    $scope.usuarios.forEach(function(usuario){

          if (usuario.perfil.departamento_id == id)
            $scope.departametoUsuarios.push( usuario);
    });
    console.log($scope.departametoUsuarios);


}
//--------------------------john nuñez ----------------------------------------
$scope.respuesta = function(user,checkPassword) {
    if (checkPassword == true){

        user.verificacion = true;
        $scope.passReset = '123';
        alert('La contraseña provicional es: 123');

    } else {
      {
        user.verificacion = false;
      }
    }
      $mdDialog.hide(user);
    };
//-------------------------john nuñez-------------------------------------------
$scope.modificarUsuario = function(ev , user){
  $rootScope.departamentoActual = $scope.concepto.departamento;

    $rootScope.rsUser = user;
    console.log($rootScope.rsUser);
    $mdDialog.show({

      templateUrl: 'temp/configuracion/modificarUsuario.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      bindToController:true,

      controller:'ctrConfiguracion'

      //fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
    }).then(function(respuesta){
      console.log('prueba***********//******** esto es OK' );
      console.log(respuesta);
      var data = {};

      data.usuario = respuesta;
      srvRestApi.modificarUsuario(data)
                .then(function(response){
                    alert('se actualizaron los datos')
                },function(response){

                });

    //  $scope.updateItemsByDepartamento($scope.concepto.departamento.id);
  },function(){
  console.log('mal');
    });


}
//-----------------------john nuñez --------------------------------------------
$scope.eliminarUsuario = function(ev, user){
  var data = {};

  console.log(user);
  var confirm = $mdDialog.prompt()
     .title('Confirma Eliminar al Usuario')
     .textContent('Ingrese un Comentario.')
     .placeholder('Comentario')
     .ariaLabel('')
     .initialValue('')
     .targetEvent(ev)
     .required(true)
     .ok('Okay!')
     .cancel('No eliminar');

   $mdDialog.show(confirm).then(function(result) {
     data.comentario= result;
     console.log(user);
     data.usuarioEliminar = user;
     console.log(data);
     srvRestApi.eliminarUsuario(data)
               .then(function(response){
                   alert('se actualizaron los datos')
               },function(response){

               });
   }, function() {
     $scope.status = 'You didn\'t name your dog.';
   });
}
//-----------------------john nuñez---------------------------------------------
$scope.crearNuevoUsuario = function(user){
  console.log(user);
  var data = {};
      data.usernew = user;
      data.departamento = $scope.concepto.departamento;
      //console.log($scope.concepto.departamento);
      srvRestApi.crearNewUser(data)
                .then(function(resp){

                },function(err){

                });

}
//------------------------john nunez-------------------------------------------

$scope.updateUnidadValor = function(id){

  srvRestApi.getUnidadValor(id)
     .then(function(resp){
       $scope.unidades = resp.data;
       console.log($scope.unidades);
     },function(err){

     });

};
//--------------------
$scope.updateItemsByDepartamento = function(id){

  srvRestApi.getItem(id)
     .then(function(resp){
       $scope.items = resp.data;
       console.log($scope.items);
       },function(err){

      });

};
//--------------------
$scope.updateUnidadCobro = function(id){

  srvRestApi.getUnidad(id)
     .then(function(resp){
       $scope.unidad = resp.data;

       },function(err){

      });

};

//---------------------
$scope.cambiarUnidadCobro = function(uni){
  $scope.updateUnidadCobro(uni.id);
  $scope.concepto.unidad = uni;
  console.log($scope.concepto.unidad);
  $scope.updateUnidadValor($scope.concepto.unidad.id);
}


//---------------------
$scope.cambiarDepartamento = function(dep){
  $scope.updateItemsByDepartamento(dep.id);
  //$scope.getUsuario(dep.id);
  $scope.concepto.departamento  = dep;
}
//---------------------
$scope.cambiarDepartamento2 = function(dep){
  //$scope.updateItemsByDepartamento(dep.id);
  $scope.getUsuario(dep.id);
  $scope.concepto.departamento  = dep;
}

//------------------------
$scope.guardarConcepto = function(){
  var data = {};
  data.concepto = $scope.concepto;
  console.log($scope.concepto);
  data.concepto.departamento = $scope.depActual;
  srvRestApi.guardarItem(data)
     .then(function(res){
       $mdDialog.hide();
     },function(err){
       console.log('error');
     });
}
//----------------------------------john nuñez----------------------------------
$scope.crearUsuario = function(ev){
$rootScope.departamentoActual = $scope.concepto.departamento;
  $mdDialog.show({

    templateUrl: 'temp/configuracion/' + dialogo + '.html',
    parent: angular.element(document.body),
    targetEvent: ev,
    clickOutsideToClose:true,
    bindToController:true,

    controller:'ctrConfiguracion'

    //fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
  }).then(function(){
    $scope.updateItemsByDepartamento($scope.concepto.departamento.id);
  },function(){
console.log('mal');
  });
}
//----------------------------------john nuñez----------------------------------


$scope.abrirDialogo = function(ev , dialogo){
$rootScope.departamentoActual = $scope.concepto.departamento;
$rootScope.unidadActual = $scope.concepto.unidad;
  $mdDialog.show({

    templateUrl: 'temp/configuracion/' + dialogo + '.html',
    parent: angular.element(document.body),
    targetEvent: ev,
    clickOutsideToClose:true,
    bindToController:true,

    controller:'ctrConfiguracion'

    //fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
  }).then(function(){
    $scope.updateItemsByDepartamento($scope.concepto.departamento.id);

  },function(){
console.log('mal');
  });
}
//----------------john nuñez 13-02-2020-----------------------------------------
$scope.guardarUindad = function(){
  console.log($scope.monto);
  console.log($scope.uniActual);
  var data ={};
  data.monto = $scope.monto;
  data.uniActual = $scope.uniActual;
  srvRestApi.nuevoMontoUnidad(data)
        .then(function(res){
          $scope.updateUnidadValor($scope.concepto.unidad.id);
          console.log($scope.unidades);
          $mdDialog.hide();
        },function(err){
          console.log('error');
        });

}
//----------------john nuñez 13-02-2020-----------------------------------------
//---------------

$scope.cerrarModal = function(){
  $mdDialog.hide();
}


//-------------
});
