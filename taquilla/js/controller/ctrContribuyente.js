var app = angular.module('taquillApp');

app.controller('ctrContribuyente', function(facGlobal,$scope,$rootScope,$mdDialog,$filter,$mdToast,$location, $state, localStorageService, srvUtils, srvRestApi){

$scope.patenAsociadas = [];
    if (localStorageService.get('lspatente') != null )
      $scope.patente = localStorageService.get('lspatente');

    if (localStorageService.get('lsPersona') != null)
      $scope.persona = localStorageService.get('lsPersona');

  //$rootScope.rsReciboEliminar = localStorageService.get('lsrecibo');
  $scope.concepto = {};
  $scope.swAgregarActividad = false;
  $scope.actividades = facGlobal.getActividadEconomica();
  $scope.actividadBase = facGlobal.getActividadBase();
  $scope.salarioActual = facGlobal.getSalarioActual();
  if($rootScope.departamentoActual == undefined)
      $rootScope.departamentoActual = {};
   else
      $scope.depActual = $rootScope.departamentoActual;
//**************************************************************

$scope.updateItemsByDepartamento = function(id){

  srvRestApi.getItem(id)
    .then(function(resp){
      $scope.items = resp.data;

      },function(err){

      });

};
//---------------john nuñez----------------------------------------------------------
$scope.getUsuario= function(departamento_id){
    srvRestApi.usuarios()
            .then(function(res){
              $scope.usuarios = res.data;

              console.log('paso1');
              console.log($scope.usuarios);
              $scope.usarioUdateByDepartamento(departamento_id);
            },function(err){
              $scope.usuarios = res.data;
              console.log('paso2');
            });
};
//---------------------
$scope.cambiarDepartamento = function(dep){
  $scope.updateItemsByDepartamento(dep.id);
  $scope.getUsuario(dep.id);
  $scope.concepto.departamento  = dep;
}

//------------------------
$scope.guardarConcepto = function(){
  var data = {};
  data.concepto = $scope.concepto;
  data.concepto.departamento = $scope.depActual;
  srvRestApi.guardarItem(data)
     .then(function(res){
       $mdDialog.hide();
     },function(err){
       console.log('error');
     });
}
//----------------------------------john nuñez----------------------------------
//-------------------------------------------------------------------
  var data = {};
  srvRestApi.findDepartamento(data)
              .then(
                      function(response){

                      },function(response){


                      });
                      //get departamentos
  srvRestApi.getDepartamentos()
          .then(function(res){
           $scope.departamentos = res.data;
            $scope.updateItemsByDepartamento(res.data[0].id);
            $scope.concepto.departamento = res.data[0];
          $scope.getUsuario(res.data[0].id);
         },function(err){

          });
 //--------------------
 //---------------john nuñez----------------------------------------------------------
 $scope.getUsuario= function(departamento_id){
     srvRestApi.usuarios()
             .then(function(res){
               $scope.usuarios = res.data;

               console.log('paso1');
               console.log($scope.usuarios);
               $scope.usarioUdateByDepartamento(departamento_id);
             },function(err){
               //$scope.usuarios = res.data;
               console.log('paso2');
             });
 };
  //---------------------john nuñez -----------------------------------------

  $scope.usarioUdateByDepartamento = function(id){
      $scope.departametoUsuarios = [];

      $scope.usuarios.forEach(function(usuario){

            if (usuario.perfil.departamento_id == id)
              $scope.departametoUsuarios.push( usuario);
      });
      console.log($scope.departametoUsuarios);


  }
  //--------------------
  $scope.econtrarRecibos = function(){
  try{
    $scope.visualizarVinculacion = false;
    $scope.mostrarVinculacion = false;

    if ($scope.patente!=null){
      var data = {};
      data.id = $scope.patente.persona.id;
      data.paginate = "200";
      data.status = $scope.selStatus;
      data.fecha = $scope.fecha;
      //data.tipo = "Declaracion";
      $scope.checkIcono = '../images/add.svg';
      $scope.color = 'Grey';

      srvRestApi.findRecibos(data)
      .then(function(response){
        $scope.swTieneRecibos = true;
        $scope.recibos = response.data.data;
        localStorageService.set('lsRecibos',$scope.recibos);

        if($scope.recibos.length == 0){
          $scope.swTieneRecibos = false;
        }
      });
      console.log($scope.patente);
      var data1 = {};
      data1.id = $scope.patente.id;
      srvRestApi.patentesAsociadas(data1)
                  .then(
                        function(response){
                          console.log('*----------*');
                          $scope.patenAsociadas = response.data;
                          console.log($scope.patenAsociadas);
                        },
                          function(response){
                            console.log('no hay');
                          });

      }

      else {
      if ($scope.patente == null && $scope.persona != null){
        var data = {};
        data.id = $scope.persona.id;
        data.paginate = "200";
        data.status = $scope.selStatus;
        data.fecha = $scope.fecha;
      //  data.tipo = "Declaracion";
        $scope.checkIcono = '../images/add.svg';
        $scope.color = 'Grey';

        srvRestApi.findRecibos(data)
        .then(function(response){
          $scope.swTieneRecibos = true;
          $scope.recibos = response.data.data;
          if($scope.recibos.length == 0){
            $scope.swTieneRecibos = false;
          }
        });
      }
    }
  }catch(error){
    console.error(error);
  }
  }
//-------------------------------------john nuñez------------------------------------
$scope.verificarData = function(fecha){

    if (fecha == undefined){

        fecha = 'No tiene Pago';
    }
    return fecha;
}

  //--------------------------------------------------------------------
  $scope.actividadById = function(id){
    var act = $scope.actividadBase.filter(function(actividad){
      if (actividad.actividad_id == id)
      return actividad;
    });
    return act[0];
  }
  //---------------------------------------------------------------------

  $scope.getMinimoTributario = function(_actividad){
    var aBase = $scope.actividadById(_actividad.id);
    return aBase.minimo;
  }

  //---------------------------------------------------------------------

  $scope.getAlicuota = function(_actividad){
    var aBase = $scope.actividadById(_actividad.id);
    return aBase.porcentaje;
  }

  $scope.econtrarRecibos();

  $scope.getActividadesEconomicas = function(){
    var data = {};
    console.log($scope.patente['id']);
    data.id = $scope.patente['id'];
    srvRestApi.findActividadPatente(data)
    .then(function(response){
      $scope.swTieneActividad = true;
      $scope.actividadPatentes = response.data;

      if($scope.actividadPatentes.length == 0){
        $scope.swTieneActividad = false;
      }
    });
  }







  //----------------------------------------------------------------------------

  $scope.mostrarAgregarActividad = function(){
    $scope.swAgregarActividad = true;
  }


  //----------------------------------------------------------------------------

  $scope.editarContribuyente = function(ev){

    $mdDialog.show({

      templateUrl: 'temp/declarar/editarContribuyente.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
    }).then(function(){
      $scope.patente = localStorageService.get('lspatente');

    },function(){
      $scope.patente = localStorageService.get('lspatente');
    });

  }
  //---------------------codigo john nuñez.............
  $scope.vermensage = function(a,ev){
    //localStorageService.set('lsRecibo',recibo);
    $rootScope.rsEliminarActividad = a;
    //console.log('aqui ver');
    //  console.log($rootScope.rsEliminarActividad);

    $mdDialog.show({
      templateUrl: 'temp/declarar/eliActividad.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
    }).then(
      function(){
        ////////
      },
      function(){
        srvUtils.getActividadesEconomicas($rootScope.rsEliminarActividad.actividadPatente.patente_id)
        .then(
          function(res){
            $scope.actividadPatentes = res.data;

          },function(err){
          }
        );

        console.log($scope.actividadPatentes);
      });
    };


    //---------------------codigo john nuñez.............
    $scope.anulado = function (ev,recibo){
      // dialogo de anular recibo ----john nuñez------
      $rootScope.rsReciboEliminar = recibo;
      $rootScope.prueba = 'esto es una prueba mas'
      console.log('********************la vida***********');
      console.log($rootScope.rsReciboEliminar);
      console.log('estoy aqui ');
      $mdDialog.show({
        templateUrl: 'temp/declarar/eliRecibo.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: false // Only for -xs, -sm breakpoints.
      }).then(
                function(response){
                    console.log('sera el 1');
                },function(response){
                  console.log('sera el 2');
                  $scope.econtrarRecibos();
                });

                  //$scope.cancel();
                  console.log('pase por aqui');
 };
//------------John nuñez------------------------------------------------------------------
$scope.eliminarRecibo = function (){
  var data = {};
  //console.log($rootScope.prueba);
  //console.log('*************eliminar**********por aqui estoy');
  //console.log($rootScope.rsReciboEliminar.id);
  //console.log($scope.comentario);
  if(($scope.comentario == null)||($scope.comentario == undefined)){
    alert('Debe ingresar un comentario justificando el cambio.')
  }else{
     data.recibo = $rootScope.rsReciboEliminar;
     data.comentario = $scope.comentario;
     data.usuario = localStorageService.get('lsUsuario');
     console.log(data);
     srvRestApi.eliminarRecibo(data)
     .then(
       function(response){
          alert('Los cambios fueron realizados correctamente');
         console.log('paso 1');

       },
       function(response){
         console.log('paso 2');
         $scope.cancel();
         //alert('Al parecer no se pudo guardar los datos, debido a que existe una declaracion para la actividad economica en el periodo seleccionado');


       });


  };
};
//---------------------------------john nunez ---------------------------------------
$scope.buscarContribuyente = function(){
  console.log($scope.numero);
  var data = {};
  data.rif = $scope.numero;
  data.nombre = $scope.nombre;
  srvRestApi.buscarContribuyente(data)
                .then(
                      function(response){
                        console.log('**1**');
                          $scope.personas = response.data;
                      },
                        function(response){
                            console.log('**2**');
                        });
}
//-------------- john nuñez ----------------------------------------------------------
$scope.buscarPatente1 = function() {
var data = {};
   data.patente_id = $scope.numeroPatente;
   console.log(data.patente_id);
   srvRestApi.buscarPatenteVincular(data)
            .then(function(response){
               $scope.patentesBuscadas = response.data;
               console.log($scope.patentesBuscadas);
               $scope.mostrarVinculacion = true;
            },
              function(response){
                alert('No se encontro resultado con ese Numero Ingresado')
              });
}
//----------------john nuñez ------------------------------------------------------
$scope.patentesContribuyente = function(persona){
  console.log(persona);
  var data = {};
  data.personas = persona.id;
       srvRestApi.patentesPersona(data)
                    .then(function(response){
                            console.log(response);
                            $scope.patenteContribuyente = response.data;
                            },
                              function(response){


                              });

}
//----------------------john nunez ---------------------------------------------------
$scope.eliminarVinculo = function(patHija,ev){
  console.log(patHija);
  console.log($scope.patente);
  var data = {};
  var data1 = {};
  data1.id = $scope.patente.id;
  data.padreId = $scope.patente.id;
  data.hijaId = patHija.id;
  var confirm = $mdDialog.prompt()
      .title('Ingrese el Comentario')
      .textContent('Motivo por el cual elimina la Vinculacion  de las Patentes.')
      .placeholder('Comentario')
      .ariaLabel('Comentario')
      .initialValue('')
      .targetEvent(ev)
      .required(true)
      .ok('Eliminar')
      .cancel('Cancelar');

    $mdDialog.show(confirm).then(function(result) {
      //$scope.status = 'You decided to name your dog ' + result + '.';
              console.log(result);
              data.comentario = result;
                srvRestApi.eliminarVinculacion(data)
                          .then(
                                function(response){

                                  srvRestApi.patentesAsociadas(data1)
                                              .then(
                                                    function(response){
                                                      $scope.patenAsociadas = response.data;
                                                      console.log($scope.patenAsociadas);
                                                    },
                                                      function(response){
                                                        console.log('no hay');
                                                      });
                                  },
                                    function(response){

                                     });
      }, function() {
      //$scope.status = 'You didn\'t name your dog.';
      });
}
//------------John nuñez------------------------------------------------------------------
$scope.vinculacion = function(ev) {
  var data1 = {};
  data1.id = $scope.patente.id;
    $mdDialog.show({

      templateUrl: 'temp/declarar/dialogoVincular.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
    })
    .then(function(answer) {
        var data1 = {};
        data1.id = $scope.patente.id;
        srvRestApi.patentesAsociadas(data1)
                  .then(
                      function(response){
                        console.log("exito debe mostar");
                        $scope.patenAsociadas = response.data;
                        console.log($scope.patenAsociadas);
                       },
                        function(response){
                        console.log('no hay');
                      });
     }, function() {




    });
};
//---------------------john nuñez ---------------------------------------------------
$scope.verVinculacion = function () {
  $scope.visualizarVinculacion = false;
  $scope.mostrarVinculacion = true;

}
//------------------------ john nuñez ------------------------------------------
$scope.addVinculacion = function (obj, ev){
var data = {};
  console.log(obj);
  console.log($scope.patente['id']);
var data1 = {};
  data1.id = $scope.patente.id;
  data.idPadre = $scope.patente['id'];
  data.idhija = obj.id;
  /*var confirm = $mdDialog.prompt()
      .title('Ingrese el Comentario')
      .textContent('Motivo por el cual se hace la Vinculacion  de las Patentes.')
      .placeholder('Comentario')
      .ariaLabel('Comentario')
      .initialValue('')
      .targetEvent(ev)
      .required(true)
      .ok('Vincular')
      .cancel('Cancelar');

    $mdDialog.show(confirm).then(function(result) {
      //$scope.status = 'You decided to name your dog ' + result + '.';
              console.log(result);*/
              data.comentario = "Agregado";
                srvRestApi.vinculacionPatentes(data)
                      .then(function(response){
                               $mdDialog.hide();
                             },function(response){

                             });
      /*}, function() {
        //$scope.status = 'You didn\'t name your dog.';
      });*/
}

//-----------------------john nuñez-----------------------------------------------
 $scope.cancel = function() {
   $mdDialog.cancel();
 };

    //----------------------------------------------------------------------------

    $scope.editarDeclaracion = function(ev,recibo){
      localStorageService.set('lsRecibo',recibo);
      $mdDialog.show({

        templateUrl: 'temp/declarar/editDeclaracion.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
      }).then(function(){
        $scope.econtrarRecibos();
        localStorageService.set('lsRecibo',undefined);
      },function(){
        $scope.econtrarRecibos();
        localStorageService.set('lsRecibo',undefined);
      });

    }
    //----------------------------------------------------------------------------

    $scope.verDetalle = function(ev,recibo){
      localStorageService.set('lsRecibo',recibo);
      $mdDialog.show({

        templateUrl: 'temp/declarar/verDeclaracion.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
      });

    }
    //----------------------------------------------------------------------------

    $scope.agregarDeclaracion = function(ev){
    if ($scope.patente!=null){

      if(($scope.patente.persona == null)
      ||($scope.patente.persona.rif==null)
      ||($scope.patente.persona.rif==undefined)
      ||($scope.patente.persona.rif=="")){
        alert('Atencion, Ingrese un numero de RIF valido antes de agregar una de declaración')
      }else {
        localStorageService.set('lsRecibo',undefined);
        $mdDialog.show({

          templateUrl: 'temp/declarar/nuevaDeclaracion.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true,
          fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        }).then(function(){
          $scope.econtrarRecibos();
        },function(){
          $scope.econtrarRecibos();
        });
      }

    }
      else {
        localStorageService.set('lsRecibo',undefined);
        $mdDialog.show({

          templateUrl: 'temp/declarar/nuevaDeclaracion.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true,
          fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        }).then(function(){
          $scope.econtrarRecibos();
        },function(){
          $scope.econtrarRecibos();
        });
      }
    }
  //--------------------------john nuñez---------------------------------------


    //----------------------------------------------------------------------------



        $scope.agregarDeclaracionItems = function(ev){
          if(($scope.patente.persona == null)
          ||($scope.patente.persona.rif==null)
          ||($scope.patente.persona.rif==undefined)
          ||($scope.patente.persona.rif=="")){
            alert('Atencion, Ingrese un numero de RIF valido antes de agregar una de declaración')
          }else {

            $scope.patente.patenteAsociadas = $scope.patenAsociadas;
            $rootScope.patenteActual = $scope.patente;
            console.log($scope.patente);
            localStorageService.set('lsRecibo',undefined);
            $mdDialog.show({

              templateUrl: 'temp/declarar/nuevaDeclaracionItems.html',
              parent: angular.element(document.body),
              targetEvent: ev,
              clickOutsideToClose:true,
              fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            }).then(function(){
              $scope.econtrarRecibos();
            },function(){
              $scope.econtrarRecibos();
            });
          }
        }


    //------------------------------------------------------------------------------
    $scope.getNombreActividad = function(acti_id){

      /*  var actividad = $scope.actividades
      .filter(function(acti){
      if(acti.id == acti_id){
      return acti;
    }
  });
  return actividad.nombre;*/
}



$scope.agregarActividad = function(){
  var data = {};
  data.patente_id = $scope.patente['id'];
  data.actividad_id = $scope.selActividad;
  srvRestApi.asociarActividad(data)
  .then(function(response){
    $scope.swAgregarActividad = false;

    $scope.getActividadesEconomicas();

  },function(response){

  });
}

//codigo modificado por john------------
$scope.eliminarActividad = function(actividad){
  var data = {};
  console.log('estoy aqui');
  data.actividad = actividad;

  console.log('ya pase');
  srvRestApi.eliminarActividad(data)
  .then(function(response){
    $scope.getActividadesEconomicas();
  },function(response){

  });
}
//codigo modificado por john------------

if($scope.patente != null)
{
  $scope.getActividadesEconomicas();
}
///////////////////john nuñez////////////////////////
//---------------------------nuevo 10/09/2020----------------------------------
$scope.calculoEstadoCuenta= function(ev){
  $mdDialog.show({

    templateUrl: 'temp/declarar/emitirEstadoCuenta.html',
    parent: angular.element(document.body),
    targetEvent: ev,
    clickOutsideToClose:true,
    fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
  }).then(function(){
    //$scope.econtrarRecibos();
  },function(){
    //$scope.econtrarRecibos();
  });

}
/////////////////john nuñez/////////////////////////////
});
