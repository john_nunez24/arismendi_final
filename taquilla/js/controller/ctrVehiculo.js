var app = angular.module('taquillApp');
app.config(function($mdIconProvider) {
  $mdIconProvider
    .defaultIconSet('../js/component/material-icon-extra/mdi.svg');
});

app.controller('ctrVehiculo', function($scope,$rootScope, $mdDialog, $filter,$mdToast,$location, $state, localStorageService, srvRestApi){
$scope.verPersona = false;
$scope.vehiculo = {};
$scope.patente = localStorageService.get('lspatente');
$scope.usuario = localStorageService.get('lsUsuario');
$scope.recibos = localStorageService.get('lsRecibo');

//----------------------------john nuñez-----------------------------
//--------------------
$scope.econtrarRecibos = function(){
  $scope.visualizarVinculacion = false;
  $scope.mostrarVinculacion = false;
  $scope.patente = localStorageService.get('lspatente');
  if ($scope.patente!=null){
    var data = {};
    data.id = $scope.patente.persona.id;
    data.paginate = "200";
    data.status = $scope.selStatus;
    data.fecha = $scope.fecha;
    data.tipo = "Item";
    $scope.checkIcono = '../images/add.svg';
    $scope.color = 'Grey';

    srvRestApi.findRecibos(data)
    .then(function(response){
      $scope.swTieneRecibos = true;
      $scope.recibos = response.data.data;
      localStorageService.set('lsRecibo',$scope.recibos);
      if($scope.recibos.length == 0){
        $scope.swTieneRecibos = false;
      }
    });
    console.log($scope.patente);
    var data1 = {};
    data1.id = $scope.patente.id;
    srvRestApi.patentesAsociadas(data1)
                .then(
                      function(response){
                        $scope.patenAsociadas = response.data;
                        console.log($scope.patenAsociadas);
                      },
                        function(response){
                          console.log('no hay');
                        });

    }

    else {
    if ($scope.patente == null && $scope.persona != null){
      var data = {};
      data.id = $scope.persona.id;
      data.paginate = "200";
      data.status = $scope.selStatus;
      data.fecha = $scope.fecha;
      data.tipo = "Item";
      $scope.checkIcono = '../images/add.svg';
      $scope.color = 'Grey';

      srvRestApi.findRecibos(data)
      .then(function(response){
        $scope.swTieneRecibos = true;
        $scope.recibos = response.data.data;
        localStorageService.set('lsRecibo',$scope.recibos);
        if($scope.recibos.length == 0){
          $scope.swTieneRecibos = false;
        }
      });
    }
  }

}
//-----------------------------john nuñez-------------------------------------
$scope.econtrarRecibos();
$scope.recibos = localStorageService.get('lsRecibo');
//------------------------john nuñez ------------------------------------------
$scope.fechaPago = function(recibo){
  console.log(recibo.lineas[0].status);
  if (recibo.lineas[0].status == 'Pagada')
        return recibo.lineas[0].updated_at;
      else
          return 'sin Pago';

}
//-------------------------------------john nuñez------------------------------------
if ($scope.patente != null){
  var data1 = {};
  data1.id = $scope.patente.id;
  srvRestApi.patentesAsociadas(data1)
              .then(
                    function(response){
                      $scope.patenAsociadas = response.data;
                      console.log($scope.patenAsociadas);
                    },
                      function(response){
                        console.log('no hay');
                      });
}

//------------------------john nuñez-----------------------------------------
$scope.busqueda= function(){
    $scope.verPersona= true;
}
//------------------------john nuñez------------------------------------------
$scope.buscarPersonas=function(){
  console.log($scope.personaNombre);
  var data = {};
  data.rif = '';
  data.nombre = $scope.personaNombre;
  srvRestApi.buscarContribuyente(data)
                .then(
                      function(response){
                        console.log('**1**');
                          $scope.personas = response.data;
                          console.log($scope.personas);
                      },
                        function(response){
                            console.log('**2**');
                        });

}
//--------------------------john nuñez--------------------------------------
$scope.selObjPer = function(persona){
  $scope.verPersona=false;
  $scope.persona=persona;
}
//----------------------------john nuñez --------------------------------
$scope.procesarGuardado= function(vehiculo, persona){
  var data = {};
   if (persona.id == undefined){
      persona.id = null;
   }
  data.vehiculo= vehiculo;
  data.persona = persona;

      srvRestApi.agregarVehiculo(data)
                  .then(function(response){
                      alert('Los Datos se Guardaron con Exito.');
                      //console.log('Los Datos se Guardaron con Exito.');
                  },function(response){
                    console.log(response.data);
                    alert(response.data.error);
                  });
}
//---------------------codigo john nuñez.............


$scope.buscarPatente = function(){
  console.log($scope.numero);
  if(($scope.numero != undefined)&&($scope.numero != ""))
  {

    $scope.patente = {};
    $scope.patente.tipo = 'todo';
    $scope.patente.patente = $scope.numero;
    $scope.patente.direccion = '';
    $scope.patente.nombre = '';
    //-----------------------------------
    srvRestApi.findPatente($scope.patente)
    .then(function(response){
      $scope.patentes = response.data.data;
    },function(response){

    });

  }
}
//------------------------------------------john ------------------------------
$scope.selObj = function(obj){
  console.log(obj);
  localStorageService.set('lspatente',null);
  localStorageService.set('lspatente',obj);

console.log(obj);
  $scope.usuario = localStorageService.get('lsUsuario');

  if($scope.usuario.nivel == 'gestion')
  $state.go('generarRecibo');
  if($scope.usuario.nivel == 'dgestion')
  $state.go('generarRecibo');
  $scope.econtrarRecibos();
  console.log($scope.recibos);
  localStorageService.set('lsRecibo',$scope.recibos);
}

//---------------------------john nuñez----------------------------------------
        $scope.agregarDeclaracionItems = function(ev){
          if(($scope.patente.persona == null)
          ||($scope.patente.persona.rif==null)
          ||($scope.patente.persona.rif==undefined)
          ||($scope.patente.persona.rif=="")){
            alert('Atencion, Ingrese un numero de RIF valido antes de agregar una de declaración')
          }else {

            $scope.patente.patenteAsociadas = $scope.patenAsociadas;
            $rootScope.patenteActual = $scope.patente;
            console.log($scope.patente);
            localStorageService.set('lsRecibo',undefined);
            $mdDialog.show({

              templateUrl: 'temp/vehiculo/nuevaDeclaracionItems.html',
              parent: angular.element(document.body),
              targetEvent: ev,
              clickOutsideToClose:true,
              fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            }).then(function(){
              $scope.econtrarRecibos();
            },function(){
              $scope.econtrarRecibos();
            });
          }
        }


    //------------------------------------------------------------------------------


});
