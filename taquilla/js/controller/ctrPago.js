var app = angular.module('taquillApp');

app.controller('ctrPago', function($scope, $rootScope, $mdDialog, $timeout,$window,$state, facGlobal, localStorageService, srvRestApi, $filter){
  var patente = localStorageService.get('lspatente');
  var persona = localStorageService.get('lsPersona');
  $scope.procesar = false;
  $scope.selectedLineas = [];
  if ($rootScope.selectedRecibo != undefined){
    $scope.selRecibo = $rootScope.selectedRecibo ;
    $scope.selectedLineas = $rootScope.selectedRecibo.lineas ;

    console.log('llamada en modal');
  }else {
    console.log('llamada no modal XXX');
  }
console.log('recibos Lineas------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
console.log($scope.selectedLineas);
$scope.utmariBase = facGlobal.getUmariBase();
  $scope.selected = [];

  $rootScope.recibos = {};
  $scope.totales = 0;
  $scope.tipoPagos = ['Tarjeta Debito / Credito',
                      'Transferencia','Cheque'];
  $scope.bancos = [ 'Banco Central de Venezuela',
   'Banco Industrial de Venezuela',
   'Banco de Venezuela',
   'Venezolano de Crédito',
   'Banco Mercantil',
   'Banco Provincial',
   'Bancaribe',
   'Banco Exterior',
   'Banco Occidental de Descuento',
   'Banco Caroní',
   'Banesco',
   'Banco Sofitasa',
   'Banco Plaza',
   'Banco Fondo Común',
   '100% Banco',
   'DelSur',
   'Banco del Tesoro',
   'Banco Agrícola de Venezuela',
   'Bancrecer',
   'Banco Activo',
  'Banco Internacional de Desarrollo',
  'Banplus',
  'Banco Bicentenario',
  'Banco de la Fuerza Armada Nacional Bolivariana',
  'Citibank',
  'Banco Nacional de Crédito'];
$scope.busquedaRecibo= function(){
try{
  //debe estar en el servicio
  if((persona != null)||(patente['persona_id']!= null))
  {

    if(persona != null){
      $scope.rif = persona['rif'];
      $scope.nombre = persona['nombre'];
      $scope.direccion = persona['direccion'];
    }else{

      $scope.rif = patente['rif'];
      $scope.nombre = patente['nombre'];
      $scope.direccion = patente['direccion'];


    }
    var data = {};
    data.status = 'Pendiente';
    data.id = (persona == null)?patente['persona_id']:persona['id'];
    data.paginate = 100;
    data.patente_id = (patente == null)?'0':patente['id'];

    srvRestApi.findRecibos(data)

    .then(function(response){
      console.log(response);
      $scope.totalPagar = 0;
      $scope.recibos = response.data.data;
      /*$scope.recibos = response.data.data.filter(function(recibo){
                              if((patente == null))
                                return recibo;
                              else
                              if(recibo.patente_linea.patente_id == patente['id'])
                                return recibo;
                         });*/
                         console.log($scope.recibos);
      $scope.recibos.forEach(function(recibo){
        var dataR = {};
        dataR.id = recibo.id;
        srvRestApi.findLinea(dataR).then(function(response){
          var totalLinea = 0;
          response.data.forEach(function(line){
            totalLinea = totalLinea + ((line.total)*1);
          });

          recibo.total = totalLinea;

          recibo.lineas = response.data;
        });

      });
        console.log($scope.recibos);
    });

  }
}
  catch(error){}
}
$scope.busquedaRecibo();
//------------------------john nuñez-----------------------------------------
$scope.subConceptoItem = function(linea){
  console.log(linea);
    var data = {};
    srvRestApi.buscarNumeroPatente(data)
                .then(function(response){

                  },function(response){

                  });
}
//-----------------------john nuñez ---------------------------------------------
$scope.getPatenteLinea = function(linea){
  if (linea.tipo == 'Tasa')
    return  $scope.subConceptoTasa(linea);
  if (linea.tipo == 'Item')
    return  $scope.subConceptoItem(linea);
}
//-----------------------john nuñez ---------------------------------------------
$scope.getConceptoLinea = function(linea){
      if (linea.tipo == 'Tasa')
        return  $scope.conceptoTasa(linea);
      if (linea.tipo == 'Item')
        return  $scope.conceptoItem(linea);

          console.log('repetira la llamada principal?????');
       /*else
            if (linea.tipo == 'Declaracion'){

            }else
              if (linea.tipo == 'Catastro'){

              }*/




}
//------------------------------------john nuñez----------------------------------
$scope.conceptoTasa = function(linea){
      $scope.tasas = facGlobal.getTasasAdministrativas();
      $scope.tasas.forEach(function(tasa){
          if (tasa.id == linea.tasa_id){
            return 'pago de:' + tasa.nombre +' ';
          }
        });
  }
//------------------------------------john nuñez----------------------------------
$scope.conceptoItem = function(linea){
  var items = facGlobal.getItems();
  var concepto = {};
  console.log($scope.items);
  console.log('que locura');

  items.forEach(function(item){
    console.log(item.id +'==' + linea.item_id);
    if (item.id == linea.item_id){
      console.log('estoy aqui');
      console.log(item.concepto);
      concepto = item.concepto;

    }
  });
  console.log(concepto);
  return  concepto;
}
//------------------------------------john nuñez---------------------------------------------
$scope.verLista = function(ev , recibo){
      //console.log(recibo.lineas);
      $rootScope.selectedRecibo = recibo;


        $mdDialog.show({
          templateUrl: 'temp/declarar/verlineas.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true,
          fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
          //preserveScope: true
        })
        .then(function(answer) {
          console.log('**paso1***');

          //$scope.status = 'You said the information was "' + answer + '".';
        }, function() {
          console.log('**paso2***');

        });
}
//------------------------------------------------------------------------------
$scope.cancel = function() {
  $mdDialog.cancel();
};
//------------------------------------------------------------------------------

  //---------------------------------john nuñez---------------------------------
  $scope.toggle = function (item, list) {
        $scope.procesar = true;
         var idx = list.indexOf(item);
         if (idx > -1) {

           list.splice(idx, 1);
           $scope.totales = ($scope.totales * 1) - (item.totalPagar * 1);
         }
         else {
           list.push(item);

           $scope.totales = ($scope.totales * 1) + (item.totalPagar * 1);
         }
         $scope.totales = $scope.totales.toFixed(2);

       };

       $scope.exists = function (item, list) {
         return list.indexOf(item) > -1;
       };
  $scope.isChecked = function() {
    return $scope.selected.length === $scope.recibos.length;
  };

  $scope.toggleAll = function() {
      $scope.procesar = true;
    if ($scope.selected.length === $scope.recibos.length) {

          $scope.totales = 0;
          $scope.selected = [];
    } else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
            $scope.totales = 0;
            $scope.selected = $scope.recibos.slice(0);
            for (var i = 0; i <= $scope.selected.length; i++) {
              $scope.totales = ($scope.totales * 1) + ($scope.selected[i].totalPagar * 1);
            }

    }
    $scope.totales = $scope.totales.toFixed(2);
  };
  //---------------------john nuñez-----------------------------------------------------
  $scope.imprimirGuardar = function(recibos , total){
     $scope.dataDec = {};

     $scope.procesar = false;

     $scope.dataDec.recibos = recibos;
     $scope.dataDec.pago = $scope.pago ;
     $scope.dataDec.total = total;
     $scope.dataDec.usuario = localStorageService.get('lsUsuario');


     srvRestApi.pagar($scope.dataDec)
             .then(
                function(response){


                  localStorageService.set('lsDeclaracion',response.data);

                  //$state.go('recibo');
                  $scope.reciboImpreso(response.data);
                  $scope.busquedaRecibo();
                  $scope.pago = {};
                  $scope.totales = 0;
                },
                function(response){
                  console.log('no se puedo guardar declaracion');

                }
             );
     $scope.selected = [];
  }
  //-----------------john nuñez-------------------------------------------------
  $scope.reciboImpreso = function(data) {

   window.open('temp/declarar/reciboImpreso.html#?data='+data,'_blank','toolbar=no,scrollbars=yes,resizable=yes,width=800,height=400')

  };
  //-------------------------------------------------
  $scope.getConcepto = function(lineas){
    var periodo = '';
    lineas.forEach(function(line){
      if(line.imes != null)
        periodo += line.imes + '-' +line.iano+' ';
    });

    return periodo;

  }
  //------
  $scope.getDescuentos = function(lineas){
    var total = 0;
    lineas.forEach(function(line){
      total +=  ((line.descuento)*1);
    });
    return total;
  }
  //------
  $scope.getMultas = function(lineas){
    var total = 0;
    lineas.forEach(function(line){
      total +=  ((line.multa)*1);
    });
    return total;
  }
  //------
  $scope.getMoras = function(lineas){
    var total = 0;
    lineas.forEach(function(line){
      total +=  ((line.interes)*1);
    });
    return total;
  }
  //------
  $scope.getTotal = function(lineas){
    var total = 0;
    lineas.forEach(function(line){
      total +=  ((line.total)*1);
    });
    return total;
  }
  //------
  $scope.getSubTotal = function(lineas){
    var total = 0;
    lineas.forEach(function(line){
      total +=  ((line.subTotal)*1);
    });
    return total;
  }
  //-------------------------------------------------
}
);
