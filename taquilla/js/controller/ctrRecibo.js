var app = angular.module('taquillApp' );

app.controller('ctrRecibo', function($scope, $timeout,$window,$state, localStorageService, srvRestApi, $filter){
  var declaracion = localStorageService.get('lsDeclaracion');

  $scope.patente = declaracion.patente;

  $scope.persona = declaracion.persona;
  $scope.linea = declaracion.linea;
  $scope.usuario = declaracion.usuario;
  var data = {};
  data.id = $scope.patente['persona_id'];
  data.paginate = "5";
  data.status = "";

  srvRestApi.findRecibos(data)
          .then(function(response){

              $scope.recibos = response.data.data;
              $scope.recibos.forEach(function(recibo){
                      var dataR = {};
                      dataR.id = recibo.id;
                      srvRestApi.findLinea(dataR).then(function(response){
                                    var totalLinea = 0;
                                    response.data.forEach(function(line){
                                        totalLinea = (totalLinea*1) + (line.total)*1;
                                    });
                                    recibo.total = totalLinea;
                                    recibo.lineas = response.data;
                                });

              });

          });


  $scope.agregarRecibo = function (){
    localStorageService.set('lspatente', $scope.patente );
  }


//  $scope.ramo = ($scope.patente.tipo == 'comercio')?'Actividades Economicas':'Propiedad inmobiliaria';

   $scope.printIt = function(){
     html2canvas(document.getElementById('printArea'), {
         onrendered: function (canvas) {
             var data = canvas.toDataURL();
             var docDefinition = {

                 content: [{
                     image: data,
                     width: 500,
                     height:400,

                 }]
             };
             pdfMake.createPdf(docDefinition).open("test.pdf");
         }
     });

   };

 $scope.printIt2 = function(){
   var table = document.getElementById('printArea').innerHTML;
     var myWindow = $window.open('', '', 'width=800, height=600');
     myWindow.document.write(table);
     myWindow.print();

     $state.go('declaracion');

 };


});
