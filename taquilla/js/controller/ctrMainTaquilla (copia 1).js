var app = angular.module('taquillApp' );
app.config(function($mdIconProvider) {
  $mdIconProvider
    .defaultIconSet('../js/component/material-icon-extra/mdi.svg');
});
  app.controller('ctrMainTaquilla', function($scope, $rootScope, $window, $state,facGlobal, localStorageService, srvRestApi){
    $scope.userLogin = false;
    $scope.usuario = localStorageService.get('lsUsuario');
    $rootScope.reciboEliminar = {};
    if(($scope.usuario==null)||($scope.usuario==undefined))
    $state.go('login');
    else {
      $scope.userLogin = true;
        if($scope.usuario.nivel == 'configuracion')
         $state.go('verConfiguracion');
    }

    this.openMenu = function() {

      };
      srvRestApi.getZonas();
      srvRestApi.getTipologia();
      srvRestApi.getAlicuotas();
      srvRestApi.getValorSuelos();
      srvRestApi.getDepreciaciones();
      srvRestApi.getSalarioActivo();
      srvRestApi.getActividadBase();
      srvRestApi.getActividadEconomica();
      srvRestApi.getConceptoProcedimiento();
      srvRestApi.getCostosConstruccion();

      srvRestApi.getItems();
      //---------------------------------------------------------------------------
      srvRestApi.globalParams()
      .then(function(response){
        localStorageService.set('lsGlobalParams',response.data);
      });
      //---------------------------------------------------------------------------
      srvRestApi.actualPayDate()
      .then(function(response){
        localStorageService.set('lsActualPayDate',response.data);
      });
      //---------------------------------------------------------------------------
      srvRestApi.diaHabil()
      .then(function(response){
        localStorageService.set('lsDiaHabil',response.data);
      });

      //---------------------------------------------------------------------------
      srvRestApi.licTasas()
      .then(function(response){
        localStorageService.set('lsTasasLicor',response.data);
      });

    //---------------------------------------------------------------------------
    srvRestApi.pubTasas();

    //---------------------------------------------------------------------------
    srvRestApi.tasas()
    .then(function(response){
      facGlobal.setTasasAdministrativas(response.data);
      localStorageService.set('lsTasasAdministrativas',response.data);
    });
    //---------------------------------------------------------------------------
    srvRestApi.getIngTasas()
    .then(function(response){
      localStorageService.set('lsTasasIngieneria',response.data);
    });
    //---------------------------------------------------------------------------
    srvRestApi.depreciacion({})
    .then(function(response){
      localStorageService.set('lsDepreciacion',response.data);
    });
    //---------------------------------------------------------------------------
    srvRestApi.dateServer({})
    .then(function(response){
      localStorageService.set('lsDate',response.data);
      //get ultmos 7 años
      var data = {};
      data.ano = response.data.ano - 7;

      srvRestApi.getIntereses(data)
      .then(function(response){
        console.log(response.data);
        localStorageService.set('lsIntereses',response.data);
      });
    });
    //---------------------------------------------------------------------------
    //---------------------------------------------------------------------------
    srvRestApi.historicoUtributaria({})
    .then(function(response){
      localStorageService.set('lsUtributariaHistorico',response.data);
    });
    //---------------------------------------------------------------------------
    //---------------------------------------------------------------------------
    srvRestApi.getUniTributaria({})
    .then(function(response){
      localStorageService.set('lsUtributariaActiva',response.data);
    });
    //---------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    //----------------------codigo john nuñez--------------------------------------------------------
    $scope.formCrearPropiedad = function(){
      $scope.patente =[];
      //localStorageService.set('lspatente',undefined);
      //localStorageService.set('lsFicha',undefined);
      console.log('prueba');
      $state.go('newDecla');
      console.log('paso');
    }
    //---------------------codigo john nuñez.............


    $scope.buscarPatente = function(){
      if(($scope.numero != undefined)&&($scope.numero != ""))
      {

        $scope.patente = {};
        $scope.patente.tipo = 'todo';
        $scope.patente.patente = $scope.numero;
        $scope.patente.direccion = '';
        $scope.patente.nombre = '';
        //-----------------------------------
        srvRestApi.findPatente($scope.patente)
        .then(function(response){
          $scope.patentes = response.data.data;
        },function(response){

        });
        //------------------------------------
        var data = {};
        data.rif = $scope.numero
        data.id = "";
        srvRestApi.findPersona(data)
        .then(function(response){
          $scope.personas = response.data.data;
        },function(response){

        });
      }
    }


    //---------------------------------------------------------------------------

    $scope.selObj = function(obj,tipo){
      console.log(obj);
      localStorageService.set('lspatente',null);
      localStorageService.set('lsPersona',null);
      if(tipo == "patente")
      localStorageService.set('lspatente',obj);
      if(tipo == "persona")
      localStorageService.set('lsPersona',obj);
  console.log(obj);
      $scope.usuario = localStorageService.get('lsUsuario');

      if($scope.usuario.nivel == 'taquilla')
      $state.go('registrarPago');
      if($scope.usuario.nivel == 'gestion')
      $state.go('mostrarRecibo');
      if($scope.usuario.nivel == 'dgestion')
      $state.go('mostrarRecibo');
      if($scope.usuario.nivel == 'simple')
      $state.go('registrarSimple');



    }



    $scope.getToken = function(){
      srvRestApi.getToken($scope.loginDat).then(
        function(response){

          localStorageService.set('lsUsuario',response.data);

          $scope.userLogin = true;
          $window.location.href = '/taquilla';

        },function(response){
          alert(response.data.error);
        }
      );
    }

    $scope.logout = function(){

      srvRestApi.logout({});
      localStorageService.set('lsUsuario',null);
      $scope.userLogin = false;
      $state.go('login');


    }

    $scope.usuarioEdit = function(){


      $state.go('cambiar');


    }


    $scope.cambiarClave = function(){

      if(($scope.cambiar.new != $scope.cambiar.renew)){
        alert('Las Claves no Coninciden');
      }else {
        srvRestApi.cambiarClave($scope.cambiar)
           .then(function(response){
                alert('La clave se cambio con exito');


              },
              function(response){
                 alert(response.data.error);

              });
      }

      $state.go('cambiar');


    }
  });
