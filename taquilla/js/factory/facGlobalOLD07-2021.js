app = angular.module("taquillApp");

app.factory("facGlobal",function($filter){

     var zona = {};
     var alicuota = {};
     var items = {};
     var tipologia = {};
     var valorsuelo = {};
     var depreciacion = {};
     var salarioActual = {};
     var tasasAdministrativas = {};
     var tasasPublicidad = {};
     var tasasIngienieria = {};
     var actividadBase = {};
     var umariBase = {}; // 19-01-2021 john nunez
     var actividadEconomica = {};
     var tasasCatastro = {};

     var costosContruccion = {};
     var conceptoProcedimiento= {};
     var tipoProcedimiento = [{codigo:'Edificaciones'},
                             {codigo:'Urbanismo'}];
     var IdTasasIngienerias = [3,7, 8, 9, 10, 11.12];
     var IdTasasCatrastro = [3,30, 31, 33, 67, 68];
     var tipoInmueble = [{codigo:'Casa Economica'},
                             {codigo:'Casa Tradicional'},
                             {codigo:'Quinta'},
                             {codigo:'Pent House'},
                             {codigo:'Town House'},
                             {codigo:'Centro Comercial'},
                             {codigo:'Local Comercial'},
                             {codigo:'Hotel'},
                             {codigo:'No Aplica'},
                             {codigo:'Galpón'}];



     var global = {
           //Items base
           setItems : function(_items){
             items = _items;
           },
           getItems : function(){
             return items;
           },

           //Actividad base
           setActividadEconomica : function(_actividad){
             actividadEconomica = _actividad;
           },
           getActividadEconomica : function(){
             return actividadEconomica;
           },
           //Actividad base
           setActividadBase : function(_actividad){
             actividadBase = _actividad;
           },
           getActividadBase : function(){
             return actividadBase;
           },
          // utmari Base 19-01-2021 john nunez
          setUmariBase : function(_umari){
            umariBase = _umari;
          },
              getUmariBase : function(){
                return umariBase;
              },


          //Salario Minimo actual
          setSalarioActual : function(_salario){
            salarioActual = _salario[0];
          },
          getSalarioActual : function(){
            return salarioActual;
          },
         //Tasas Administrativas
         setTasasAdministrativas : function(_tasas){
              tasasAdministrativas = _tasas;
              //tasas de catastro
              tasasCatastro = $filter('filter')
                   (tasasAdministrativas,
                       function(tasa){
                         if( IdTasasCatrastro.indexOf(tasa.id) > -1 )
                          return tasa
                     });
            //-------------------------
            //tasas de ingieneria
            tasasIngienieria = $filter('filter')
                 (tasasAdministrativas,
                     function(tasa){
                       if( IdTasasIngienerias.indexOf(tasa.id) > -1 )
                        return tasa
                   });
          //-------------------------

         },

         getTasasAdministrativas : function(){
             return tasasAdministrativas;
         },
         getTasasAdministrativasCatastro : function(){
           return tasasCatastro;
         },
         //----------john nuñez-------------------------
         setConceptoProcedimiento : function(_ingtasas){
              conceptoProcedimiento = _ingtasas;
            },

          getConceptoProcedimiento : function(){
             return conceptoProcedimiento;
          },
          //----------john nuñez-------------------------
          setTasaPublicidad : function(_pubtasas){
               tasasPublicidad = _pubtasas;
             },

           getTasaPublicidad : function(){
              return tasasPublicidad;
           },
          setCostosConstruccion : function(_costos){
               costosContruccion = _costos;
             },

           getCostosConstruccion : function(){
              return costosContruccion;
           },

         //----john nuñez----------------------------------
         getTasasAdministrativasIngieneria : function(){
           return tasasIngienieria;
         },
         //get newTipologias
         gettipoProcedimiento  : function(){
              return tipoProcedimiento;
         },
         //get newTipologias
         getTipoInmueble  : function(){
              return tipoInmueble;
         },
          //set zonas
        setZonas : function(_zona){
          zona = _zona;
        },
         // get zonas
        getZonas : function(){
          return zona;
        },
        //get zonas by id
        getZonaById :  function(_id){
              var result = $filter('filter')(zona,{"id":_id},true);
              return result[0];
         },
         //****************************************************************
         //set tipologias
       setTipologias : function(_tipologia){
         tipologia = _tipologia;
       },
        // get tipologia
       getTipologias : function(){
         return tipologia;
       },
       //get tipologia by id
       getTipologiaById :  function(_id){
             var result = $filter('filter')(tipologia,{"id":_id},true);
             return result[0];
        },
        //get tipologia by id
        getTipologiaValor :  function(_zona_id,_tipologia){

              var result = $filter('filter')(tipologia,
                    function(tipo){
                        if((tipo.zona_id == _zona_id) && (tipo.tipologia == _tipologia))
                        return tipo;
                    });
              return result[0];
         },
        //****************************************************************
        //set depreciacions
      setDepreciaciones : function(_depreciacion){
        depreciacion = _depreciacion;
      },
       // get depreciacion
      getDepreciaciones : function(){
        return depreciacion;
      },
      //get depreciacion by id
      getDepreciacionById :  function(_id){
            var result = $filter('filter')(depreciacion,{"id":_id},true);
            return result[0];
       },
       //get porcentaje deprecion
       getDepreciacionValor :  function(_ano){

             var result = $filter('filter')(depreciacion,
                   function(depre){
                       if((depre.minimo <= (2019-_ano)) && (depre.maximo >= (2019-_ano)))
                       return depre;
                   });
             return result[0];
        },
       //********************************************************************
       //set valorsuelos
     setValorSuelos : function(_valorsuelo){
       valorsuelo = _valorsuelo;
     },
      // get valorsuelo
     getValorSuelos : function(){
       return valorsuelo;
     },
     //get valorsuelo by id
     getValorSueloById :  function(_id){
           var result = $filter('filter')(valorsuelo,{"id":_id},true);
           return result[0];
      },
      //get valor CONSTRUCCION VCC
      getValorVCC :  function(_zona_id,_uso){

        var result = $filter('filter')
                  (valorsuelo,
                    function(valor){
                        if ((valor.zona_id ==_zona_id) && (valor.uso == _uso)&&(valor.tipo == "VCC") )
                              return valor;
                      });
            return result[0];
       },
       //get tipologias Activas 22-01-2021------------
       getTipologiaActivas :  function(){

         var result = $filter('filter')
                   (tipologia,
                     function(tipo){
                         if (tipo.status =='Activo')
                               return tipo;
                       });
             return result;
        },
       // valor VC-------------------
       getValorVC :  function(_zona_id,_uso){

         var result = $filter('filter')
                   (valorsuelo,
                     function(valor){
                         if ((valor.zona_id ==_zona_id) && (valor.uso == _uso)&&(valor.tipo == "VC") )
                               return valor;
                       });
             return result[0];
        },
       //get valor TERRNO VCT
       getValorVCT :  function(_zona_id,_uso){

        result = $filter('filter')
                   (valorsuelo,
                     function(valor){
                         if((valor.zona_id ==_zona_id) && (valor.uso == _uso)&&(valor.tipo == "VCT"))
                               return valor;
                       });
             return result[0];
        },
      //********************************************************************
      //set alicuotas
    setAlicuotas : function(_alicuota){
      alicuota = _alicuota;
    },
     // get alicuota
    getAlicuotas : function(){
      return alicuota;
    },
    //get alicuota by id
    getAlicuotaById :  function(_id){

          var result = alicuota.filter(function(ali){
                        if (ali.id == _id)
                          return ali;
                        });
          if ((result == undefined)||(result.length == 0))
             result.porcentaje = 0;
            console.log(result);
          return result[0];
     }

     };


    return global;

});
