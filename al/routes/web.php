<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return view('alcaldia.index');
});

$router->get('home', ['middleware' => 'auth',function () use ($router) {
    return view('alcaldia.home');
}]);


$router->get('/partials/{resource}/{action}',   function ($resource,$action)  {
    return view('alcaldia.partials.'.$resource.'.'.$action);
});
//-------------CONFIGURACIONES--------------------------------------------------

$router->get('/api/v1/configuracion/unidadcobro', 'ConfiguracionController@getUnidadCobro');
$router->get('/api/v1/configuracion/unidadvalor/{unidad_id}', 'ConfiguracionController@getUnidadValor');
$router->get('/api/v1/configuracion/departamento', 'ConfiguracionController@getDepartamentos');
$router->get('/api/v1/configuracion/traer/usuarios', 'ConfiguracionController@todosUsuarios');
$router->get('/api/v1/configuracion/item/{departamento_id}', 'ConfiguracionController@getItem');
$router->get('/api/v1/configuracion/unidad/{unidad_id}', 'ConfiguracionController@getUnidad');
$router->post('/api/v1/configuracion/guardar/item',  'ConfiguracionController@guardarItem');
$router->post('/api/v1/configuracion/crear/usuario',  'ConfiguracionController@crearNewUser');//john nuñez
$router->post('/api/v1/configuracion/modificar/usuario',  'ConfiguracionController@modificarUser');//john nuñez
$router->post('/api/v1/configuracion/eliminar/usuario',  'ConfiguracionController@eliminarUser');//john nuñez
$router->get('/api/v1/configuracion/items', 'ConfiguracionController@getItems');//john nuñez
$router->post('/api/v1/configuracion/unidad/nuevoMonto', 'ConfiguracionController@newMontoUnidad');//john nuñez /13-02-2020
$router->get('/api/v1/utilidad/enum/{tabla}/{campo}', 'UtilController@getEnumColumnValues');//john nuñez
//---------------------ITEMS----------------------------------------------------

$router->post('/api/v1/declarar/guardar/item',  'ItemController@guardarLineas');

//$router->get('tu',['middleware' => ['auth', 'PersonaController@test']);
$router->get('/date', 'UtilController@date');
$router->post('/api/v1/tercero/new', 'PersonaController@create');
$router->post('/api/v1/tercero/edit', 'PersonaController@edit');
$router->get('/api/v1/tercero/{token}', 'PersonaController@index');
$router->get('/api/v1/patente/{token}', 'PatenteController@index');
$router->post('/api/v1/logout', 'UserController@logout');
$router->post('/api/v1/cambiar/clave', 'UserController@changePass');
$router->get('activar/{code}', 'UserController@activar');
$router->post('/api/v1/user', 'UserController@create');
$router->get('/api/v1/user', 'UserController@index');
$router->post('/api/v1/login', 'UserController@login');
$router->post('/api/v1/loginSystem', 'UserController@loginSystem');
$router->post('/api/v1/patente/old', 'PatenteController@findPatente');
$router->post('/api/v1/declara/old',  'DeclararController@create');
$router->post('/api/v1/declara/update',  'DeclararController@updateDeclaracion');
$router->post('/api/v1/catastro/guardar',  'CatastroController@create');
$router->post('/api/v1/catastro/reporte/totales',  'CatastroController@reporteTotales');
$router->post('/api/v1/catastro/crear',  'CatastroController@newCatastro');
$router->post('/api/v1/declara/crear',  'DeclararController@newPatente');//creado por john
$router->post('/api/v1/catastro/buscarVincular',  'CatastroController@findPatenteVincular');//creado por john aqui
$router->post('/api/v1/catastro/buscar/patentePadres',  'CatastroController@findPatentePadre');//creado por john aqui
$router->post('/api/v1/catastro/editar',  'CatastroController@editCatastro');
$router->post('/api/v1/catastro/editar/fichaCatastral',  'CatastroController@editarFichaCatastral');//john nuñez
$router->post('/api/v1/catastro/find',  'CatastroController@findCatastroId');
$router->post('/api/v1/catastro/patente/find',  'CatastroController@findCatastroIdPatente');
$router->post('/api/v1/catastro/patente/eliminar',  'CatastroController@eliminarPatenteCatastro');//creado por john nuñez
$router->post('/api/v1/catastro/patente/eliminar/comentario',  'CatastroController@findComentarioPatente');//creado por john nuñez
$router->post('/api/v1/catastro/patente/activar',  'CatastroController@activarPatenteCatastro');//creado por john nuñez
$router->post('/api/v1/catastro/recibo/manual',  'CatastroController@reciboManual');//creado por john nuñez
$router->get('/api/v1/ingieneria/procedimientos',  'TasaController@conceptoProcedimiento');//creado por john nuñez
$router->get('/api/v1/ingieneria/costos',  'TasaController@costosContruccion');//creado por john nuñez
//----------------UTILS-----------------------------------------------------------------------
$router->post('/api/v1/reporte', 'UtilController@reporteCatastro');// creado por john nuñez 04/03/2020
$router->post('/api/v1/contribuyente/buscar/persona',  'PersonaController@buscarPersonaId');
$router->post('/api/v1/contribuyente/departamentos',  'UtilController@findDepartamento');
$router->post('/api/v1/catastro/depreciacion',  'UtilController@tablaDepreciacion');
$router->get('/api/v1/catastro/depreciaciones',  'UtilController@getDepreciacion');
$router->get('/api/v1/catastro/valorsuelo',  'UtilController@getValorSuelo');
$router->get('/api/v1/catastro/tipologia',  'UtilController@getTipologia');
$router->get('/api/v1/catastro/alicuota',  'UtilController@getAlicuota');
$router->get('/api/v1/catastro/zona',  'UtilController@getZona');
$router->post('/api/v1/declara/vinculacion/patente', 'UtilController@vinculacionPatentes');///*********john nuñez*********
$router->post('/api/v1/declara/find/recibo', 'DeclararController@findRecibos');
$router->post('/api/v1/declara/find/recibo/linea', 'DeclararController@findLineaRecibo');
$router->post('/api/v1/persona/find', 'PersonaController@findPersona');
$router->post('/api/v1/persona/patente/guardar', 'PersonaController@updatePersonaPatente');
$router->post('/api/v1/persona/find/only', 'PersonaController@findPersonaOnly');
$router->post('/api/v1/recibo/pagar', 'DeclararController@pagarRecibo');
$router->post('/api/v1/declara/eliminar/recibo', 'DeclararController@eliminarRecibo');//*********john nuñez**********
$router->post('/api/v1/declara/vinculacion/eliminar/patente', 'UtilController@eliminarVinculadas');//***********john nunez *************
$router->post('/api/v1/declara/vinculacion/Patentevinculada', 'UtilController@getPatentesVinculadas');///*********john nuñez*********
$router->post('/api/v1/ingieneria/contribuyente/Patente', 'PatenteController@getPatentes');///*********john nuñez*********
$router->post('/api/v1/ingieneria/guardar/cobro', 'DeclararController@guardarReciboIngenieria');///*********john nuñez*********
$router->post('/api/v1/actividad/economica', 'ActividadController@getActividades');

$router->get('/api/v1/actividad/economica', 'UtilController@getActividades');

$router->post('/api/v1/actividad/economica/eliminar', 'ActividadController@eliminarActividad');
$router->post('/api/v1/actividad/economica/asociar', 'ActividadController@asociarActividad');
$router->post('/api/v1/actividad/economica/find', 'ActividadController@findActividades');
$router->post('/api/v1/tasa/administrativa', 'TasaController@index');
$router->post('/api/v1/tasa/ingieneria', 'TasaController@ingTasa');
$router->post('/api/v1/tasa/publicidad', 'TasaController@pubTasa');
$router->post('/api/v1/tasa/licor', 'TasaController@licTasa');
$router->post('/api/v1/recibo/reimprimir', 'DeclararController@reImprimirRecibo');
$router->post('/api/v1/unidad/tributaria', 'UtributariaController@getUniTributariaActiva');
$router->post('/api/v1/unidad/tributaria/historico', 'UtributariaController@getUniTributariaHistorico');

$router->get('/api/v1/salario/minimo/actual', 'UtilController@getSalarioActivo');
$router->get('/api/v1/actividad/base', 'UtilController@getActividadBase');
// umari valor 19-01-2021 john nunez--------------------------------------------
$router->get('/api/v1/utmari/base', 'UtilController@valorVijenteUmari');
$router->post('/api/v1/interes/bcv', 'UtilController@getIntereses');

$router->post('/api/v1/day', 'UtilController@day');
$router->post('/api/v1/day/work', 'UtilController@diaHabil');
$router->post('/api/v1/actual/pay/period', 'UtilController@actualPayDate');
$router->post('/api/v1/global/params', 'UtilController@parametrosGlobales');
$router->post('/api/v1/trimestre', 'UtilController@trimestre');

$router->get('/api/v1/test2', 'UtilController@test2');

$router->post('/api/v1/publicidad/patente/agregar', 'PublicidadController@agregarPublicidad');
$router->post('/api/v1/publicidad/patente', 'PublicidadController@getPublicidades');
$router->post('/api/v1/publicidad/recibo/guardar', 'PublicidadController@guardarRecibo');

$router->post('/api/v1/licores/recibo/guardar', 'LicorController@guardarRecibo');
$router->post('/api/v1/licores/patente/agregar', 'LicorController@agregarLicor');
$router->post('/api/v1/licores/patente', 'LicorController@getLicores');

$router->post('/api/v1/recibo/patente/find', 'DeclararController@findRecibosPatente');

$router->post('/api/v1/contribuyente/buscar/patente', 'PersonaController@buscarPatentes');//************john nuñez******
$router->post('/api/v1/contribuyente/buscar', 'PersonaController@buscarPersona');//************john nuñez******
$router->post('api/v1/vehiculo/agregar', 'VehiculoController@agregarVehiculo');//************john nuñez******
$router->post('/api/v1/contribuyente/guardar', 'PersonaController@findOrCreatePerson');
$router->post('/api/v1/tasa/administrativa/recibo/guardar', 'DeclararController@createTasa');
$router->post('/api/v1/tasa/publicidad/recibo/guardar', 'DeclararController@createTasaPublicidad');
$router->post('/api/v1/catastro/recibo/guardar',  'DeclararController@createCatastro');
$router->post('/api/v1/catastro/orden/guardar',  'DeclararController@createOrderCatastro');


$router->post('/probando','DeclararController@deleteRecibo');



$router->get('/webadmin','UserController@seratInit');
$router->get('/webadmin/user/list','UserController@indexSys');
$router->post('webadmin/user/edit', 'UserController@updateSys');
$router->post('webadmin/user/inactive', 'UserController@inactiveSys');
Route::get('/webadmin/{resource}/{action}',function ( $resource,$action ){

     return view('serat.'.$resource.'.'.$action);

});
