<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateTablaPersonas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {

            $table->increments('id');
            $table->string('nombre');
          
            $table->string('calle',60);
            $table->enum('tipo',['Juridica','Natural']);
            $table->string('zona',60);
            $table->string('direccion',100);
            $table->string('direccion2',100);
            $table->string('ciudad',60);
            $table->string('postal',10);
            $table->date('fecha');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                  ->references('id')
                  ->on('users');


            // Constraints declaration
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
