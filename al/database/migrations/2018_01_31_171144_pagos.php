<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pagos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagos', function (Blueprint $table) {
          $table->increments('id');
          $table->timestamps();
          $table->integer('declaracion_id')->unsigned();
          $table->integer('user_id')->unsigned();
          $table->string('banco',60);
          $table->string('tipo',20);
          $table->string('referencia',15);
          $table->string('monto',15);
          $table->foreign('declaracion_id')->references('id')->on('declaraciones');
          $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagos');
    }
}
