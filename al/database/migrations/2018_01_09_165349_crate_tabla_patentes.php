<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateTablaPatentes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patentes', function (Blueprint $table) {
             $table->increments('id');
            $table->string('numero',30);
            $table->string('tipo',2);
            $table->date('fecha');
            $table->boolean('status')->default(true);
            $table->enum('pago',['Solvente','Insolvente','Reparar','Verificar'])->default('Verificar');
            $table->integer('persona_id')->unsigned();
            $table->integer('tipo_id')->unsigned();
            $table->integer('direccion_id')->unsigned()->nullable();
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->foreign('tipo_id')->references('id')->on('tipos');
            $table->foreign('direccion_id')->references('id')->on('direcciones');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patentes');
    }
}
