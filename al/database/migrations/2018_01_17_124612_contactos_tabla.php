<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContactosTabla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contactos', function (Blueprint $table) {
          $table->increments('id');
          $table->string('contacto',60);
          $table->enum('tipo',['telefono','twiter','facebook','instagram','whatsapp','correo','otro']);
          $table->boolean('status')->default(true);
          $table->integer('persona_id')->unsigned();
          $table->foreign('persona_id')->references('id')->on('personas');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contactos');
    }
}
