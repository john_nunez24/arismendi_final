<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PatenteOldTabla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patenteolds', function (Blueprint $table) {
           $table->increments('id');
           $table->string('patente',20)->nullable();
           $table->string('nombre',100)->nullable();
           $table->string('direccion',255)->nullable();
           $table->integer('dia')->nullable();
           $table->integer('mes')->nullable();
           $table->integer('ano')->nullable();
           $table->string('telefono',20)->nullable();
           $table->string('email',60)->nullable();
           $table->string('calle',60)->nullable();
           $table->integer('codigo')->nullable();
           $table->enum('tipo',['comercio','inmobiliaria','licores','publicidad'])->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patenteolds');
    }
}
