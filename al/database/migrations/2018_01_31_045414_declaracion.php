<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Declaracion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('declaraciones', function (Blueprint $table) {
          $table->increments('id');
          $table->timestamps();
          $table->integer('patente_id')->unsigned();
          $table->integer('user_id')->unsigned();
          $table->string('monto',60);
          $table->string('imes',15);
          $table->string('iano',15);
          $table->string('hmes',15);
          $table->string('hano',15);
          $table->string('descuento',15);
          $table->string('interes',15);
          $table->string('total',15);         
          $table->string('alicuota',4);
          $table->foreign('patente_id')->references('id')->on('patenteolds');
          $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('declaraciones');
    }
}
