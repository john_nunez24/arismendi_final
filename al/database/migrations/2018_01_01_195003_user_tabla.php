<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserTabla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rif',15)->unique();
            $table->string('correo',60);
            $table->string('telefono',15);
            $table->string('password',60);
            $table->boolean('confirmado')->default(false);
            $table->string('validate_code',64);
            $table->enum('status',['Activo','Inactivo','Recuperacion'])->default('Inactivo');
            $table->enum('tipo',['cliente','sistema'])->default('cliente');
            $table->enum('nivel',['super','cliente','consulta','gestion'])->default('cliente');
            //para recuperar la clave debe estar en status recuperacion y confirmado falso
            //para activar usuario debe estar en status Inactivo y confirmado falso
            //el usuario inactivo es aquel que esta en status inactivo y confirmado true;
            $table->string('api_token',60)->unique();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
