<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model {
    protected $table = 'perfiles';
    protected $fillable = ["value", "nombre", "departamento_id", "status"];

    protected $dates = [];
    public $timestamps = false;
    public static $rules  = [

    ];

    public static $messages = [

    ];



}
