<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Pagorecibo extends Model {
    protected $table = 'pago_recibo';
    protected $fillable = ["recibo_id", "pago_id", "monto_total", "status"];

    protected $dates = [];
    public $timestamps = false;
    public static $rules  = [

    ];

    public static $messages = [

    ];



}
