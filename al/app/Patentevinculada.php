<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Patentevinculada extends Model {
  protected $table = 'patente_vinculada';
    protected $fillable = ["patente_padre","patente_hija","status", "comentario",
                            "usuario_id"];



    public static $rules = [

    ];

    public static $messages = [


    ];

    public function publicidad()
    {
        return $this->hasMany("App\Publicidad","patente_id");
    }


}
