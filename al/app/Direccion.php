<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Direccion extends Model {

    protected $fillable = ["numero", "tipo", "patente_id", "persona_id"];
    protected $table = "direcciones";
    protected $dates = ["fecha"];

    public static $rules = [
        "numero" => "required",
        "tipo" => "required",
        "patente_id" => "required|numeric",
        "persona_id" => "required|numeric",
    ];

    public static $messages = [
      "numero.required" => "El numero de Patente y/o Permiso es requerido",
      "tipo.required" => "El tipo de Patente y/o Permiso es requerido",

    ];

    public function persona()
    {
        return $this->belongsTo("App\Persona");
    }


}
