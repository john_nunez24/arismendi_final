<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Actividadbase extends Model {
    protected $table = 'actividad_base';
    protected $fillable = ["minimo", "porcentaje", "status", "actividad_id"];

    protected $dates = [];

    public static $rules  = [

    ];

    public static $messages = [

    ];



}
