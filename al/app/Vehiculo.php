<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehiculo extends Model {



    protected $fillable = ["patente_id", "placa", "marca",
                           "modelo","color","clase",
                           "tipo","serial","ano",
                           "uso","status"];



    public static $rules = [

    ];

    public static $messages = [


    ];

    /*public function patente(){
      return $this->belongsTo("App\Patenteold");
    }*/


}
