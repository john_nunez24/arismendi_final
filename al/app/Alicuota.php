<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Alicuota extends Model {
   
    protected $fillable = ["codigo", "tipo", "porcentaje", "status"];
    public $timestamps = false;



    public static $rules = [

    ];

    public static $messages = [


    ];




}
