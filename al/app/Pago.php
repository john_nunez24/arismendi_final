<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Recibo;
class Pago extends Model {

    protected $fillable = ["banco", "referencia", "tipo", "monto","recibo_id"];



    public static $rules = [

    ];  

    public static $messages = [


    ];

    public function recibo(){
      return $this->belongsTo("App\Recibo");
    }


}
