<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Pubtasa extends Model {

    protected $fillable = ["nombre", "ut", "factor","etiqueta","tipo"];



    public static $rules = [

    ];

    public static $messages = [

    ];
    public function publicidad()
    {
        return $this->hasMany("App\Publicidad");
    }

}
