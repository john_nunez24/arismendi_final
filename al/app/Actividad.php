<?php namespace App;
 
use Illuminate\Database\Eloquent\Model;

class Actividad extends Model {
    protected $table = 'actividades';
    protected $fillable = ["nombre", "codigo", "status","minimo","porcentaje"];

    protected $dates = [];

    public static $rules = [

    ];

    public static $messages = [

    ];



}
