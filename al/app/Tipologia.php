<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipologia extends Model {

    protected $fillable = ["sector","zona","previd"];
    protected $table = "tipologia";

    public static $rules = [

    ];

    public static $messages = [


    ];

}
