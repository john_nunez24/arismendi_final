<?php namespace App;

use Illuminate\Database\Eloquent\Model;
      
class Depreciacion extends Model {
    protected $table = 'depreciacion';
    protected $fillable = ["edad", "excelente", "bueno", "regular","malo"];
    public $timestamps = false;


    public static $rules = [

    ];

    public static $messages = [


    ];




}
