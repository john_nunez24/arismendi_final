<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Valorsuelo extends Model {
    protected $table = 'valor_suelo';
    protected $fillable = ["zona_id", "tipo", "uso","valor","porcentaje","status"];
 
    protected $dates = [];

    public static $rules = [

    ];

    public static $messages = [

    ];



}
