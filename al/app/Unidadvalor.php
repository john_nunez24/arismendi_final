<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Unidadvalor extends Model {

    protected $fillable = ["unidad_id", "valor", "desde", "hasta","user_id","status"];
    //public $timestamps = false;
    protected $table = 'unidad_valor';


    public static $rules = [

    ];

    public static $messages = [


    ];




}
