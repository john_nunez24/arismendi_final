<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Patenteold extends Model {

    protected $fillable = ["patente","nombre","alicuota", "tipo","departamento_id",
                            "rif","direccion","dia","mes","ano",
                             "telefono","email","calle","codigo","tipo","persona_id"];



    public static $rules = [

    ];

    public static $messages = [


    ];

    public function publicidad()
    {
        return $this->hasMany("App\Publicidad","patente_id");
    }


}
