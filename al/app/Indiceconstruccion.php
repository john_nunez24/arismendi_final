<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Indiceconstruccion extends Model {
  protected $table = 'indice_construccion';
    protected $fillable = ["tipo","codigo","clasificacion", "icc",
                            "status"];



    public static $rules = [

    ];

    public static $messages = [


    ];

    /*public function publicidad()
    {
        return $this->hasMany("App\Publicidad","patente_id");
    }
*/

}
