<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Publicidad extends Model {

    protected $fillable = ["pubtasa_id","factor","direccion",
                           "status",
                            "patente_id", "user_id"];
    protected $table = "publicidades";

    public static $rules = [

    ];

    public static $messages = [


    ];

    public function patente()
    {
        return $this->belongsTo("App\Patenteold","patente_id");
    }

    public function usuario()
    {
        return $this->belongsTo("App\User");
    }

    public function tasa()
    {
        return $this->belongsTo("App\Pubtasa");
    }

}
