<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Linea extends Model {



    protected $fillable = ["base", "imes", "iano",
                           "hmes","hano","descuento",
                           "interes","total","alicuota",
                           "multa","subTotal","status","patente_id",
                            "unidad","recibo_id","tipo","item_id","indice_id"];



    public static $rules = [

    ];

    public static $messages = [


    ];

    public function recibo(){
      return $this->belongsTo("App\Recibo");
    }


}
