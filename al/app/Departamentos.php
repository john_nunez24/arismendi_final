<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamentos extends Model {
    protected $table = 'departamentos';
    protected $fillable = ["nombre", "status"];
    public $timestamps = false;



    public static $rules = [

    ];

    public static $messages = [


    ];




}
