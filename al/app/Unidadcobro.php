<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Unidadcobro extends Model {

    protected $fillable = [ "abreviatura", "nombre","user_id","status"];
    public $timestamps = false;
    protected $table = 'unidad_cobro';


    public static $rules = [

    ];

    public static $messages = [


    ];




}
