<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Licor extends Model {

    protected $fillable = ["lictasa_id","factor",
                           "status",
                            "patente_id", "user_id"];
    protected $table = "licores";

    public static $rules = [

    ];

    public static $messages = [


    ];

    public function patente()
    {
        return $this->belongsTo("App\Patenteold","patente_id");
    }

    public function usuario()
    {
        return $this->belongsTo("App\User");
    }

    public function tasa()
    {
        return $this->belongsTo("App\Lictasa");
    }

}
