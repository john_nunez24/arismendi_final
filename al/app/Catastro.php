<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Catastro extends Model {

    protected $fillable = ["patente_id", "user_id", "construccion","uso_id",
                           "direccion","zona_id",
                           "tipologia","alicuota_id",
                           "terreno", "uso","zona","estado","ano","norte",
                           "sur","este","oeste","registro","asiento","matriculado","folio",
                          "urbanizacion","avenida","calle","lote"];



    public static $rules = [

    ];

    public static $messages = [


    ];




}
