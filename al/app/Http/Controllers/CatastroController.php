<?php

namespace App\Http\Controllers;


use App\Patenteold;
use App\Pago;
use App\Linea;
use App\Persona;
use App\Recibo;
use App\Utributaria;
use App\User;
use App\Catastro;
use App\Contacto;
use App\Comentario;
use App\Depreciacion;
use App\Patentevinculada;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Contracts\Support\Jsonable;
class CatastroController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function findOrCreatePerson($patente,$user){
    try {

      $persona = Persona::where('rif',$patente['rif'])->firstOrFail();

      return $persona;
    } catch (ModelNotFoundException $e) {

      $persona = new Persona;
      $persona['nombre'] = $patente['nombre'];
      $persona['rif'] = $patente['rif'];
      $persona['direccion'] = $patente['direccion'];
      $persona['user_id'] = $user['id'];
      $persona->save();
      $patente['persona_id'] = $persona['id'];
      $patente->save();
      return $persona;
    }


  }

  public function findOrCreatePersonOnly($patente,$user){
    try {

      $persona = Persona::where('rif',$patente['rif'])->firstOrFail();

      return $persona;
    } catch (ModelNotFoundException $e) {

      $persona = new Persona;
      $persona['nombre'] = $patente['nombre'];
      $persona['rif'] = $patente['rif'];
      $persona['direccion'] = $patente['direccion'];
      $persona['user_id'] = $user['id'];
      $persona->save();

      return $persona;
    }


  }


  public function findAndEditPerson($patente,$user){
    try {
      $persona = Persona::where('id',$patente['persona_id'])->firstOrFail();


      $persona['nombre'] = $patente['nombre'];
      $persona['rif'] = $patente['rif'];
      $persona['direccion'] = $patente['direccion'];
      $persona['user_id'] = $user['id'];
      $persona->save();

      return $persona;
    } catch (ModelNotFoundException $e) {

      $persona = new Persona;
      $persona['nombre'] = $patente['nombre'];
      $persona['rif'] = $patente['rif'];
      $persona['direccion'] = $patente['direccion'];
      $persona['user_id'] = $user['id'];
      $persona->save();

      return $persona;
    }


  }


  public function findOrCreateFicha($patente,$ficha,$user){
    try {

      $catastro = Catastro::where('patente_id',$patente['id'])->firstOrFail();

      return $catastro;
    } catch (ModelNotFoundException $e) {

      $catastro = new Catastro;
      $catastro['terreno'] = $ficha['terreno'];
      $catastro['construccion'] = $ficha['construccion'];
      $catastro['zona'] = $ficha['zona'];
      $catastro['uso'] = $ficha['uso'];
      $catastro['patente_id'] = $patente['id'];
      $catastro['user_id'] = $user['id'];
      $catastro->save();

      return $catastro;
    }


  }

  //---------------------------------------------------------------------
  public function createContact($dato,$tipo, $persona){

    if(($dato != "")){
      $contacto = new Contacto;
      $contacto['contacto'] = $dato;
      $contacto['tipo'] = $tipo;
      $contacto['persona_id'] = $persona['id'];
      $contacto->save();
      return $contacto;}


    }

    //------------------------------------agregar comentario------------------------
    public function addComentario($id,$comentario,$margen,$tabla,$user_id){
      $dbComentario = new Comentario;

      $dbComentario['referencia_id'] =  $id;
      $dbComentario['comentario'] =  $comentario;
      $dbComentario['margen'] =  $margen;
      $dbComentario['tabla'] =  $tabla;
      $dbComentario['user_id'] =  $user_id;
      $dbComentario->save();
    }




    //----------------------------------------------------------------------

    public function newCatastro(Request $request){


      $patente = $request->input('patente');
      $ficha = $request->input('ficha');
      $usuario = $request->input('usuario');
      $comentario = $request->input('comentario');

      try {

        //buscar patente
        $dbPatente = Patenteold::where('patente','1-'.$patente['catastro'])->firstOrFail();
        return response()->json(['error'=>'El numero de catastro '.$dbPatente['patente'].', ya existe y esta registrado a nombre de '.$dbPatente['nombre']],406);
      } catch (ModelNotFoundException $e) {
        //-buscar o crear persona ----------------
        $patente['rif'] = $patente['prefijo'].'-'.$patente['rif'];
        $dbPersona = $this->findOrCreatePersonOnly($patente,$usuario);

        //create patente
        $dbNewPatente = new Patenteold;
        $dbNewPatente['persona_id'] = $dbPersona['id'];
        $dbNewPatente['patente'] = '1-'.$patente['catastro'];
        $dbNewPatente['nombre'] = $patente['nombre'];
        $dbNewPatente['rif'] = $patente['rif'];
        $dbNewPatente['direccion'] = $patente['direccion'];
        $dbNewPatente['tipo'] = 'inmobiliaria';
        $dbNewPatente->save();

        //crear contactos
        $dbTelefono = $this->createContact($patente['telefono'],'telefono',$dbPersona);
        //$dbCorreo = $this->createContact($patente['correo'],'correo',$dbPersona);

        //crear comentario
        $margen = 'Registro nuevo catastro: 1-'.$patente['catastro'];
        $this->addComentario($dbNewPatente['id'],$comentario,$margen,'patente',$usuario['id']);

        $catastro = new Catastro;
        $catastro['terreno'] = $ficha['terreno'];
        $catastro['construccion'] = $ficha['construccion'];
        $catastro['zona_id'] = $ficha['zona_id'];
        $catastro['alicuota_id'] = $ficha['alicuota_id'];
        $catastro['tipologia'] = $ficha['tipologia'];
        $catastro['direccion'] = $patente['direccion'];
        $catastro['uso_id'] = $ficha['uso_id'];
      //  $catastro['estado'] = $ficha['estado'];
        $catastro['ano'] = $ficha['ano'];
        //$catastro['norte'] = $ficha['norte'];
      //  $catastro['sur'] = $ficha['sur'];
        //$catastro['este'] = $ficha['este'];
        //$catastro['oeste'] = $ficha['oeste'];
        $catastro['patente_id'] = $dbNewPatente['id'];
        $catastro['user_id'] = $usuario['id'];
        $catastro->save();

      }




    }
//----------------------john nuñez---------------------------------------------
public function reciboManual(Request $request){
    $patente =  $request->input('patente');
    $reciboManual =  $request->input('reciboManual');
    $user =  $request->input('user');

    $fechaDesde = explode("-" ,$reciboManual['dateDesde']);

        $fechaHasta = explode("-" ,$reciboManual['dateHasta']);

         $dbRecibo = new Recibo;
          $dbRecibo['persona_id'] = $patente['persona_id'];
          $dbRecibo['user_id'] = $user['id'];
          $dbRecibo['status'] = 'Pendiente';
          $dbRecibo['comentario'] = $reciboManual['reciboNumero'];
          $dbRecibo->save();

            $dbLinea = new Linea;
            $dbLinea['recibo_id'] =$dbRecibo['id'];
            $dbLinea['patente_id'] =$patente['id'];
            $dbLinea['user_id'] = $user['id'];

            $dbLinea['base'] = $reciboManual['monto'];
            $dbLinea['total'] = $reciboManual['monto'];
            $dbLinea['subTotal'] = $reciboManual['monto'];
            $dbLinea['tipo'] = 'ReciboManual';
            $dbLinea['status'] = 'PagoManual';
            $dbLinea['concepto'] = $reciboManual['concepto'];
            $dbLinea['imes'] = $fechaDesde[1];
            $dbLinea['iano'] = $fechaDesde[0];

                  $dbLinea['hmes'] = $fechaHasta[1];
                  $dbLinea['hano'] = $fechaHasta[0];

            $dbLinea->save();

}
//---------------------john nuñez-----------------------------------------------
public function eliminarPatenteCatastro(Request $request){
$patente =  $request->input('patente');
$comentario = $request->input('comentario');
$usuario =  $request->input('user');
  $dbPatentes = Patenteold::where('id' , $patente['id'])->first();
  $dbPatentes['status'] = 'Inactivo';
  $dbPatentes->save();
    $margen = 'Eliminado por Sistema N° de catastro: 1-'.$patente['catastro'].'-Bloqueado';

    $this->addComentario($patente['id'],$comentario,$margen,'patente',$usuario['id']);
}
//---------------------john nuñez-----------------------------------------------
public function activarPatenteCatastro(Request $request){
$patente =  $request->input('patente');
$comentario = $request->input('comentario');
$usuario =  $request->input('user');
  $dbPatentes = Patenteold::where('id' , $patente['id'])->first();
  $dbPatentes['status'] = 'Activo';
  $dbPatentes->save();
    $margen = 'Activa por Sistema N° de catastro: 1-'.$patente['catastro'].'-Activada por la Directora de Catastro.';

    $this->addComentario($patente['id'],$comentario,$margen,'patente',$usuario['id']);
}
//-----------------john nuñez--------------------------------------------------
public function findPatentePadre(Request $request){
    $idHija =  $request->input('idHija');
    $dbPatenteVinculadas = Patentevinculada::where('patente_hija' , $idHija)->get();
    //return $dbPatenteVinculada;
    $arrPadreId= [];
      foreach ($dbPatenteVinculadas as $dbPatenteVinculada) {
          array_push($arrPadreId , $dbPatenteVinculada['patente_padre']);
      }
      //$dbPatenteVinculadas = {};
        //return $arrPadreId;
        $dbPatentes = Patenteold::whereIn('id' , $arrPadreId)->get();
        //return $dbPatentes;
          foreach ($dbPatentes as $dbPatente) {
            $arrHijaId = [];
              $dbPatenteVinculadas = Patentevinculada::where('patente_padre' , $dbPatente['id'])->
              where('status' , 'Activo')->get();
                foreach ($dbPatenteVinculadas as $dbPatenteVinculada) {
                      array_push($arrHijaId , $dbPatenteVinculada['patente_hija']);
                }

              $dbPatente['hijas'] = Patenteold::whereIn('id' , $arrHijaId)->
                                                      where('id', '<>' , $idHija )->get();
                            foreach ($dbPatente['hijas'] as $dbPatenteHija) {
                              $tmp= linea::where('patente_id' , $dbPatenteHija['id'])
                                            ->where('status' , 'Pagada')
                                            ->orderBy('created_at' , true)->first();

                              $dbPatenteHija['ultimoPago'] = Pago::where('recibo_id' , $tmp['recibo_id'])
                                                          ->where('status' , 'Activa')->first();

                            }

              $tmp= linea::where('patente_id' , $dbPatente['id'])
                            ->where('status' , 'Pagada')
                            ->orderBy('created_at' , true)->first();

              $dbPatente['ultimoPago'] = Pago::where('recibo_id' , $tmp['recibo_id'])
                                          ->where('status' , 'Activa')->first();

          }
        return $dbPatentes;
}
//------------------------john nuñez----------------------------------------
public function editarFichaCatastral(Request $request){
  $patente = $request->input('patente');

  $ficha = $request->input('ficha');
  $usuario = $request->input('usuario');
  //$comentario = $request->input('comentario');

  $dbPatente = Patenteold::where('id' , $patente['id'])->first();

        $dbPatente['persona_id'] = $patente['persona_id'];
        $dbPatente['patente'] = $patente['catastro'];
        $dbPatente['nombre'] = $patente['nombre'];
        $dbPatente['rif'] = $patente['rif'];
        $dbPatente['direccion'] = $patente['direccion'];
        $dbPatente['tipo'] = 'inmobiliaria';

        $dbPatente->save();
        $dbCatastro =  Catastro::where('patente_id',$patente['id'])->first();

        $dbCatastro['terreno'] = $ficha['terreno'];
        $dbCatastro['zona_id'] = $ficha['zona_id'];
        $dbCatastro['construccion'] = $ficha['construccion'];
        $dbCatastro['alicuota_id'] = $ficha['alicuota_id'];
        $dbCatastro['alicuota2021_id'] = $ficha['alicuota2021_id'] *1;
        $dbCatastro['tipologia'] = $ficha['tipologia'];
        $dbCatastro['tipologia_id'] = $ficha['tipologia_id'] *1;
        $dbCatastro['direccion'] = $patente['direccion'];
        $dbCatastro['uso_id'] = $ficha['uso_id'];
        $dbCatastro['ano'] = $ficha['ano'];
        $dbCatastro['norte'] = $ficha['norte'];
        $dbCatastro['sur'] = $ficha['sur'];
        $dbCatastro['este'] = $ficha['este'];
        $dbCatastro['oeste'] = $ficha['oeste'];
        $dbCatastro['registro'] = $ficha['registro'];
        $dbCatastro['asiento'] = $ficha['asiento'];
        $dbCatastro['matriculado'] = $ficha['matriculado'];
        $dbCatastro['folio'] = $ficha['folio'];
        $dbCatastro['urbanizacion'] = $ficha['urbanizacion'];
        $dbCatastro['avenida'] = $ficha['avenida'];
        $dbCatastro['calle'] = $ficha['calle'];
        $dbCatastro['lote'] = $ficha['lote'];
        $dbCatastro->save();

        $result['patente'] = $dbPatente;
        $result['catastro'] = $dbCatastro;
        return $result;
}

//----------------------------------------------------------------------

    public function editCatastro(Request $request){
      $token = $request->header('token');
      $valUser = User::where('api_token', $token)->first();


      if(($valUser['nivel'] == 'dcatastro')||($valUser['nivel'] == 'auxcatastro')||($valUser['nivel'] == 'secrecatastro')||($valUser['nivel'] == 'catastro')){
        $patente = $request->input('patente');

        $ficha = $request->input('ficha');
        $usuario = $request->input('usuario');
        $comentario = $request->input('comentario');

        try {

          //buscar patente
          $dbPatente = Patenteold::where('patente','1-'.$patente['catastro'])->where('id','<>',$patente['id'])->firstOrFail();
          //solo permite que el usuario tipo auxcatastro edite una unica vez los registros.
          if(($dbPatente['persona_id']>0)&&(($valUser['nivel'] == 'auxcatastro')||($valUser['nivel'] == 'catastro')))
          return response()->json(['error'=>'Atención su usuario no puede editar este contribuyente. Debe solicitar al Director de Catastro, para editarlo'],406);

          return response()->json(['error'=>'El numero de catastro '.$dbPatente['patente'].', ya existe y esta registrado a nombre de '.$dbPatente['nombre']],406);
        } catch (ModelNotFoundException $e) {

          //-buscar o crear persona ----------------
          $patente['rif'] = $patente['prefijo'].'-'.$patente['rif'];
          $dbPersona = $this->findAndEditPerson($patente,$usuario);

          $patente['catastro'] = '1-'.$patente['catastro'];

          //create patente
          $dbPatente =  Patenteold::where('id',$patente['id'])->first();
          $oriPatente = $dbPatente;
          $dbPatente['persona_id'] = $dbPersona['id'];
          $dbPatente['patente'] = $patente['catastro'];
          $dbPatente['nombre'] = $patente['nombre'];
          $dbPatente['rif'] = $patente['rif'];
          $dbPatente['direccion'] = $patente['direccion'];
          $dbPatente['tipo'] = 'inmobiliaria';

          $dbPatente->save();

          //crear contactos
          //$dbTelefono = $this->editContact($patente['telefono'],'telefono',$dbPersona);
          //$dbCorreo = $this->editContact($patente['correo'],'correo',$dbPerson);

          try{
            $dbCatastro =  Catastro::where('patente_id',$patente['id'])->firstOrFail();
            $oriCatastro = $dbCatastro;
            $dbCatastro['terreno'] = $ficha['terreno'];
            $dbCatastro['zona_id'] = $ficha['zona_id'];
            $dbCatastro['construccion'] = $ficha['construccion'];
            //$dbCatastro['alicuota_id'] = $ficha['alicuota_id'];
            $dbCatastro['alicuota2021_id'] = $ficha['alicuota2021_id'] *1;
            //$dbCatastro['tipologia'] = $ficha['tipologia'];
            $dbCatastro['tipologia_id'] = $ficha['tipologia_id'] *1;
            $dbCatastro['direccion'] = $patente['direccion'];
            $dbCatastro['uso_id'] = $ficha['uso_id'];
            $dbCatastro['ano'] = $ficha['ano'];
            //$dbCatastro['norte'] = $ficha['norte'];

             $dbCatastro->save();
          } catch (ModelNotFoundException $e) {
            $dbCatastro =  new Catastro;
            $dbCatastro['patente_id'] = $patente['id'];
            $dbCatastro['user_id'] = $usuario['id'];
            $dbCatastro['terreno'] = $ficha['terreno'];
            $dbCatastro['construccion'] = $ficha['construccion'];
            $dbCatastro['zona_id'] = $ficha['zona_id'];
            //$dbCatastro['alicuota_id'] = $ficha['alicuota_id'];
            $dbCatastro['alicuota2021_id'] = $ficha['alicuota2021_id']*1;
            //$dbCatastro['tipologia'] = $ficha['tipologia'];
            $dbCatastro['tipologia_id'] = $ficha['tipologia_id'] *1;
            $dbCatastro['direccion'] = $patente['direccion'];
            $dbCatastro['uso_id'] = $ficha['uso_id'];
            $dbCatastro['ano'] = $ficha['ano'];

            $dbCatastro->save();
            $oriCatastro = $dbCatastro;

          }


          //  $dbCatastro =  Catastro::where('patente_id',$patente['id'])->first();

          //
          //

          //crear comentario
          $margen = 'Editar catastro: 1-'.$oriPatente['catastro'];
          $margen .= ' nombre: '.$oriPatente['nombre'];
          $margen .= ' rif: '.$oriPatente['rif'];
          $margen .= ' direccion: '.$oriPatente['direccion'];
          $margen .= ' *nombre: '.$dbPatente['nombre'];
          $margen .= ' *rif: '.$dbPatente['rif'];
          $margen .= ' *direccion: '.$dbPatente['direccion'];

          $margen .= ' terreno: '.$oriCatastro['terreno'] ;
          $margen .= ' construccion: '.$oriCatastro['construccion'] ;
          $margen .= ' zona: '.$oriCatastro['zona'];
          $margen .= ' uso: '.$oriCatastro['uso'];
          $margen .= ' estado: '.$oriCatastro['estado'] ;
          $margen .= ' ano: '.$oriCatastro['ano'];
          $margen .= ' norte: '.$oriCatastro['norte'] ;
          $margen .= ' sur: '.$oriCatastro['sur'];
          $margen .= ' este: '.$oriCatastro['este'];
          $margen .= ' oeste: '.$oriCatastro['oeste'] ;

          $margen .= ' *terreno: '.$dbCatastro['terreno'] ;
          $margen .= ' *construccion: '.$dbCatastro['construccion'] ;
          $margen .= ' *zona: '.$dbCatastro['zona'];
          $margen .= ' *uso: '.$dbCatastro['uso'];
          $margen .= ' *estado: '.$dbCatastro['estado'] ;
          $margen .= ' *ano: '.$dbCatastro['ano'];
          $margen .= ' *norte: '.$dbCatastro['norte'] ;
          $margen .= ' *sur: '.$dbCatastro['sur'];
          $margen .= ' *este: '.$dbCatastro['este'];
          $margen .= ' *oeste: '.$dbCatastro['oeste'] ;
          $this->addComentario($patente['id'],$comentario,$margen,'patente',$usuario['id']);



          //  obtener telefono
          $temp = Contacto::where('persona_id',$dbPersona['id'])->where('tipo','telefono')->get();
          if(sizeof($temp) > 0 )
              $dbPatente['telefono'] = $temp[0]['contacto'];
          else{
            $newContacto =  new Contacto;
            $newContacto['tipo'] = 'telefono';
            $newContacto['persona_id'] = $dbPersona['id'];
            $newContacto['contacto'] = $patente['telefono'];
            $newContacto->save();
            $dbPatente['telefono'] = $patente['telefono'];
          }

          //  obtener correo
          $temp = Contacto::where('persona_id',$dbPersona['id'])->where('tipo','correo')->get();
          if(sizeof($temp) > 0 )
          $dbPatente['correo'] = $temp[0]['contacto'];

          $result['patente'] = $dbPatente;
          $result['catastro'] = $dbCatastro;
          $result['persona'] = $dbPersona;

          return $result;

        }

      }else {
        return response()->json(['error'=>'ALERTA DE SEGURIDAD Su usuario no esta Autorizado para realizar esta accion! '],401);
      }


    }

//------------------------john nuñez----------------------------------------------
    public function findPatenteVincular(Request $request) {
      //return $request;
      $codigoPatente = $request->input('patente_id');

      $dbpatente = Patenteold::where('patente','like', "%".$codigoPatente."%")->get();
      return $dbpatente;

    }
//---------------------------john nunez-------------------------------------------
public function findComentarioPatente(Request $request){
  $patenteId = $request->input('patente_id');
  $dbComentario= Comentario::where('referencia_id' , $patenteId)->
                            where('tabla' , 'patente')->
                            where('margen','like', "%".'Bloqueado'."%")->first();

        //return $request;
            $request['comentario'] = $dbComentario['comentario'];
        return $request;

}
//---------------------------john nunez-------------------------------------------

    public function create(Request $request){


      $patente = $request->input('patente');
      $ficha = $request->input('ficha');
      $usuario = $request->input('usuario');

      //buscar patente
      $dbPatente = Patenteold::find($patente['id']);
      //-buscar o crear persona ----------------
      $dbPersona = $this->findOrCreatePerson($dbPatente,$usuario);
      $dbPatente['persona_id'] = $dbPersona['id'];
      $dbPatente->save();
      if(($dbPatente['rif']=='')||($dbPatente['rif'] != $patente['rif'])){
        $dbPatente['rif'] = $patente['rif'];
        $dbPatente->save();
        $dbPersona['rif'] = $patente['rif'];
        $dbPersona->save();
      }



      //get unidad tributarios vigente;
      $Utributaria = Utributaria::where('status','activa')->first();


      try {

        $catastro = Catastro::where('patente_id',$patente['id'])->firstOrFail();
        $catastro['terreno'] = $ficha['terreno'];
        $catastro['construccion'] = $ficha['construccion'];
        $catastro['zona'] = $ficha['zona'];
        $catastro['uso'] = $ficha['uso'];
        $catastro['estado'] = $ficha['estado'];
        $catastro['ano'] = $ficha['ano'];
        $catastro['patente_id'] = $dbPatente['id'];
        $catastro['user_id'] = $usuario['id'];
        $catastro->save();
        return $catastro;
      } catch (ModelNotFoundException $e) {

        $catastro = new Catastro;
        $catastro['terreno'] = $ficha['terreno'];
        $catastro['construccion'] = $ficha['construccion'];
        $catastro['zona'] = $ficha['zona'];
        $catastro['uso'] = $ficha['uso'];
        $catastro['estado'] = $ficha['estado'];
        $catastro['ano'] = $ficha['ano'];
        $catastro['patente_id'] = $dbPatente['id'];
        $catastro['user_id'] = $usuario['id'];
        $catastro->save();


        return $catastro;
      }


      //return $result;




      //return $dbPatente;


    }

    public function findCatastroId(Request $request){
      if($request->isJson()){
        try {
          $catastro = Catastro::where('id',$request->input('id'))->firstOrFail();
          return $catastro;
        } catch (ModelNotFoundException $e) {
          return response()->json(['error'=>'no encontrado'],406);
        }
      }
    }

    public function findCatastroIdPatente(Request $request){
      if($request->isJson()){
        try {

          $catastro = Catastro::where('patente_id',$request->input('patente_id'))->firstOrFail();
          return $catastro;
        } catch (ModelNotFoundException $e) {
          return response()->json(['error'=>'no encontrado'],406);
        }
      }
    }


    //----------------------------------------------------------
    public function reImprimirRecibo(Request $request){
      $idr = $request->input('recibo_id');

      $recibo = Recibo::where('id',$idr)->first();
      $lineas = Linea::where('recibo_id',$recibo['id'] )->get();

      $usuario = User::where('id',$recibo['user_id'] )->first();
      $pago = Pago::where('recibo_id',$recibo['id'])->first();
      $patente = Patenteold::where('id', $lineas[0]['patente_id'])->first();
      $persona = Persona::where('id',$recibo['persona_id'])->first();

      $result['recibo'] = $recibo;
      $result['lineas'] = $lineas;
      $result['usuario'] = $usuario;
      $result['pago'] = $pago;
      $result['patente'] = $patente;
      $result['persona'] = $persona;
      return $result;
    }
    //-------------------------------------------------------
    public function pagarRecibo(Request $request){

      if($request->isJson()){
        $pago = $request->input('pago');

        $usuario = $request->input('usuario');


        $dbRecibo = Recibo::where('id',$pago['recibo_id'])->first();
        $dbRecibo['status'] = 'Pagada';
        $dbRecibo->save();

        $totalLineas = Linea::where('recibo_id',$pago['recibo_id'])->sum('total');
        $dbLineas = Linea::where('recibo_id',$pago['recibo_id'])->get();

        $dbPatente = Patenteold::where('id',$dbLineas[0]['patente_id'])->first();

        $dbPersona = Persona::where('id',$dbRecibo['persona_id'])->first();

        //guardar pago

        $dbPago = new Pago;

        $dbPago['banco'] = $pago['banco'];
        $dbPago['referencia'] = $pago['referencia'];
        $dbPago['tipo'] = $pago['tipo'];
        $dbPago['monto'] = $totalLineas;
        $dbPago['recibo_id'] = $pago['recibo_id'];
        $dbPago['user_id'] = $usuario['id'];
        $dbPago->save();

        $result['recibo'] = $dbRecibo;
        $result['lineas'] = $dbLineas;
        $result['usuario'] = $usuario;
        $result['pago'] = $dbPago;
        $result['patente'] = $dbPatente;
        $result['persona'] = $dbPersona;
        return $result;


      }else {
        return response()->json(['error'=>'no Aurorizado'],401);
      }
    }


    public function findLineaRecibo(Request $request){
      if($request->isJson()){

        $linea = Linea::where('recibo_id', $request->input('id'))->get();
        return $linea;

      }else {
        return response()->json(['error'=>'no Aurorizado'],401);
      }
    }
    //

    //Reporte de Catastro
    public function reporteTotales(Request $request){
      if($request->isJson()){
        $totales['totales'] = Patenteold::where('tipo','inmobiliaria')->count();
        $totales['completas'] = Patenteold::join('catastros','patenteolds.id','=','catastros.patente_id')->count();
        $totales['anteriores'] = Patenteold::where('tipo','inmobiliaria')->where('id','<=','14427')->count();
        $totales['nuevas'] = Patenteold::where('tipo','inmobiliaria')->where('id','>','14427')->count();
        $totales['pagadas'] = Patenteold::join('lineas', function($join){
                                              $join->on('patenteolds.id','=','lineas.patente_id')
                                                 ->where('lineas.tipo','catastro')
                                                 ->where('lineas.hano','2018');
                                            })->count();
        return $totales;
      }else {
        return response()->json(['error'=>'no Aurorizado'],401);
      }
    }

    //Reporte de Catastro
    public function listarCatastroTotales(Request $request){
      if($request->isJson()){
        $totales = Patenteold::where('tipo','inmobiliaria')->get();

        return $totales;
      }else {
        return response()->json(['error'=>'no Aurorizado'],401);
      }
    }

    //Reporte de Catastro
    public function listarCatastroActualizados(Request $request){
      if($request->isJson()){
        $totales = Patenteold::join('catastros','patenteolds.id','=','catastros.patente_id')->get();
        return $totales;
      }else {
        return response()->json(['error'=>'no Aurorizado'],401);
      }
    }


    //Reporte de Catastro
    public function listarCatastroAnteriores(Request $request){
      if($request->isJson()){
        $totales = Patenteold::where('tipo','inmobiliaria')->where('id','<=','14427')->get();
        return $totales;
      }else {
        return response()->json(['error'=>'no Aurorizado'],401);
      }
    }


    //Reporte de Catastro
    public function listarCatastroNuevos(Request $request){
      if($request->isJson()){
        $totales = Patenteold::where('tipo','inmobiliaria')->where('id','>','14427')->get();
        return $totales;
      }else {
        return response()->json(['error'=>'no Aurorizado'],401);
      }
    }


    //Reporte de Catastro
    public function listarCatastroPagadosByYear(Request $request){
      if($request->isJson()){
        $ano = $request->input('ano');
        $totales = Patenteold::join('lineas', function($join){
                                              $join->on('patenteolds.id','=','lineas.patente_id')
                                                 ->where('lineas.tipo','catastro')
                                                 ->where('lineas.hano',$ano);
                                            })->get();
        return $totales;
      }else {
        return response()->json(['error'=>'no Aurorizado'],401);
      }
    }

  }
