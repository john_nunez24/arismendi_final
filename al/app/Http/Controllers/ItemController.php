<?php

namespace App\Http\Controllers;

use App\Item;
use App\Linea;
use App\Recibo;
use App\Lictasa;
use Illuminate\Http\Request;


class ItemController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {

  }

  public function guardarLineas(Request $request){

    if($request->isJson()){
      $lineas = $request->input('lineas');
      $patente = $request->input('patente');
      $usuario = $request->input('user');
      //return $lineas;
      //crear recibo
      $dbRecibo = new Recibo;
      $dbRecibo['persona_id'] = $patente['persona']['id'];
      $dbRecibo['user_id'] = $usuario['id'];
      $dbRecibo->save();
//return $dbRecibo;
      foreach ($lineas as $linea) {
          $dbLinea = new Linea;
          $dbLinea['patente_id'] = $linea['patente']['id'];

          $dbLinea['item_id'] = $linea['concepto']['id'];

          $dbLinea['valor_unidad_id'] = $linea['concepto']['valorActual']['id'];
          $dbLinea['base'] = $linea['total'];
          $dbLinea['imes'] = 1;//$fecha['imes'];
          $dbLinea['iano'] = 2019;//$fecha['iano'];
          $dbLinea['hmes'] = 1;//$fecha['hmes'];
          $dbLinea['hano'] = 2019;//$fecha['hano'];
          $dbLinea['descuento'] = 0;//$ldescuento[$acti['id']];
          $dbLinea['interes'] = 0;//$linteres[$acti['id']];
          $dbLinea['multa'] = 0;//$lmulta[$acti['id']];
          $dbLinea['subTotal'] = $linea['total'];
          $dbLinea['total'] = $linea['total'];
          $dbLinea['alicuota'] = 0;//$acti['porcentaje'];
          $dbLinea['user_id'] = $usuario['id'];
          $dbLinea['tipo'] = 'Item';
          $dbLinea['recibo_id'] = $dbRecibo['id'];

          $dbLinea->save();

      }


    }
  }

}
