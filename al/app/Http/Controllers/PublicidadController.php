<?php

namespace App\Http\Controllers;


use App\Publicidad;
use App\Linea;
use App\Recibo;
use App\Utributaria;
use Illuminate\Http\Request;


class PublicidadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }


public function agregarPublicidad(Request $request){

  $patente = $request->input('patente');
  $user = $request->input('user');

   $dbPublicidad = new Publicidad;
   $dbPublicidad['direccion'] = $request->input('direccion');
   $dbPublicidad['patente_id'] = $patente['id'];
   $dbPublicidad['factor'] = $request->input('factor');
   $dbPublicidad['pubtasa_id'] = $request->input('pubtasa_id');
   $dbPublicidad['user_id'] = $user['id'];

   //return $dbPublicidad;
   $dbPublicidad->save();


}


public function getPublicidades(Request $request){
  $patente = $request->input('patente');

  $publicidades = Publicidad::where('patente_id',$patente['id'])->get();

  return $publicidades;
}


public function guardarRecibo(Request $request){


    $publicidades = $request->input('publicidades');
    $ano = $request->input('ano');
    $usuario = $request->input('user');
    $patente = $request->input('patente');
    $sw = true;
    $contLineas = 0;


    //return $lineas;
    //get unidad tributarios vigente;
    $Utributaria = Utributaria::where('status','activa')->first();

    //return $lineas;
    foreach ($publicidades as $linea) {


      //verificar si existe una declaracion igual para el mismo periodo
        $qLinea = Linea::where(function($q)use($patente,$linea,$ano){
                                $q->where('patente_id',$patente['id']);
                                $q->where('actividad_id',$linea['id']);
                                $q->where('tasa_id',$linea['pubtasa_id']);
                                $q->where('iano',$ano);
                                $q->where('tipo','Publicidad');
                                $q->whereIn('status',['Pagada','Pendiente']);
                              })->get();

      if(sizeof($qLinea) == 0 ){
          if($sw){
            //crear recibo
            $dbRecibo = new Recibo;
            $dbRecibo['persona_id'] = $patente['persona']['id'];
            $dbRecibo['user_id'] = $usuario['id'];
            $dbRecibo->save();
            $sw = false;

            }

      $dbLinea = new Linea;
      $dbLinea['patente_id'] = $patente['id'];
      $dbLinea['actividad_id'] = $linea['id'];
      $dbLinea['tasa_id'] = $linea['pubtasa_id'];
      $dbLinea['base'] = $linea['subtotal'];
      $dbLinea['unidad'] = $Utributaria['base'];
      $dbLinea['subTotal'] = $linea['subtotal'];
      $dbLinea['total'] = $linea['subtotal'];
      $dbLinea['user_id'] = $usuario['id'];
      $dbLinea['tipo'] = "Publicidad";
      $dbLinea['recibo_id'] = $dbRecibo['id'];
      $dbLinea['imes']  = "1";
      $dbLinea['hmes']  = "12";
      $dbLinea['iano']  = $ano;
      $dbLinea['hano']  = $ano;
      $dbLinea->save();
      $contLineas++;
     }
    }


   if($contLineas == 0)
       return response()->json(['error'=>'Ya existe una Declaración para el año seleccionado'],406);
    else {
      return response()->json(['success'=>'Se registro la declaración correctamente'],201);
    }

    

}


}
