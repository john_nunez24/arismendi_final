<?php

namespace App\Http\Controllers;
use App\Persona;
use App\User;
use App\Patente;
use App\Direccion;
use App\Patenteold;
use App\Tipo;
use App\Contacto;
use App\Catastro;
use Illuminate\Http\Request;

class PatenteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //------------------------------------agregar comentario------------------------
    public function addComentario($id,$comentario,$margen,$tabla,$user_id){
      $dbComentario = new Comentario;

      $dbComentario['referencia_id'] =  $id;
      $dbComentario['comentario'] =  $comentario;
      $dbComentario['margen'] =  $margen;
      $dbComentario['tabla'] =  $tabla;
      $dbComentario['user_id'] =  $user_id;
      $dbComentario->save();
    }


   public function findPatente(Request $request){

     $patentes =  Patenteold::where(function($q)use($request){
             $param = $request->all();
             if($param['tipo'] != ""){
               if($param['tipo']==="todo")
                  $q->where('id','>','0');
                 else
                  $q->where('tipo', $param['tipo']);
             }
            if($param['nombre'] != "")
               $q->where('nombre','like', "%".$param['nombre']."%");
            if($param['patente'] != "")
               $q->where('patente','like', "%".$param['patente']."%");
            if($param['direccion'] != "")
               $q->where('direccion','like', "%".$param['direccion']."%");
            if (isset($param['rif'])&&($param['rif'] != ""))
                $q->where('rif','like', "%".$param['rif']."%");

           })->paginate(20);

        foreach ($patentes as $patente) {
            //si tiene persona asociada
          if($patente['persona_id'] != null){
            $patente['persona'] = Persona::where('id',$patente['persona_id'])->first();
          }
            // info adicional para catastro
          if($patente['tipo'] == 'inmobiliaria'){
            //  obtener telefono
            $temp = Contacto::where('persona_id',$patente['persona_id'])->where('tipo','telefono')->get();
            if(sizeof($temp) > 0 )
               $patente['telefono'] = $temp[0]['contacto'];
               //  obtener correo
               $temp = Contacto::where('persona_id',$patente['persona_id'])->where('tipo','correo')->get();
            if(sizeof($temp) > 0 )
                  $patente['correo'] = $temp[0]['contacto'];
          }


        }
        return $patentes;
   }

//-----------------john nuñez--------------------------------------------------
public function getPatentes(Request $request){
  $patente = $request->input('patente');
      $dbPatentes = Patenteold::where('patente','like', "%".$patente."%")->get();
          foreach ($dbPatentes as  $dbPatente) {
              $dbPatente['ficha']= Catastro::where('patente_id' , $dbPatente['id'])->get();
          }
      return $dbPatentes;
}
//-----------------john nuñez--------------------------------------------------





    public function index($token){
      try {

          $user = User::where('api_token',$token)->first();
          $persona = $user->persona()->firstOrFail();
          $patentes = Patente::where('persona_id',$persona['id'])->get();
          foreach ($patentes as $patente) {
               $patente['tDetalle'] = Tipo::find($patente['tipo_id']);
               $patente['direccion'] = Direccion::find($patente['direccion_id']);
           }
          return response()->json($patentes,200);

      } catch (ModelNotFoundException $e) {
        return response()->json(['error'=>'no encontrado'],404);
      }
    }

    //
}
