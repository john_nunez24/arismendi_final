<?php

namespace App\Http\Controllers;


use App\Licor;
use App\Linea;
use App\Recibo;
use App\Utributaria;
use Illuminate\Http\Request;


class LicorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }


public function agregarLicor(Request $request){
$patente = $request->input('patente');
  $user = $request->input('user');

   $dbLicor = new Licor;

   $dbLicor['patente_id'] = $patente['id'];
   $dbLicor['factor'] = $request->input('factor');
   $dbLicor['lictasa_id'] = $request->input('lictasa_id');
   $dbLicor['user_id'] = $user['id'];

   //return $dbLicor;
   $dbLicor->save();

}


public function getLicores(Request $request){
  $patente = $request->input('patente');

  $licores = Licor::where('patente_id',$patente['id'])->get();

  return $licores;
}


public function guardarRecibo(Request $request){



    $licores = $request->input('licores');
    $ano = $request->input('ano');
    $usuario = $request->input('user');
    $patente = $request->input('patente');
    $sw = true;
    $contLineas = 0;

//return response()->json(['error'=>$licores],406);
    //return $lineas;
    //get unidad tributarios vigente;
    $Utributaria = Utributaria::where('status','activa')->first();

    //return $lineas;
    foreach ($licores as $linea) {


      //verificar si existe una declaracion igual para el mismo periodo
        $qLinea = Linea::where(function($q)use($patente,$linea,$ano){
                                $q->where('patente_id',$patente['id']);
                                $q->where('actividad_id',$linea['id']);
                                $q->where('tasa_id',$linea['lictasa_id']);
                                $q->where('iano',$ano);
                                $q->where('tipo','Licores');
                                $q->whereIn('status',['Pagada','Pendiente']);
                              })->get();

      if(sizeof($qLinea) == 0 ){
          if($sw){
            //crear recibo
            $dbRecibo = new Recibo;
            $dbRecibo['persona_id'] = $patente['persona']['id'];
            $dbRecibo['user_id'] = $usuario['id'];
            $dbRecibo->save();
            $sw = false;

            }

      $dbLinea = new Linea;
      $dbLinea['patente_id'] = $patente['id'];
      $dbLinea['actividad_id'] = $linea['id'];
      $dbLinea['tasa_id'] = $linea['lictasa_id'];
      $dbLinea['base'] = $linea['subtotal'];
      $dbLinea['unidad'] = $Utributaria['base'];
      $dbLinea['subTotal'] = $linea['subtotal'];
      $dbLinea['total'] = $linea['subtotal'];
      $dbLinea['user_id'] = $usuario['id'];
      $dbLinea['tipo'] = "Licores";
      $dbLinea['recibo_id'] = $dbRecibo['id'];
      $dbLinea['imes']  = "1";
      $dbLinea['hmes']  = "12";
      $dbLinea['iano']  = $ano;
      $dbLinea['hano']  = $ano;
      $dbLinea->save();
      $contLineas++;
     }
    }


   if($contLineas == 0)
       return response()->json(['error'=>'Ya existe una Declaración para el año seleccionado'],406);
    else {
      return response()->json(['success'=>'Se registro la declaración correctamente'],201);
    }



}


}
