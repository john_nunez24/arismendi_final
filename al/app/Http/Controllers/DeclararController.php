<?php

namespace App\Http\Controllers;


use App\Patenteold;
use App\Pago;
use App\Linea;
use App\Persona;
use App\Recibo;
use App\Utributaria;
use App\User;
use App\Comentario;
use App\Salario;
use App\Contacto;
use App\Actividadpatente;
use App\Pagorecibo;
////use App\Cuenta;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Contracts\Support\Jsonable;
class DeclararController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    //$this->middleware('auth');
  }


//------------------------------------agregar comentario------------------------
public function addComentario($id,$comentario,$margen,$tabla,$user_id){
  $dbComentario = new Comentario;

  $dbComentario['referencia_id'] =  $id;
  $dbComentario['comentario'] =  $comentario;
  $dbComentario['margen'] =  $margen;
  $dbComentario['tabla'] =  $tabla;
  $dbComentario['user_id'] =  $user_id;
  $dbComentario->save();
}
//------------------------------------------------------------------------------
  public function findOrCreatePerson($patente,$user){
    try {

      $persona = Persona::where('rif',$patente['rif'])->firstOrFail();

      return $persona;
    } catch (ModelNotFoundException $e) {

      $persona = new Persona;
      $persona['nombre'] = $patente['nombre'];
      $persona['rif'] = $patente['rif'];
      $persona['direccion'] = $patente['direccion'];
      $persona['user_id'] = $user['id'];
      $persona->save();
      $patente['persona_id'] = $persona['id'];
      $patente->save();
      return $persona;
    }


  }
  //---------------------john nuñez-------------------------------------------------

  public function newPatente(Request $request){


    $patente = $request->input('patente');
//
    $usuario = $request->input('usuario');
    $comentario = $request->input('comentario');
    $actividades = $request->input('actividades');
    //return $actividades;
   try {

      //buscar patente

      $dbPatente = Patenteold::where('patente','AE-2-'.$patente['patente'])->firstOrFail();
      return response()->json(['error'=>'El numero de patente '.$dbPatente['patente'].', ya existe y esta registrado a nombre de '.$dbPatente['nombre']],406);
    } catch (ModelNotFoundException $e) {
      //-buscar o crear persona ----------------

      $patente['rif'] = $patente['prefijo'].'-'.$patente['rif'];
      $dbPersona = $this->findOrCreatePersonOnly($patente,$usuario);

      $dbNewPatente = new Patenteold;
      $dbNewPatente['persona_id'] = $dbPersona['id'];
      $dbNewPatente['patente'] = 'AE-2-'.$patente['patente'];
      $dbNewPatente['nombre'] = $patente['nombre'];
      $dbNewPatente['rif'] = $patente['rif'];
      $dbNewPatente['direccion'] = $patente['direccion'];
      $dbNewPatente['tipo'] = 'comercio';
      $dbNewPatente['departamento_id'] = '1';
      $dbNewPatente->save();

      //crear contactos
      $dbTelefono = $this->createContact($patente['telefono'],'telefono',$dbPersona);
      //$dbCorreo = $this->createContact($patente['correo'],'correo',$dbPersona);

      //crear comentario
      $margen = 'Registro nuevo Actividad Económica: AE-2-'.$patente['patente'];
      $this->addComentario($dbNewPatente['id'],$comentario,$margen,'patente',$usuario['id']);

      //codigo de john ------------------------------

             //$usuario = $request->input('usuario');
             foreach ($actividades as $act) {
               // code...

             $actividadPatente = new Actividadpatente;
             $actividadPatente['patente_id'] = $dbNewPatente['id'];
             $actividadPatente['actividad_id'] = $act['id'];

             $actividadPatente->save();
              }
  //codigo de john----------------------------------

    }
  }

//---------


  //-------------------John Nuñez--------------------------------------------------
  public function createContact($dato,$tipo, $persona){

    if(($dato != "")){
      $contacto = new Contacto;
      $contacto['contacto'] = $dato;
      $contacto['tipo'] = $tipo;
      $contacto['persona_id'] = $persona['id'];
      $contacto->save();
      return $contacto;
    }


    }

  //------
  //codigo john----------------------------------------
     public function eliminarRecibo(Request $request){
     $recibo = $request->input('recibo');
     $comentario = $request->input('comentario');
     $usuario = $request->input('usuario');

            $dbRecibo = Recibo::find($recibo['id']);

            $dbRecibo['status'] = 'Anulada';
            $dbRecibo['comentario'] = $comentario;
            $dbRecibo->save();

            $lineas = Linea::where('recibo_id',$recibo['id'] )->get();
                foreach ($lineas as $linea) {
                  $linea['status'] = 'Anulada';
                  $linea->save();
                }

            if ($recibo['status'] == 'Pagada'){
                $dbPagos = Pago::where('recibo_id',$recibo['id'])->where('status','Pagada')->first();
                $dbPagos['status'] = 'Anulada';
                $dbPagos->save();
                return 'hola 1';
                $margen = 'Se Anula Pago de Linia #:'+ $dbPagos['id'];
                $this->addComentario($dbPagos['id'],$comentario,$margen,'pago',$usuario['id']);

             }
              $margen = 'Se Anula Recibo #:'+ $recibo['id'];
            $this->addComentario($recibo['id'],$comentario,$margen,'recibo',$usuario['id']);

     }

  //codigo john----------------------------------------
  public function findOrCreatePersonOnly($patente,$user){
    try {

      $persona = Persona::where('rif',$patente['rif'])->firstOrFail();

      return $persona;
    } catch (ModelNotFoundException $e) {

      $persona = new Persona;
      $persona['nombre'] = $patente['nombre'];
      $persona['rif'] = $patente['rif'];
      $persona['direccion'] = $patente['direccion'];
      $persona['user_id'] = $user['id'];
      $persona->save();

      return $persona;
    }


  }

  //---------------------JOHN nuñez-------------------------------------------------

  //----------------------------------------------------------------------

  public function createTasa(Request $request){

    $persona = $request->input('persona');
    $lineas = $request->input('lineas');
    $usuario = $request->input('usuario');
    //return $lineas;
    //get unidad tributarios vigente;
    $Utributaria = Utributaria::where('status','activa')->first();
    //crear recibo
    $dbRecibo = new Recibo;
    $dbRecibo['persona_id'] = $persona['id'];
    $dbRecibo['user_id'] = $usuario['id'];
    $dbRecibo->save();
    //return $lineas;
    foreach ($lineas as $linea) {

      $dbLinea = new Linea;

      $dbLinea['tasa_id'] = $linea['id'];
      $dbLinea['base'] = $linea['subtotal'];
      $dbLinea['unidad'] = $Utributaria['base'];
      $dbLinea['subTotal'] = $linea['subtotal'];
      $dbLinea['total'] = $linea['subtotal'];
      $dbLinea['user_id'] = $usuario['id'];
      $dbLinea['tipo'] = $linea['tipo'];
      $dbLinea['recibo_id'] = $dbRecibo['id'];
      $dbLinea->save();

    }


    $result['recibo'] = $dbRecibo;


    return $result;




    //return $dbPatente;


  }


  //----------------------------------------------------------------------

  public function createTasaPublicidad(Request $request){

    $patente = $request->input('patente');
    $lineas = $request->input('lineas');
    $usuario = $request->input('usuario');
    //return $lineas;
    //get unidad tributarios vigente;
    $Utributaria = Utributaria::where('status','activa')->first();
    //crear recibo
    $dbRecibo = new Recibo;
    $dbRecibo['persona_id'] = $patente['persona']['id'];
    $dbRecibo['user_id'] = $usuario['id'];
    $dbRecibo->save();
    //return $lineas;
    foreach ($lineas as $linea) {

      $dbLinea = new Linea;

      $dbLinea['tasa_id'] = $linea['id'];
      $dbLinea['base'] = $linea['subtotal'];
      $dbLinea['unidad'] = $Utributaria['base'];
      $dbLinea['subTotal'] = $linea['subtotal'];
      $dbLinea['patente_id'] = $patente['id'];
      $dbLinea['iano'] = $linea['ano'];
      $dbLinea['hano'] = $linea['ano'];
      $dbLinea['imes'] = '1';
      $dbLinea['hmes'] = '12';
      $dbLinea['total'] = $linea['subtotal'];
      $dbLinea['user_id'] = $usuario['id'];
      $dbLinea['tipo'] = $linea['tipo'];
      $dbLinea['recibo_id'] = $dbRecibo['id'];
      $dbLinea->save();

    }


    $result['recibo'] = $dbRecibo;


    return $result;




    //return $dbPatente;


  }
  //----------------------------------------------------------------------

  //----------------------------------------------------------------------
  public function create(Request $request){


    $actividades = $request->input('actividad');
    $patente = $request->input('patente');
    $usuario = $request->input('usuario');
    $ltotal = $request->input('total');
    $lsub = $request->input('sub');
    $ldescuento = $request->input('descuento');
    $linteres = $request->input('interes');
    $lmulta = $request->input('multa');
    $lbase = $request->input('base');
    $fecha = $request->input('fecha');

    //buscar patente
    $dbPatente = Patenteold::find($patente['id']);

    //get unidad tributarios vigente;
    $Utributaria = Utributaria::where('status','activa')->first();




    foreach ($actividades as $acti) {
      if($lbase[$acti['id']] != null ){
        $lineaExiste =  Linea::where('iano',$fecha['iano'])
        ->where('imes',$fecha['imes'])
        ->where('actividad_id',$acti['id'])
        ->where('patente_id',$patente['id'])
        ->whereIn('status',['Pagada','Pendiente'])->get();
        if(count($lineaExiste)>0 )
        {
          return response()->json(['error'=>"Atencion, ya se encuentra un registro para la actividad economica en el periodo seleccionado"],406);
        }
      }}




      //crear recibo
      $dbRecibo = new Recibo;
      $dbRecibo['persona_id'] = $patente['persona']['id'];
      $dbRecibo['user_id'] = $usuario['id'];
      $dbRecibo->save();

      foreach ($actividades as $acti) {
        if($lbase[$acti['id']] != null ){
          $dbLinea = new Linea;
          $dbLinea['patente_id'] = $patente['id'];
          $dbLinea['actividad_id'] = $acti['id'];
          $dbLinea['base'] = $lbase[$acti['id']];
          $dbLinea['unidad'] = $Utributaria['base'];
          $dbLinea['imes'] = $fecha['imes'];
          $dbLinea['iano'] = $fecha['iano'];
          $dbLinea['hmes'] = $fecha['hmes'];
          $dbLinea['hano'] = $fecha['hano'];
          $dbLinea['descuento'] = $ldescuento[$acti['id']];
          $dbLinea['interes'] = $linteres[$acti['id']];
          $dbLinea['multa'] = $lmulta[$acti['id']];
          $dbLinea['subTotal'] = $lsub[$acti['id']];
          $dbLinea['total'] = $ltotal[$acti['id']];
          $dbLinea['alicuota'] = $acti['porcentaje'];
          $dbLinea['user_id'] = $usuario['id'];
          $dbLinea['tipo'] = 'Declaracion';
          $dbLinea['recibo_id'] = $dbRecibo['id'];

          $dbLinea->save();
        }
      }

      $result['patente'] = $dbPatente;

      return $result;

    }

    //----------------------------------------------------------------------
    public function updateDeclaracion(Request $request){


      $actividades = $request->input('actividad');
      $patente = $request->input('patente');
      $usuario = $request->input('usuario');
      $ltotal = $request->input('total');
      $lsub = $request->input('sub');
      $ldescuento = $request->input('descuento');
      $linteres = $request->input('interes');
      $lmulta = $request->input('multa');
      $lbase = $request->input('base');
      $fecha = $request->input('fecha');
      $recibo = $request->input('recibo');
      $lineas = $recibo['lineas'];
      $comentario =   $request->input('comentario');


      //get unidad tributarios vigente;
      $Utributaria = Utributaria::where('status','activa')->first();

     $margen = "Actualizar: recibo (".$fecha['recibo_id'].") ";

      foreach ($lineas as $linea){
        if($lbase[$linea['id']] != null ){
          $lineaExiste =  Linea::where('iano',$fecha['iano'])
          ->where('imes',$fecha['imes'])
          ->where('actividad_id',$linea['actividad_id'])
          ->where('patente_id',$patente['id'])
          ->where('id','<>',$fecha['id'])
          ->where('recibo_id','<>',$fecha['recibo_id'])
          ->whereIn('status',['Pagada','Pendiente'])->get();
          $margen .= $linea['actividad_id'] ." - ";
          if(count($lineaExiste)>0 )
          {
            return response()->json(['error'=>"Atencion, ya se encuentra un registro para la actividad economica en el periodo seleccionado"],406);
          }
        }
      }






        foreach ($lineas as $linea) {


          if($lbase[$linea['id']] != null ){

            $dbLinea = Linea::where('id',$linea['id'])->first();
            $margen .= " Valor Anterior: ".$dbLinea['base']." - ";
            $margen .= " Nuevo Valor: ".$lbase[$linea['id']]." - ";
            $dbLinea['base'] = $lbase[$linea['id']];
            $dbLinea['unidad'] = $Utributaria['base'];
            $dbLinea['imes'] = $fecha['imes'];
            $dbLinea['iano'] = $fecha['iano'];
            $dbLinea['hmes'] = $fecha['hmes'];
            $dbLinea['hano'] = $fecha['hano'];
            $dbLinea['descuento'] = $ldescuento[$linea['id']];
            $dbLinea['interes'] = $linteres[$linea['id']];
            $dbLinea['multa'] = $lmulta[$linea['id']];
            $dbLinea['subTotal'] = $lsub[$linea['id']];
            $dbLinea['total'] = $ltotal[$linea['id']];
            $dbLinea['user_id'] = $usuario['id'];
            $dbLinea->save();
          }
        }


        $this->addComentario($fecha['recibo_id'],$comentario,$margen,'recibo',$usuario['id']);
        return  response()->json(['ok'=>"Se actualizo el recibo correctamente"],200);


      }



//------------------------------------------------------------------------------

public function findRecibosPatente(Request $request){

   if($request->isJson()){
     $patente = $request->input('patente');
     //$tipo = $request->input('tipo');
     return $patente;
     $lineas = Recibo::load('lineas')->get();

     return $lineas;
   }



}



//------------------------------------------------------------------------------



    public function findRecibos(Request $request){

      if($request->isJson()){
        $declaraciones = Recibo::where(function($q) use($request){
          if($request->input('status')!='')
           $q->where('status',$request->input('status'));
          if($request->input('id')!='')
           $q->where('persona_id', $request->input('id'));

          if($request->input('fecha')!=''){
            $fecha = explode("T",$request->input('fecha'));
            $cale = explode("-",$fecha[0]);
             $q->whereYear('updated_at',$cale[0]);
            if($request->input('tipoReporte')!=''){
              if($request->input('tipoReporte')=='mes')
                 $q->whereMonth('updated_at',$cale[1]);
              if($request->input('tipoReporte')=='dia'){
                 $q->whereMonth('updated_at',$cale[1]);
                 $q->whereDay('updated_at',$cale[2]);
               }

            }
          }
        })->orderBy('id','desc')->paginate($request->input('paginate'));
        $totalizado = 0;
        $totalizadoInteres = 0;
        $totalizadoMulta = 0;
        $totalizadoDescuento = 0;
        foreach ($declaraciones as $key => $dec) {


          $dec['lineas']= Linea::where(function($q)use($request,$dec){
                                           $q->where('recibo_id',$dec['id']);
                                         if($request->input('tipo')!='')
                                            $q->where('tipo', $request->input('tipo'));
                                         })->get();
          $dec['totalPagar'] = Linea::where(function($q)use($request,$dec){
                                           $q->where('recibo_id',$dec['id']);
                                         if($request->input('tipo')!='')
                                            $q->where('tipo', $request->input('tipo'));
                                         })->sum('total');
          $dec['totalInteres'] = Linea::where(function($q)use($request,$dec){
                                           $q->where('recibo_id',$dec['id']);
                                         if($request->input('tipo')!='')
                                            $q->where('tipo', $request->input('tipo'));
                                         })->sum('interes');
          $dec['totalMulta'] = Linea::where(function($q)use($request,$dec){
                                           $q->where('recibo_id',$dec['id']);
                                         if($request->input('tipo')!='')
                                            $q->where('tipo', $request->input('tipo'));
                                         })->sum('multa');
          $dec['totalDescuento'] = Linea::where(function($q)use($request,$dec){
                                           $q->where('recibo_id',$dec['id']);
                                         if($request->input('tipo')!='')
                                            $q->where('tipo', $request->input('tipo'));
                                         })->sum('descuento');
          $dec['totalBase'] = Linea::where(function($q)use($request,$dec){
                                           $q->where('recibo_id',$dec['id']);
                                         if($request->input('tipo')!='')
                                            $q->where('tipo', $request->input('tipo'));
                                         })->sum('base');
          $dec['tipo']= Linea::where('recibo_id',$dec['id'])->first()['tipo'];
          $dec['pago']= Pagorecibo::where('recibo_id',$dec['id'])->first();
          $dec['persona']= Persona::where('id',$dec['persona_id'])->first();
          $dec['usuario']= User::where('id',$dec['user_id'])->first();
          $dbLinea = Linea::where(function($q)use($request,$dec){
                                           $q->where('recibo_id',$dec['id']);
                                         if($request->input('tipo')!='')
                                            $q->where('tipo', $request->input('tipo'));
                                         })->first();
          if($dbLinea)
          {
            $dbPatente_id = $dbLinea['patente_id'];

          $dec['patente']= Patenteold::where('id',$dbPatente_id)->first();
          $dec['patente_linea'] = $dbLinea;
          $totalizado = $totalizado + $dec['totalPagar'];
          $totalizadoMulta = $totalizadoMulta + $dec['totalMulta'];
          $totalizadoInteres = $totalizadoInteres + $dec['totalInteres'];
          $totalizadoDescuento = $totalizadoDescuento + $dec['totalDescuento'];
        }else{
          $dec['patente']= [];
          $dec['patente_linea'] = [];
        }
        }
        return $declaraciones;


      }
    }

    //----------------------------------------------------------------------

    public function createCatastro(Request $request){

      $jsPatente = $request->input('patente');
      $lineas = $request->input('lineas');
      $usuario = $request->input('usuario');

      $patente = Patenteold::where('id',$jsPatente['id'])->first();

      //----------Validar pagos anteriores

      foreach ($lineas as $linea) {

        if($linea != null ){
          $lineaExiste =  Linea::where('iano',$linea['ano'])
                            ->where('patente_id',$patente['id'])
                            ->whereIn('status',['Pagada','Pendiente'])->get();
          if(count($lineaExiste)>0 )
          {
            return response()->json(['error'=>"Atencion, ya se encuentra un registro para la fecha seleccionada"],406);
          }
        }}

        //-------------------------------




        //get unidad tributarios vigente;
        $Utributaria = Utributaria::where('status','activa')->first();
        //crear recibo
        $dbRecibo = new Recibo;
        $dbRecibo['persona_id'] = $patente['persona_id'];
        $dbRecibo['user_id'] = $usuario['id'];
        $dbRecibo->save();
        //return $lineas;
        foreach ($lineas as $linea) {

          $dbLinea = new Linea;

          $dbLinea['patente_id'] = $patente['id'];
          $dbLinea['base'] = $linea['valorCatastral'];
          $dbLinea['unidad'] = $linea['uTributaria']['base'];
          $dbLinea['subTotal'] = $linea['totalDepre'];
          $dbLinea['total'] = $linea['totalPagar'];
          $dbLinea['descuento'] = $linea['totalDescuento'];
          $dbLinea['interes'] = $linea['intereses'];
          $dbLinea['user_id'] = $usuario['id'];
          $dbLinea['imes'] = '1';
          $dbLinea['iano'] = $linea['ano'];
          $dbLinea['hmes'] = '12';
          $dbLinea['hano'] = $linea['ano'];
          $dbLinea['tipo'] = 'Catastro';
          $dbLinea['recibo_id'] = $dbRecibo['id'];

          $dbLinea->save();

        }


        $result['recibo'] = $dbRecibo;


        return $result;




        //return $dbPatente;


      }


      //----------------------------------------------------------------------

      public function createOrderCatastro(Request $request){

        $jsPatente = $request->input('patente');
        $autos = $request->input('auto');
        $manual = $request->input('manual');
        $tasas = $request->input('tasas');
        $usuario = $request->input('usuario');
        //return $tasas;
        $patente = Patenteold::where('id',$jsPatente['id'])->first();

        //----------Validar pagos anteriores
/*
        foreach ($lineas as $linea) {

          if($linea != null ){
            $lineaExiste =  Linea::where('iano',$linea['ano'])
                              ->where('patente_id',$patente['id'])
                              ->whereIn('status',['Pagada','Pendiente'])->get();
            if(count($lineaExiste)>0 )
            {
              return response()->json(['error'=>"Atencion, ya se encuentra un registro para la fecha seleccionada"],406);
            }
          }}*/

          //-------------------------------




          //get unidad tributarios vigente;
          $Utributaria = Utributaria::where('status','activa')->first();
          //get salario minimo actual;
          $salario = Salario::where('status','activo')->first();
          //crear recibo
          $dbRecibo = new Recibo;
          $dbRecibo['persona_id'] = $patente['persona_id'];
          $dbRecibo['user_id'] = $usuario['id'];
          $dbRecibo->save();
          //return $lineas;
          foreach ($tasas as $tasa) {

            $dbLinea = new Linea;
            $dbLinea['patente_id'] = $patente['id'];
            $dbLinea['item_id'] = $tasa['concepto']['id'];
            $dbLinea['base'] = $tasa['total'];
            $dbLinea['unidad'] = 0;
            $dbLinea['subTotal'] = $tasa['total'];
            $dbLinea['total'] = $tasa['total'];
            $dbLinea['user_id'] = $usuario['id'];
            $dbLinea['tipo'] = 'Tasa';
            $dbLinea['recibo_id'] = $dbRecibo['id'];
            $dbLinea['imes'] = 1;//$fecha['imes'];
            $dbLinea['iano'] = 2020;//$fecha['iano'];
            $dbLinea['hmes'] = 1;//$fecha['hmes'];
            $dbLinea['hano'] = 2020;//$fecha['hano'];
            $dbLinea['descuento'] = 0;//$ldescuento[$acti['id']];
            $dbLinea['interes'] = 0;//$linteres[$acti['id']];
            $dbLinea['multa'] = 0;//$lmulta[$acti['id']];
            $dbLinea['status'] = 'Pendiente';
            $dbLinea->save();

          }
          foreach ($autos as $auto) {
            // code...

              if($auto['monto'] > 0){
                        $dbLinea = new Linea;

                        $dbLinea['patente_id'] = $patente['id'];
                        $dbLinea['base'] = $auto['monto'];
                        $dbLinea['unidad'] = '0';
                        $dbLinea['subTotal'] = 0;//$linea['totalDepre'];
                        $dbLinea['total'] = $auto['monto'];
                        $dbLinea['descuento'] = 0;//$linea['totalDescuento'];
                        $dbLinea['interes'] = 0;//$linea['intereses'];
                        $dbLinea['user_id'] = $usuario['id'];
                        $dbLinea['imes'] = '1';
                        $dbLinea['iano'] = $auto['anno'];
                        $dbLinea['hmes'] = '12';
                        $dbLinea['hano'] = $auto['anno'];
                        $dbLinea['tipo'] = 'Catastro';
                        $dbLinea['recibo_id'] = $dbRecibo['id'];
                        $dbLinea->save();

                            }
            }
if($manual['monto'] > 0){
          $dbLinea = new Linea;

          $dbLinea['patente_id'] = $patente['id'];
          $dbLinea['base'] = $manual['monto'];
          $dbLinea['unidad'] = '1800';
          $dbLinea['subTotal'] = 0;//$linea['totalDepre'];
          $dbLinea['total'] = $manual['monto'];
          $dbLinea['concepto'] = $manual['concepto'];
          $dbLinea['descuento'] = 0;//$linea['totalDescuento'];
          $dbLinea['interes'] = 0;//$linea['intereses'];
          $dbLinea['user_id'] = $usuario['id'];
          $dbLinea['imes'] = '1';
          $dbLinea['iano'] = $manual['desde'];
          $dbLinea['hmes'] = '12';
          $dbLinea['hano'] = $manual['hasta'];
          $dbLinea['tipo'] = 'CatastroManual';
          $dbLinea['recibo_id'] = $dbRecibo['id'];
          $dbLinea->save();
}
          $result['recibo'] = $dbRecibo;


          return $result;




          //return $dbPatente;


        }
        //----

public function guardarReciboIngenieria(Request $request){

          $jsPatente = $request->input('patente');
          $proc = $request->input('procedimiento');
          $tasas = $request->input('tasas');
          $usuario = $request->input('usuario');
          //return $tasas;
          $patente = Patenteold::where('id',$jsPatente['id'])->first();

            //get unidad tributarios vigente;
            $Utributaria = Utributaria::where('status','activa')->first();
            //get salario minimo actual;
            $salario = Salario::where('status','activo')->first();
            //crear recibo
            $dbRecibo = new Recibo;
            $dbRecibo['persona_id'] = $patente['persona_id'];
            $dbRecibo['user_id'] = $usuario['id'];
            $dbRecibo->save();
            //return $lineas;
            foreach ($tasas as $tasa) {

              $dbLinea = new Linea;
              $dbLinea['patente_id'] = $patente['id'];
              $dbLinea['item_id'] = $tasa['concepto']['id'];
              $dbLinea['base'] = $tasa['total'];
              $dbLinea['unidad'] = 0;
              $dbLinea['subTotal'] = $tasa['total'];
              $dbLinea['total'] = $tasa['total'];
              $dbLinea['user_id'] = $usuario['id'];
              $dbLinea['tipo'] = 'Item';
              $dbLinea['recibo_id'] = $dbRecibo['id'];
              $dbLinea['imes'] = 1;//$fecha['imes'];
              $dbLinea['iano'] = 2020;//$fecha['iano'];
              $dbLinea['hmes'] = 1;//$fecha['hmes'];
              $dbLinea['hano'] = 2020;//$fecha['hano'];
              $dbLinea['descuento'] = 0;//$ldescuento[$acti['id']];
              $dbLinea['interes'] = 0;//$linteres[$acti['id']];
              $dbLinea['multa'] = 0;//$lmulta[$acti['id']];
              $dbLinea['status'] = 'Pendiente';
              $dbLinea->save();

            }
                if(($proc != null)||($proc != undefined)){
                          $dbLinea = new Linea;

                          $dbLinea['patente_id'] = $patente['id'];
                          $dbLinea['base'] = $proc['resuladoProcedimiento'];
                          $dbLinea['unidad'] = '0';
                          $dbLinea['subTotal'] = 0;//$linea['totalDepre'];
                          $dbLinea['total'] = $proc['resuladoProcedimiento'];
                          $dbLinea['descuento'] = 0;//$linea['totalDescuento'];
                          $dbLinea['interes'] = 0;//$linea['intereses'];
                          $dbLinea['user_id'] = $usuario['id'];
                          $dbLinea['imes'] = '1';
                          $dbLinea['iano'] = '2020';
                          $dbLinea['hmes'] = '12';
                          $dbLinea['hano'] = '2020';
                          $dbLinea['tipo'] = 'Ingieneria';
                          $dbLinea['indice_id'] = $proc['procedimiento']['id'];
                          $dbLinea['concepto'] = $proc['procedimiento']['nombre'] . 'Metros Cuadrados:' . $proc['metroCuadrado'];
                          $dbLinea['recibo_id'] = $dbRecibo['id'];
                          $dbLinea->save();


            }
            $result['recibo'] = $dbRecibo;
            return $result;
            //return $dbPatente;
          }
          //----
      //--------88--------------------------------------------------
      public function reImprimirRecibo(Request $request){
        $idr = $request->input('recibo_id');

        $recibo = Recibo::where('id',$idr)->first();
        $lineas = Linea::where('recibo_id',$recibo['id'] )->get();

        $usuario = User::where('id',$recibo['user_id'] )->first();
        $pago = Pago::where('recibo_id',$recibo['id'])->first();
        $patente = Patenteold::where('id', $lineas[0]['patente_id'])->first();
        $persona = Persona::where('id',$recibo['persona_id'])->first();
        $result['allPatentes'] = [];
        $result['recibo'] = $recibo;
        $result['lineas'] = $lineas;
        $result['usuario'] = $usuario;
        $result['pago'] = $pago;
        $result['patente'] = $patente;
        $result['persona'] = $persona;
        return $result;
      }
      //-------------------------------------------------------
      public function pagarRecibo(Request $request){

        if($request->isJson()){
          $pago = $request->input('pago');
          $total = $request->input('total');
          $recibos = $request->input('recibos');
          $usuario = $request->input('usuario');

          //return $recibos;
          $dbPago = new Pago;
          $dbPago['banco'] = $pago['banco'];
          $dbPago['referencia'] = $pago['referencia'];
          $dbPago['tipo'] = $pago['tipo'];
          $dbPago['monto'] = $total;
          //$dbPago['recibo_id'] = $pago['recibo_id'];
          $dbPago['user_id'] = $usuario['id'];
          $dbPago->save();
          $patentes_id = [];
          $recibos_id = [];
          $personas_id = [];

          foreach ($recibos as $recibo) {
             array_push($recibos_id , $recibo['id']);

            // code...

            $lineas = $recibo['lineas'];
            $dbRecibo = recibo::where('id', $recibo['id'])->first();
            $dbRecibo['status'] = 'Pagada';
            $dbRecibo->save();
            $i = 0;
            $dbLineas = Linea::where('recibo_id',$recibo['id'])->get();




              foreach ($dbLineas as $linea) {
                    $linea['status'] = 'Pagada';
                    $linea->save();
                    //------------------
                      if ($linea['patente_id'] != null) {
                          $sw = true;
                          //return $sw;

                          if ($patentes_id != null)
                            foreach ($patentes_id as $patente_id)
                              if($patente_id == $linea['patente_id'])
                                $sw = false;
                          if ($sw)
                            array_push($patentes_id , $linea['patente_id']);
                      }

                      if ($linea['patente_id'] != null){
                          $sw1 = true;
                          //return $sw1;
                          if ($personas_id != null)
                             foreach ($personas_id as $persona_id)
                              if($persona_id == $recibo['persona_id'])
                                $sw1 = false;
                          if ($sw1)
                            array_push($personas_id , $recibo['persona_id']);
                        }
                    //------------------


                }//fin foreach linea
                  //return $patentes_id;
            $dbPagorecibo = new Pagorecibo;
            $dbPagorecibo['pago_id'] = $dbPago['id'];
            $dbPagorecibo['recibo_id'] = $recibo['id'];
            $dbPagorecibo['monto_total'] = $recibo['totalPagar'];
            $dbPagorecibo->save();


          }//fin foreach recibo
              if ($patentes_id != null){
                $dbPatentes = Patenteold::whereIn('id' , $patentes_id)->get();

                foreach ($dbPatentes as $patente) {
                  // code...
                      $patente['lineas'] = Linea::where('patente_id' , $patente['id'])
                                                ->whereIn('recibo_id' , $recibos_id)->get();
                }
                $result['patente'] = $dbPatentes;
              }

              if ($personas_id != null){


                $dbPersonas = Persona::whereIn('id' , $personas_id)->get();
                  foreach ($dbPersonas as $dbPersona) {
                    // code...
                        $dbPersona['lineas'] = Linea::whereIn('recibo_id' , $recibos_id)->get();


                }
                $result['persona'] = $dbPersonas;
              }


              //if ($dbPersonas == null)
                  //$dbPersonas = Persona::where('id',$dbRecibos['persona_id'])->get();

          //verificar si existe pago para ese recibo
          $dbRecibos = recibo::whereIn('id' , $recibos_id)->get();
          $dbPagorecibo = Pagorecibo::where('pago_id' , $dbPago['id'])->get();
          //return $dbRecibos;
          $result['recibo'] = $dbRecibos;
          //
          //$result['lineas'] = $dbLineas;
          $result['usuario'] = $usuario;

          $result['pago'] = $dbPagorecibo;
          $result['detalle'] =  $dbPago;

          return $result;



        }else {
          return response()->json(['error'=>'no Aurorizado'],401);
         }
      }

//--------------------------------------------------------------------
      public function findLineaRecibo(Request $request){
        if($request->isJson()){

          $linea = Linea::where('recibo_id', $request->input('id'))->get();
          return $linea;

        }else {
          return response()->json(['error'=>'no Aurorizado'],401);
        }
      }
//-------------------------------------------------------------------
/*
     eliminar todas la ordenes de cobro que no esten pagadas
     y anularlar, ademas de incluir un comentario indicando que la anulo el
     sistema
*/
      public function deleteRecibo(Request $request){

        $recibos = Recibo::where('status','Pendiente')->get();

        foreach ($recibos  as $recibo) {

              Linea::where('recibo_id',$recibo['id'])->update(['status'=>'Anulada']);
              $comentario = 'Anulado por el sistema, Vencimiento de terminos';
              //$this->addComentario($recibo['id'],$comentario,$comentario,'recibo','0');

        }
        return $recibos;
      }
/*
     REPORTES DE HACIENDA

*/
public function reportePorCuenta(Request $request){
  if($request->isJson()){


  /*  $totales['declaracion'] = Linea::where('updated_at','>=',  $request['inicio'].' 00:00:00')
                                 ->where('updated_at','<=',$request['hasta'].' 23:59:59')
                                  ->where('tipo','Declaracion')
                                  ->where('status','Pagada')
                                  ->where('multa','=','0')
                                  ->sum('subTotal');
    $totales['declaracionMoroso'] = Linea::where('updated_at','>=',  $request['inicio'].' 00:00:00')
                                 ->where('updated_at','<=',$request['hasta'].' 23:59:59')
                                  ->where('tipo','Declaracion')
                                  ->where('status','Pagada')
                                  ->where('multa','>','0')
                                  ->sum('subTotal');

    $totales['tasa'] = Linea::where('updated_at','>=',  $request['inicio'].' 00:00:00')
                                 ->where('updated_at','<=',$request['hasta'].' 23:59:59')
                                  ->where('tipo','Tasa')
                                  ->where('status','Pagada')
                                  ->sum('subTotal');

    $totales['publicidad'] = Linea::where('updated_at','>=',  $request['inicio'].' 00:00:00')
                                 ->where('updated_at','<=',$request['hasta'].' 23:59:59')
                                  ->where('tipo','Publicidad')
                                  ->where('status','Pagada')
                                  ->sum('subTotal');
    $totales['catastro'] = Linea::where('updated_at','>=',  $request['inicio'].' 00:00:00')
                               ->where('updated_at','<=',$request['hasta'].' 23:59:59')
                                ->where('tipo','catastro')
                                ->where('status','Pagada')
                                ->sum('total');
    $totales['catastroInteres'] = Linea::where('updated_at','>=',  $request['inicio'].' 00:00:00')
                             ->where('updated_at','<=',$request['hasta'].' 23:59:59')
                              ->where('tipo','catastro')
                              ->where('status','Pagada')
                              ->sum('interes');
    $totales['mora'] = Linea::where('updated_at','>=',  $request['inicio'].' 00:00:00')
                               ->where('updated_at','<=',$request['hasta'].' 23:59:59')
                                ->where('status','Pagada')
                                ->sum('multa');
    $totales['catastro'] = $totales['catastro'] - $totales['catastroInteres'];
    $totales['interes'] = Linea::where('updated_at','>=',  $request['inicio'].' 00:00:00')
                               ->where('updated_at','<=',$request['hasta'].' 23:59:59')
                                ->where('status','Pagada')
                                ->sum('interes');

    $totales['descuento'] = Linea::where('updated_at','>=',  $request['inicio'].' 00:00:00')
                               ->where('updated_at','<=',$request['hasta'].' 23:59:59')
                                ->where('status','Pagada')
                                ->sum('descuento');*/

//$cuenta = Cuenta::where('id','1')->get();

//return $cuenta;

  /*  $totales['completas'] = Patenteold::join('catastros','patenteolds.id','=','catastros.patente_id')->count();
    $totales['anteriores'] = Patenteold::where('tipo','inmobiliaria')->where('id','<=','14427')->count();
    $totales['nuevas'] = Patenteold::where('tipo','inmobiliaria')->where('id','>','14427')->count();
    $totales['pagadas'] = Patenteold::join('lineas', function($join){
                                          $join->on('patenteolds.id','=','lineas.patente_id')
                                             ->where('lineas.tipo','catastro')
                                             ->where('lineas.hano','2018');
                                        })->count();*/
    return $totales;
  }else {
    return response()->json(['error'=>'no Aurorizado'],401);
  }
}
    }
