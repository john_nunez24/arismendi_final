<?php

namespace App\Http\Controllers;

use App\Unidadvalor;
use App\Unidadcobro;
use App\Departamento;
use App\Item;
use App\User;
use App\Perfil;
use App\Comentario;
use Illuminate\Http\Request;


class ConfiguracionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

   public function getUnidadCobro(){
      return Unidadcobro::where('status','Activo')->get();
   }

   public function getUnidadValor($unidad_id){
      return Unidadvalor::where('unidad_id',$unidad_id)->get();
   }

   public function getItem($departamento_id){
      $dbItems = Item::where('departamento_id',$departamento_id)->where('status','Activo')->get();

      foreach ($dbItems as $item ) {
        $item['valorActual'] =  Unidadvalor::where('unidad_id',$item['unidad_id'])->where('status','Activo')->first();
        $item['unidadCobro'] =  Unidadcobro::find($item['unidad_id']);
      }

      return $dbItems;
   }
//----------------------------john nuñez----------13-02-2020------------------------------
 /*public function newMontoUnidad($request){
   $monto = $request->input('monto');
   $unidad = $request->input('uniActual');
    $dbUnidad = new Unidadvalor;
    $dbUnidad['unidad_id'] =  $unidad['id'];
    $dbUnidad['valor'] = $monto;
    $dbUnidad['desde'] =
    $dbUnidad['hasta'] =

 }*/
//----------------------------john nuñez----------13-02-2020------------------------------
public function getUnidad($unidad_id){
   $dbUnidads = Unidadcobro::where('id',$unidad_id)->where('status','Activo')->get();

   foreach ($dbUnidads as $dbUnidad ) {
     $dbUnidad['valorActual'] =  Unidadvalor::where('unidad_id',$dbUnidad['id'])->where('status','Activo')->first();

   }

   return $dbUnidads;
}
//----------------------------john nuñez------------13-02-2020----------------------------
public function getItems(){
   $dbItems = Item::where('status','Activo')->get();

   foreach ($dbItems as $item ) {
     $item['valorActual'] =  Unidadvalor::where('unidad_id',$item['unidad_id'])->where('status','Activo')->first();
     $item['unidadCobro'] =  Unidadcobro::find($item['unidad_id']);
   }

   return $dbItems;
}
//----------------------------john nuñez----------------------------------------
public function modificarUser(Request $request){
  //return 'hola';
  $usuario = $request->input('usuario');
  $dbuser = User::where('id' ,$usuario['id'] )->first();
   if ($usuario['verificacion'] == true ){
        $dbuser['password'] = 123;
   }
       $dbuser['rif'] = $usuario['rif'];
       $dbuser['telefono'] = $usuario['telefono'];
       $dbuser['nivel'] = $usuario['nivel'];
       $dbuser['correo'] = $usuario['email'];
       $dbuser->save();

  return $dbuser;
}
//----------------------------john nuñez --------------------------------------
public function eliminarUser(Request $request){
  $comentario = $request->input('comentario');
  $user = $request->input('user');
  $usuario = $request->input('usuarioEliminar');
  $dbuser = User::where('id' ,$usuario['id'] )->first();
  $dbuser['status'] = 'Inactivo';

  $dbuser->save();

    $dbComentario = new Comentario;
    $dbComentario['comentario'] = $comentario;
    $dbComentario['user_id'] = $user['id'];
    $dbComentario['referencia_id'] = $usuario['id'];
    $dbComentario['tabla'] = 'usuario';
    $dbComentario['margen'] = 'Eliminacion de usuario del sistema';
    $dbComentario->save();
}
//----------------------------john nuñez----------------------------------------
public function todosUsuarios(){
     $dbuser = User::where('status' , 'Activo')->get();

      foreach ($dbuser as $user ) {
          $user['perfil'] = Perfil::find($user['perfil_id']);
      }
      return $dbuser;
}
//---------------------------john nuñez----------------------------------------
public function crearNewUser(Request $request){

    $usuario = $request->input('usernew');
    $dbPerfil = Perfil::where('value',$usuario['nivel'])->first();

      $dbUsuario = new User;
      $dbUsuario['rif'] = $usuario['login'];
      $dbUsuario['telefono'] = $usuario['telefono'];
      $dbUsuario['nivel'] = $usuario['nivel'];
      $dbUsuario['correo'] = $usuario['email'];
      $dbUsuario['perfil_id'] = $dbPerfil['id'];
      $dbUsuario['tipo'] = 'sistema';
      $dbUsuario['password'] = $usuario['pass'];
      $dbUsuario['status'] = 'Activo';
      $dbUsuario['confirmado'] = 1;

      $dbUsuario['api_token'] =  str_random(60);
      $dbUsuario['validate_code'] =  str_random(64);

      $dbUsuario->save();
}

//--------------------------john nuñez------------------------------------------
public function guardarItem(Request $request){

  $concepto = $request->input('concepto');
  //return $concepto;
  $newItem = new Item;
  $newItem['concepto'] = $concepto['concepto'];
  $newItem['departamento_id'] = $concepto['departamento']['id'];
  $newItem['unidad_id'] = $concepto['unidad'];
  $newItem['cantidad'] = $concepto['cantidad'];
  $newItem['user_id'] = '1';
  $newItem['periodo_id'] = '1';

  $newItem->save();
return $newItem;
  /*$dbItems = Item::where('departamento_id',$newItem['departamento_id'])->where('status','Activo')->get();

  foreach ($dbItems as $item ) {
    $item['valorActual'] =  Unidadvalor::where('unidad_id',$item['unidad_id'])->where('status','Activo')->first();
    $item['unidadCobro'] =  Unidadcobro::find($item['unidad_id']);
  }

  return $dbItems;*/

}



   public function getDepartamentos(){
      return Departamento::where('status','Activo')->get();
   }
   public function getUniTributariaHistorico(){

        return Utributaria::where('id','>','0')->orderBy('ano')->get();

   }


    //
}
