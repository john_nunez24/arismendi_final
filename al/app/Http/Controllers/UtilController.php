<?php

namespace App\Http\Controllers;

use App\Tasa;
use App\Interes;
use App\Depreciacion;
use App\Departametos;
use App\Depreciaciones;
use App\Zona;
use App\Tipologia;
use App\Valorsuelo;
use App\Alicuota;
use App\Linea;
use App\Recibo;
use Illuminate\Http\Request;
use App\Test;
use App\Salario;
use App\Actividadbase;
use App\Actividad;
use App\Patentevinculada;
use App\Patenteold;
use App\Pago;
use App\Unidadvalor;
use App\Indiceconstruccion;
use Illuminate\Support\Facades\DB;

class UtilController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {

  }
  public function reporteCatastro(Request $request){
    $fechadesde = explode("T",$request->input('desde'));
    $fechahasta = explode("T",$request->input('hasta'));
  $desde = $fechadesde[0];
  $hasta = $fechahasta[0];
$sql =  "select";
$sql .=   "  (select created_at from pagos where id = pr.pago_id) fecha,";
$sql .=    " pr.pago_id ,";
$sql .=    " (select patente from patenteolds where id = a.patente_id) catastro,";
$sql .="(select rif from personas where id = a.persona_id) rif,";
$sql .="(select nombre from personas where id = a.persona_id) rif,";

$sql .=" (select construccion from catastros where patente_id = a.patente_id) construccion,";
$sql .=" (select terreno from catastros where patente_id = a.patente_id) terreno,";
$sql .=" (select tipologia from catastros where patente_id = a.patente_id) tipologia,";
$sql .=" (select uso_id from catastros where patente_id = a.patente_id) tipologia,";
$sql .=" (select z.zona from catastros c join zonas z on z.id=c.zona_id  where c.patente_id = a.patente_id) zona,";

$sql .=" (select direccion from patenteolds where id = a.patente_id) direccion,";
$sql .=" (select monto from pagos where id = pr.pago_id) monto,";
$sql .=" (select contacto from contactos where tipo = 'telefono' and persona_id = a.persona_id limit 1) telefono,";
$sql .=" (select contacto from contactos where tipo = 'correo' and persona_id = a.persona_id limit 1) correo,";

$sql .=" a.* from pago_recibo pr JOIN ";

$sql .=" (select l.recibo_id ,l.patente_id, r.persona_id from lineas l  join recibos r on r.id = l.recibo_id where l.status='Pagada' and (l.tipo = 'Catastro' or l.tipo = 'ReciboManual')  and l.updated_at BETWEEN '".$desde."' and '".$hasta."' group by l.recibo_id,l.patente_id, r.persona_id)a on a.recibo_id = pr.recibo_id";

$results = DB::select($sql,[]);
return $results;

}

  public function test2(Request $request){
    /*$test = new Test;
  $test['date'] = $request->ip();
    $test->save();*/
  if(((date('G')>=8)&&(date('G')<=17))&&((date('N')>=1)&&(date('N')<=1)))return "esta en ley";else return "alerta";
  }
  public function test(){
    return Recibo::where('status','Pagada')->get()->lineas()->first();/*Linea::where('iano','2018')
                  ->where('imes','1')
                  ->where('actividad_id','49')
                  ->where('patente_id','14602')->get();*/
  }
  public function date(Request $request){

    $dateA['ano'] =  date('Y');
    $dateA['mes'] =  date('n');
    $dateA['dia'] =  date('j');

    return $dateA;

  }


  public function actualPayDate(Request $request){

    $dateA['ano'] =  date('Y');
    $dateA['mes'] =  date('n')-1;

    if($dateA['mes'] == 0){
      $dateA['ano'] = $dateA['ano'] -1;
      $dateA['mes'] = 12;
    }

    return $dateA;

  }
  //  Valor de la Umari vigente 19-01-2021------------------------------------
public function valorVijenteUmari(Request $request){


  {
    try {

      $uBase = Unidadvalor::where('status','Activo')->first();
      return $uBase;
    } catch (ModelNotFoundException $e) {
      return response()->json(['error'=>'no encontrado'],406);
    }
  }
}
  //-------------------------------------------------------------------------

  public function parametrosGlobales(Request $request){

    $parametro['maxHabil'] =  5;
    return $parametro;
  }




  public function diaHabil(Request $request){

    $habil = 0;
    $diahoy = date('N');
    $diames = date('j');
    $sw = true;

    while ($sw){
     if($diahoy>5){
       $diames = $diames - ($diahoy -1);
       $diahoy = 5;
     }
     if($diames <= 5){
       $sw = false;
       if ($diames>0)
         $habil = $habil + $diames;
     }else {
       $diames = $diames - $diahoy;
       $diames = $diames - 3;
       $habil = $habil + $diahoy;
       $diahoy = 5;
     }
    }
    return $habil  ;
  }

  public function tablaDepreciacion(Request $request){
    if($request->isJson()){
      try {

        $catastro = Depreciacion::where('id','>','0')->get();
        return $catastro;
      } catch (ModelNotFoundException $e) {
        return response()->json(['error'=>'no encontrado'],406);
      }
    }
  }
//-----------------------------------------------------------
public function getDepreciacion(Request $request){
  //if($request->isJson())
  {
    try {

      $depreciaciones = Depreciaciones::where('status','Activo')->get();
      return $depreciaciones;
    } catch (ModelNotFoundException $e) {
      return response()->json(['error'=>'no encontrado'],406);
    }
  }
}
//-----------------------------------------------------------------------------
public function getValorSuelo(Request $request){
  //if($request->isJson())
  {
    try {

      $valorsuelo = Valorsuelo::where('status','Activo')->get();
      return $valorsuelo;
    } catch (ModelNotFoundException $e) {
      return response()->json(['error'=>'no encontrado'],406);
    }
  }
}
//-----------------------------------------------------------------------------
public function getTipologia(Request $request){
  //if($request->isJson())
  {
    try {

      $tipologia = Tipologia::all();
      return $tipologia;
    } catch (ModelNotFoundException $e) {
      return response()->json(['error'=>'no encontrado'],406);
    }
  }
}
//-----------------------------------------------------------------------------
public function getAlicuota(Request $request){
  //if($request->isJson())
  {
    try {

      $alicuota = Alicuota::where('status','Activo')->get();
      return $alicuota;
    } catch (ModelNotFoundException $e) {
      return response()->json(['error'=>'no encontrado'],406);
    }
  }
}
//-----------------------------------------------------------------------------
public function getZona(Request $request){
  //if($request->isJson())
  {
    try {

      $zona = Zona::where('status','Activo')->get();
      return $zona;
    } catch (ModelNotFoundException $e) {
      return response()->json(['error'=>'no encontrado'],406);
    }
  }
}
//-----------------------------------------------------------

  public function getIntereses(Request $request){
    return Interes::where('ano','>=',$request->input('ano'))->orderBy('ano','desc')->orderBy('mes','desc')->get();
  }


  public function trimestre(Request $request)
    {
        $mes =  date('n')-1;
        $mes = is_null($mes) ? date('m') : $mes;
        $trim=floor(($mes) / 3)+1;
        return $trim;
    }
    //-----------------------------------------------------------------------------
    public function getSalarioActivo(Request $request){
      //if($request->isJson())
      {
        try {

          $salario = Salario::where('status','Activo')->get();
          return $salario;
        } catch (ModelNotFoundException $e) {
          return response()->json(['error'=>'no encontrado'],406);
        }
      }
    }

    //-----------------------------------------------------------------------------
    public function getActividadBase(Request $request){
      //if($request->isJson())
      {
        try {

          $aBase = Actividadbase::where('status','Activo')->get();
          return $aBase;
        } catch (ModelNotFoundException $e) {
          return response()->json(['error'=>'no encontrado'],406);
        }
      }
    }
    //---------------------john nuñez----------------------------------------------
    public function vinculacionPatentes(Request $request){
      //return $request;
      $padre = $request->input('idPadre');

      $hija = $request->input('idhija');
      $comentario = $request->input('comentario');
      $usuario = $request->input('user');
      //return 'hola';
      $dbPatentevinculada = new Patentevinculada;
      $dbPatentevinculada['patente_padre'] = $padre;
      $dbPatentevinculada['patente_hija'] = $hija;
      $dbPatentevinculada['comentario'] = $comentario;
      $dbPatentevinculada['status'] = 'Activo';
      $dbPatentevinculada['usuario_id'] = $usuario['id'];
      $dbPatentevinculada->save();
    }
//---------------------------john nuñez---------------------------------------------------
 public function getPatentesVinculadas(Request $request)
    {    // code...
      $id = $request->input('id');
      //return $request;
      $dbPatentevinculada =  Patentevinculada::where('patente_padre',$id)
                                              ->where('status' , 'Activo')->get();
      //return $dbPatentevinculada;
       $idHijos = array();
           foreach ($dbPatentevinculada as $patente) {
             // code...
             array_push($idHijos , $patente['patente_hija']);
           }
           //return $idHijos;
           $dbPatenteold = Patenteold::whereIn('id' , $idHijos)->get();
           //return $dbPatenteold;
           foreach ($dbPatenteold as $pat) {

              $tmp= Linea::where('patente_id' , $pat['id'])
                            ->where('status' , 'Pagada')
                            ->orderBy('created_at' , true)->first();

              if ($tmp <> null || $tmp <> []){
                    $pat['ultimoPago'] = Pago::where('recibo_id' , $tmp['recibo_id'])
                                          ->where('status' , 'Activa')->first();
                                        }
                                    else {
                                      $pat['ultimoPago'] = null;
                                    }
              $pat['pantentesAnteriores'] = Patentevinculada:: where('patente_hija' ,$pat['id'])
                                                        ->where('status' , 'Inactivo')->get();
           }
           return $dbPatenteold;

  }
  //---------------------john nuñez---------------------------------------------
  public function getEnumColumnValues($tabla,$campo) {

  //  $table = Indiceconstruccion;
  //  $column = tipo;
  //return $tabla;
      $type = DB::select(DB::raw("SHOW COLUMNS FROM $tabla WHERE Field = '$campo'"))[0]->Type ;

  preg_match('/^enum\((.*)\)$/', $type, $matches);

      $enum_values = [];
      foreach( explode(',', $matches[1]) as $value )
      {
        $v = trim( $value, "'" );
         array_push($enum_values, $v);
      }
      return $enum_values;
    }
//---------------------------john nunez ------------------------------------
public function eliminarVinculadas(Request $request){
  //return $request;
  $padre = $request->input('padreId');
  $hija = $request->input('hijaId');
  $comentario = $request->input('comentario');
  $usuario = $request->input('user');
    $dbPatentevinculada = Patentevinculada::where('patente_padre' , $padre)->
                                              where('patente_hija' , $hija)->
                                              where('status' , 'Activo')->first();
                //return $dbPatentevinculada;
                                            $dbPatentevinculada['status'] = 'Inactivo';
                                            $dbPatentevinculada['comentario'] = $comentario;
                                            $dbPatentevinculada['usuario_id'] = $usuario['id'];
                                            $dbPatentevinculada-> save();
}
//---------------------------john nunez ------------------------------------
public function findDepartamento(Request $request){
    return 'hola';
}
//--------------------------john nuñez ---------------------------------------
    public function getActividades(Request $request){


try {
      return Actividad::where('status','Activo')->get();
    } catch (ModelNotFoundException $e) {
      return response()->json(['error'=>'no encontrado'],406);
    }
    }
  //
}
