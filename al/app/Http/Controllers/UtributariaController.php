<?php

namespace App\Http\Controllers;

use App\Utributaria;
use Illuminate\Http\Request;


class UtributariaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      
    }

   public function getUniTributariaActiva(){

        return Utributaria::where('status','Activa')->first();

   }
   public function getUniTributariaHistorico(){

        return Utributaria::where('id','>','0')->orderBy('ano')->get();

   }


    //
}
