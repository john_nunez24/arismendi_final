<?php

namespace App\Http\Controllers;

use App\Tasa;
use App\Ingtasa;
use App\Pubtasa;
use App\Lictasa;
use App\Indiceconstruccion;
use Illuminate\Http\Request;


class TasaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

   public function index(Request $request){

    if($request->isJson()){

     return Tasa::where('status','Activa')->get();
      }
   }
// procedimiento de ingieneria ----- john nuñez---------
public function conceptoProcedimiento(Request $request){

  return  Ingtasa::where('status','Activa')->get();


}
// procedimiento de ingieneria ----- john nuñez---------
public function costosContruccion(Request $request){

  return Indiceconstruccion::where('status','Activo')->get();

}
//----------------------------------------------------------------------------
//tasas de ingieneria
   public function ingTasa(Request $request){

    if($request->isJson()){

     return Ingtasa::where('status','Activa')->get();
      }
   }
   //tasas de Publicidad
      public function pubTasa(Request $request){

       if($request->isJson()){

        return Pubtasa::where('status','Activa')->get();
         }
      }
    //
    //tasas de Licores
       public function licTasa(Request $request){

        if($request->isJson()){

         return Lictasa::where('status','Activa')->get();
          }
       }
     //

}
