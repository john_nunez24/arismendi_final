<?php

namespace App\Http\Controllers;

use App\Actividad;
use App\Linea;
use Illuminate\Http\Request;
use App\Actividadpatente;
use App\Comentario;
use App\User;
use App\Contacto;

class ActividadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

   public function getActividades(Request $request){

    if($request->isJson())
    {

     return Actividad::where('status','Activo')->get();
      }
   }

   public function asociarActividad(Request $request){

    if($request->isJson()){

       $patente = $request->input('patente_id');
       $actividad = $request->input('actividad_id');

       //$usuario = $request->input('usuario');

       $actividadPatente = new Actividadpatente;
       $actividadPatente['patente_id'] = $patente;
       $actividadPatente['actividad_id'] = $actividad;

       $actividadPatente->save();

       return $actividadPatente;

    }
   }
   //------------------------------------agregar comentario------------------------
   public function addComentario($id,$comentario,$margen,$tabla,$user_id){
     $dbComentario = new Comentario;

     $dbComentario['referencia_id'] =  $id;
     $dbComentario['comentario'] =  $comentario;

     $dbComentario['margen'] =  $margen;
     $dbComentario['tabla'] =  $tabla;
     $dbComentario['user_id'] =  $user_id;

     $dbComentario->save();
   }




   //----------------------------------------------------------------------

//codigo john----------------------------------------
   public function eliminarActividad(Request $request){
     $actividad = $request->input('actividad');
     $comentario = $request->input('comentario');
     $usuario = $request->input('usuario');
    
     $actividadPatente = $actividad['actividadPatente'];
      $acpat = Actividadpatente::find($actividadPatente['id']);

      $acpat['status'] = 'Inactivo';
      $acpat->save();

      $margen = 'Se Anula Actividad Economica #: ' . $acpat['id'] .' Referente a la Patente #: '.  $acpat['patente_id'];

     $this->addComentario($acpat['id'],$comentario,$margen,'patente',$usuario['id']);

   }

//codigo john----------------------------------------



   public function findActividades(Request $request){

    //if($request->isJson()){
       $patente = $request->input('id');

       $dbact = Actividadpatente::where('patente_id',$patente)->where('status','Activo')->get();
        $result = [];

       foreach ($dbact as $act) {

           $temp = Actividad::where('id',$act['actividad_id'])->first();
           $temp['actividadPatente'] = $act;
           $result[] = $temp;
       }
      return $result;
    }
   //}
    //
}
