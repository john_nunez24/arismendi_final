<?php

namespace App\Http\Controllers;


use App\Vehiculo;
use App\Linea;
use App\Recibo;
use App\Patenteold;
use App\Persona;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Contracts\Support\Jsonable;

class VehiculoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }


public function agregarVehiculo(Request $request){

  $vehiculo = $request->input('vehiculo');
  $persona = $request->input('persona');
  $user = $request->input('user');
  try {

    $dbPatente = Patenteold::where('patente','like', "%".$vehiculo['placa']."%")->firstOrFail();

    return response()->json(['error'=>'El numero de Placa de vehiculo '.$vehiculo['placa'].', ya existe y esta registrado a nombre de '.$dbPatente['nombre']],406);
  } catch (ModelNotFoundException $e) {

   if ($persona['id'] == null){
     //crear persona

     $dbPersona = new Persona;
     $dbPersona['nombre'] = $persona['nombre'];
     $dbPersona['rif'] = $persona['rif'];
     $dbPersona['direccion'] = $persona['direccion'];
     $dbPersona['user_id'] = $user['id'];

     $dbPersona->save();
     //return $dbPersona;
   }
      $dbPatente = new Patenteold;
      $dbPatente['patente'] = 'VE-'. $vehiculo['placa'];
      $dbPatente['nombre'] = $persona['nombre'];
      $dbPatente['rif'] = $persona['rif'];
      $dbPatente['direccion'] = $persona['direccion'];
      $dbPatente['tipo'] = 'vehiculo';
      $dbPatente['departamento_id'] = 3;
      $dbPatente['status'] = 'Activo';
      $dbPatente['persona_id'] = $dbPersona['id'];
      $dbPatente->save();

      $dbVehiculo = new Vehiculo;
      $dbVehiculo['patente_id'] = $dbPatente['id'];
      $dbVehiculo['marca'] = $vehiculo['marca'];
      $dbVehiculo['placa'] = $vehiculo['placa'];
      $dbVehiculo['modelo'] = $vehiculo['modelo'];
      $dbVehiculo['color'] = $vehiculo['color'];
      $dbVehiculo['clase'] = $vehiculo['clase'];
      $dbVehiculo['tipo'] = $vehiculo['tipo'];
      $dbVehiculo['serial'] = $vehiculo['serial'];
      $dbVehiculo['ano'] = $vehiculo['ano'];
      $dbVehiculo['uso'] = $vehiculo['uso'];
      $dbVehiculo['status'] = 'Activo';
      $dbVehiculo->save();

      return response()->json(['success'=>'Datos Guardados exitosamente'],201);
    }
}



/*public function guardarRecibo(Request $request){


    $publicidades = $request->input('publicidades');
    $ano = $request->input('ano');
    $usuario = $request->input('user');
    $patente = $request->input('patente');
    $sw = true;
    $contLineas = 0;


    //return $lineas;
    //get unidad tributarios vigente;
    $Utributaria = Utributaria::where('status','activa')->first();

    //return $lineas;
    foreach ($publicidades as $linea) {


      //verificar si existe una declaracion igual para el mismo periodo
        $qLinea = Linea::where(function($q)use($patente,$linea,$ano){
                                $q->where('patente_id',$patente['id']);
                                $q->where('actividad_id',$linea['id']);
                                $q->where('tasa_id',$linea['pubtasa_id']);
                                $q->where('iano',$ano);
                                $q->where('tipo','Publicidad');
                                $q->whereIn('status',['Pagada','Pendiente']);
                              })->get();

      if(sizeof($qLinea) == 0 ){
          if($sw){
            //crear recibo
            $dbRecibo = new Recibo;
            $dbRecibo['persona_id'] = $patente['persona']['id'];
            $dbRecibo['user_id'] = $usuario['id'];
            $dbRecibo->save();
            $sw = false;

            }

      $dbLinea = new Linea;
      $dbLinea['patente_id'] = $patente['id'];
      $dbLinea['actividad_id'] = $linea['id'];
      $dbLinea['tasa_id'] = $linea['pubtasa_id'];
      $dbLinea['base'] = $linea['subtotal'];
      $dbLinea['unidad'] = $Utributaria['base'];
      $dbLinea['subTotal'] = $linea['subtotal'];
      $dbLinea['total'] = $linea['subtotal'];
      $dbLinea['user_id'] = $usuario['id'];
      $dbLinea['tipo'] = "Publicidad";
      $dbLinea['recibo_id'] = $dbRecibo['id'];
      $dbLinea['imes']  = "1";
      $dbLinea['hmes']  = "12";
      $dbLinea['iano']  = $ano;
      $dbLinea['hano']  = $ano;
      $dbLinea->save();
      $contLineas++;
     }
    }


   if($contLineas == 0)
       return response()->json(['error'=>'Ya existe una Declaración para el año seleccionado'],406);
    else {
      return response()->json(['success'=>'Se registro la declaración correctamente'],201);
    }



}*/


}
