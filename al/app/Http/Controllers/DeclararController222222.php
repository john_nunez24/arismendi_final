<?php

namespace App\Http\Controllers;


use App\Patenteold;
use App\Pago;
use App\Linea;
use App\Persona;
use App\Recibo;
use App\Utributaria;
use App\User;
use App\Comentario;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Contracts\Support\Jsonable;
class DeclararController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }


//------------------------------------agregar comentario------------------------
public function addComentario($id,$comentario,$margen,$tabla,$user_id){
  $dbComentario = new Comentario;

  $dbComentario['referencia_id'] =  $id;
  $dbComentario['comentario'] =  $comentario;
  $dbComentario['margen'] =  $margen;
  $dbComentario['tabla'] =  $tabla;
  $dbComentario['user_id'] =  $user_id;
  $dbComentario->save();
}
//------------------------------------------------------------------------------
  public function findOrCreatePerson($patente,$user){
    try {

      $persona = Persona::where('rif',$patente['rif'])->firstOrFail();

      return $persona;
    } catch (ModelNotFoundException $e) {

      $persona = new Persona;
      $persona['nombre'] = $patente['nombre'];
      $persona['rif'] = $patente['rif'];
      $persona['direccion'] = $patente['direccion'];
      $persona['user_id'] = $user['id'];
      $persona->save();
      $patente['persona_id'] = $persona['id'];
      $patente->save();
      return $persona;
    }


  }
  //----------------------------------------------------------------------

  public function createTasa(Request $request){

    $persona = $request->input('persona');
    $lineas = $request->input('lineas');
    $usuario = $request->input('usuario');
    //return $lineas;
    //get unidad tributarios vigente;
    $Utributaria = Utributaria::where('status','activa')->first();
    //crear recibo
    $dbRecibo = new Recibo;
    $dbRecibo['persona_id'] = $persona['id'];
    $dbRecibo['user_id'] = $usuario['id'];
    $dbRecibo->save();
    //return $lineas;
    foreach ($lineas as $linea) {

      $dbLinea = new Linea;

      $dbLinea['tasa_id'] = $linea['id'];
      $dbLinea['base'] = $linea['subtotal'];
      $dbLinea['unidad'] = $Utributaria['base'];
      $dbLinea['subTotal'] = $linea['subtotal'];
      $dbLinea['total'] = $linea['subtotal'];
      $dbLinea['user_id'] = $usuario['id'];
      $dbLinea['tipo'] = $linea['tipo'];
      $dbLinea['recibo_id'] = $dbRecibo['id'];
      $dbLinea->save();

    }


    $result['recibo'] = $dbRecibo;


    return $result;




    //return $dbPatente;


  }


  //----------------------------------------------------------------------

  public function createTasaPublicidad(Request $request){

    $patente = $request->input('patente');
    $lineas = $request->input('lineas');
    $usuario = $request->input('usuario');
    //return $lineas;
    //get unidad tributarios vigente;
    $Utributaria = Utributaria::where('status','activa')->first();
    //crear recibo
    $dbRecibo = new Recibo;
    $dbRecibo['persona_id'] = $patente['persona']['id'];
    $dbRecibo['user_id'] = $usuario['id'];
    $dbRecibo->save();
    //return $lineas;
    foreach ($lineas as $linea) {

      $dbLinea = new Linea;

      $dbLinea['tasa_id'] = $linea['id'];
      $dbLinea['base'] = $linea['subtotal'];
      $dbLinea['unidad'] = $Utributaria['base'];
      $dbLinea['subTotal'] = $linea['subtotal'];
      $dbLinea['patente_id'] = $patente['id'];
      $dbLinea['iano'] = $linea['ano'];
      $dbLinea['hano'] = $linea['ano'];
      $dbLinea['imes'] = '1';
      $dbLinea['hmes'] = '12';
      $dbLinea['total'] = $linea['subtotal'];
      $dbLinea['user_id'] = $usuario['id'];
      $dbLinea['tipo'] = $linea['tipo'];
      $dbLinea['recibo_id'] = $dbRecibo['id'];
      $dbLinea->save();

    }


    $result['recibo'] = $dbRecibo;


    return $result;




    //return $dbPatente;


  }
  //----------------------------------------------------------------------

  //----------------------------------------------------------------------
  public function create(Request $request){


    $actividades = $request->input('actividad');
    $patente = $request->input('patente');
    $usuario = $request->input('usuario');
    $ltotal = $request->input('total');
    $lsub = $request->input('sub');
    $ldescuento = $request->input('descuento');
    $linteres = $request->input('interes');
    $lmulta = $request->input('multa');
    $lbase = $request->input('base');
    $fecha = $request->input('fecha');

    //buscar patente
    $dbPatente = Patenteold::find($patente['id']);

    //get unidad tributarios vigente;
    $Utributaria = Utributaria::where('status','activa')->first();




    foreach ($actividades as $acti) {
      if($lbase[$acti['id']] != null ){
        $lineaExiste =  Linea::where('iano',$fecha['iano'])
        ->where('imes',$fecha['imes'])
        ->where('actividad_id',$acti['id'])
        ->where('patente_id',$patente['id'])
        ->whereIn('status',['Pagada','Pendiente'])->get();
        if(count($lineaExiste)>0 )
        {
          return response()->json(['error'=>"Atencion, ya se encuentra un registro para la actividad economica en el periodo seleccionado"],406);
        }
      }}




      //crear recibo
      $dbRecibo = new Recibo;
      $dbRecibo['persona_id'] = $patente['persona']['id'];
      $dbRecibo['user_id'] = $usuario['id'];
      $dbRecibo->save();

      foreach ($actividades as $acti) {
        if($lbase[$acti['id']] != null ){
          $dbLinea = new Linea;
          $dbLinea['patente_id'] = $patente['id'];
          $dbLinea['actividad_id'] = $acti['id'];
          $dbLinea['base'] = $lbase[$acti['id']];
          $dbLinea['unidad'] = $Utributaria['base'];
          $dbLinea['imes'] = $fecha['imes'];
          $dbLinea['iano'] = $fecha['iano'];
          $dbLinea['hmes'] = $fecha['hmes'];
          $dbLinea['hano'] = $fecha['hano'];
          $dbLinea['descuento'] = $ldescuento[$acti['id']];
          $dbLinea['interes'] = $linteres[$acti['id']];
          $dbLinea['multa'] = $lmulta[$acti['id']];
          $dbLinea['subTotal'] = $lsub[$acti['id']];
          $dbLinea['total'] = $ltotal[$acti['id']];
          $dbLinea['alicuota'] = $acti['porcentaje'];
          $dbLinea['user_id'] = $usuario['id'];
          $dbLinea['tipo'] = 'Declaracion';
          $dbLinea['recibo_id'] = $dbRecibo['id'];

          $dbLinea->save();
        }
      }

      $result['patente'] = $dbPatente;

      return $result;

    }

    //----------------------------------------------------------------------
    public function updateDeclaracion(Request $request){


      $actividades = $request->input('actividad');
      $patente = $request->input('patente');
      $usuario = $request->input('usuario');
      $ltotal = $request->input('total');
      $lsub = $request->input('sub');
      $ldescuento = $request->input('descuento');
      $linteres = $request->input('interes');
      $lmulta = $request->input('multa');
      $lbase = $request->input('base');
      $fecha = $request->input('fecha');
      $recibo = $request->input('recibo');
      $lineas = $recibo['lineas'];
      $comentario =   $request->input('comentario');


      //get unidad tributarios vigente;
      $Utributaria = Utributaria::where('status','activa')->first();

     $margen = "Actualizar: recibo (".$fecha['recibo_id'].") ";

      foreach ($lineas as $linea){
        if($lbase[$linea['id']] != null ){
          $lineaExiste =  Linea::where('iano',$fecha['iano'])
          ->where('imes',$fecha['imes'])
          ->where('actividad_id',$linea['actividad_id'])
          ->where('patente_id',$patente['id'])
          ->where('id','<>',$fecha['id'])
          ->where('recibo_id','<>',$fecha['recibo_id'])
          ->whereIn('status',['Pagada','Pendiente'])->get();
          $margen .= $linea['actividad_id'] ." - ";
          if(count($lineaExiste)>0 )
          {
            return response()->json(['error'=>"Atencion, ya se encuentra un registro para la actividad economica en el periodo seleccionado"],406);
          }
        }
      }






        foreach ($lineas as $linea) {


          if($lbase[$linea['id']] != null ){

            $dbLinea = Linea::where('id',$linea['id'])->first();
            $margen .= " Valor Anterior: ".$dbLinea['base']." - ";
            $margen .= " Nuevo Valor: ".$lbase[$linea['id']]." - ";
            $dbLinea['base'] = $lbase[$linea['id']];
            $dbLinea['unidad'] = $Utributaria['base'];
            $dbLinea['imes'] = $fecha['imes'];
            $dbLinea['iano'] = $fecha['iano'];
            $dbLinea['hmes'] = $fecha['hmes'];
            $dbLinea['hano'] = $fecha['hano'];
            $dbLinea['descuento'] = $ldescuento[$linea['id']];
            $dbLinea['interes'] = $linteres[$linea['id']];
            $dbLinea['multa'] = $lmulta[$linea['id']];
            $dbLinea['subTotal'] = $lsub[$linea['id']];
            $dbLinea['total'] = $ltotal[$linea['id']];
            $dbLinea['user_id'] = $usuario['id'];
            $dbLinea->save();
          }
        }


        $this->addComentario($fecha['recibo_id'],$comentario,$margen,'recibo',$usuario['id']);
        return  response()->json(['ok'=>"Se actualizo el recibo correctamente"],200);


      }



//------------------------------------------------------------------------------

public function findRecibosPatente(Request $request){

   if($request->isJson()){
     $patente = $request->input('patente');
     //$tipo = $request->input('tipo');
return $patente;
     $lineas = Recibo::load('lineas')->get();




     return $lineas;
   }



}



//------------------------------------------------------------------------------



    public function findRecibos(Request $request){

      if($request->isJson()){
        $declaraciones = Recibo::where(function($q) use($request){
          if($request->input('status')!='')
           $q->where('status',$request->input('status'));
          if($request->input('id')!='')
           $q->where('persona_id', $request->input('id'));

          if($request->input('fecha')!=''){
            $fecha = explode("T",$request->input('fecha'));
            $cale = explode("-",$fecha[0]);
             $q->whereYear('updated_at',$cale[0]);
            if($request->input('tipoReporte')!=''){
              if($request->input('tipoReporte')=='mes')
                 $q->whereMonth('updated_at',$cale[1]);
              if($request->input('tipoReporte')=='dia'){
                 $q->whereMonth('updated_at',$cale[1]);
                 $q->whereDay('updated_at',$cale[2]);
               }

            }
          }
        })->orderBy('id','desc')->paginate($request->input('paginate'));
        $totalizado = 0;
        $totalizadoInteres = 0;
        $totalizadoMulta = 0;
        $totalizadoDescuento = 0;
        foreach ($declaraciones as $key => $dec) {


          $dec['lineas']= Linea::where(function($q)use($request,$dec){
                                           $q->where('recibo_id',$dec['id']);
                                         if($request->input('tipo')!='')
                                            $q->where('tipo', $request->input('tipo'));
                                         })->get();
          $dec['totalPagar'] = Linea::where(function($q)use($request,$dec){
                                           $q->where('recibo_id',$dec['id']);
                                         if($request->input('tipo')!='')
                                            $q->where('tipo', $request->input('tipo'));
                                         })->sum('total');
          $dec['totalInteres'] = Linea::where(function($q)use($request,$dec){
                                           $q->where('recibo_id',$dec['id']);
                                         if($request->input('tipo')!='')
                                            $q->where('tipo', $request->input('tipo'));
                                         })->sum('interes');
          $dec['totalMulta'] = Linea::where(function($q)use($request,$dec){
                                           $q->where('recibo_id',$dec['id']);
                                         if($request->input('tipo')!='')
                                            $q->where('tipo', $request->input('tipo'));
                                         })->sum('multa');
          $dec['totalDescuento'] = Linea::where(function($q)use($request,$dec){
                                           $q->where('recibo_id',$dec['id']);
                                         if($request->input('tipo')!='')
                                            $q->where('tipo', $request->input('tipo'));
                                         })->sum('descuento');
          $dec['totalBase'] = Linea::where(function($q)use($request,$dec){
                                           $q->where('recibo_id',$dec['id']);
                                         if($request->input('tipo')!='')
                                            $q->where('tipo', $request->input('tipo'));
                                         })->sum('base');
          $dec['tipo']= Linea::where('recibo_id',$dec['id'])->first()['tipo'];
          $dec['pago']= Pago::where('recibo_id',$dec['id'])->first();
          $dec['persona']= Persona::where('id',$dec['persona_id'])->first();
          $dec['usuario']= User::where('id',$dec['user_id'])->first();
          $dbLinea = Linea::where(function($q)use($request,$dec){
                                           $q->where('recibo_id',$dec['id']);
                                         if($request->input('tipo')!='')
                                            $q->where('tipo', $request->input('tipo'));
                                         })->first();
          if($dbLinea)
          {
            $dbPatente_id = $dbLinea['patente_id'];

          $dec['patente']= Patenteold::where('id',$dbPatente_id)->first();
          $dec['patente_linea'] = $dbLinea;
          $totalizado = $totalizado + $dec['totalPagar'];
          $totalizadoMulta = $totalizadoMulta + $dec['totalMulta'];
          $totalizadoInteres = $totalizadoInteres + $dec['totalInteres'];
          $totalizadoDescuento = $totalizadoDescuento + $dec['totalDescuento'];
          }
        }
        return $declaraciones;


      }
    }

    //----------------------------------------------------------------------

    public function createCatastro(Request $request){

      $jsPatente = $request->input('patente');
      $lineas = $request->input('lineas');
      $usuario = $request->input('usuario');

      $patente = Patenteold::where('id',$jsPatente['id'])->first();

      //----------Validar pagos anteriores

      foreach ($lineas as $linea) {

        if($linea != null ){
          $lineaExiste =  Linea::where('iano',$linea['ano'])
                            ->where('patente_id',$patente['id'])
                            ->whereIn('status',['Pagada','Pendiente'])->get();
          if(count($lineaExiste)>0 )
          {
            return response()->json(['error'=>"Atencion, ya se encuentra un registro para la fecha seleccionada"],406);
          }
        }}

        //-------------------------------




        //get unidad tributarios vigente;
        $Utributaria = Utributaria::where('status','activa')->first();
        //crear recibo
        $dbRecibo = new Recibo;
        $dbRecibo['persona_id'] = $patente['persona_id'];
        $dbRecibo['user_id'] = $usuario['id'];
        $dbRecibo->save();
        //return $lineas;
        foreach ($lineas as $linea) {

          $dbLinea = new Linea;

          $dbLinea['patente_id'] = $patente['id'];
          $dbLinea['base'] = $linea['valorCatastral'];
          $dbLinea['unidad'] = $linea['uTributaria']['base'];
          $dbLinea['subTotal'] = $linea['totalDepre'];
          $dbLinea['total'] = $linea['totalPagar'];
          $dbLinea['descuento'] = $linea['totalDescuento'];
          $dbLinea['interes'] = $linea['intereses'];
          $dbLinea['user_id'] = $usuario['id'];
          $dbLinea['imes'] = '1';
          $dbLinea['iano'] = $linea['ano'];
          $dbLinea['hmes'] = '12';
          $dbLinea['hano'] = $linea['ano'];
          $dbLinea['tipo'] = 'Catastro';
          $dbLinea['recibo_id'] = $dbRecibo['id'];

          $dbLinea->save();

        }


        $result['recibo'] = $dbRecibo;


        return $result;




        //return $dbPatente;


      }
      //----------------------------------------------------------
      public function reImprimirRecibo(Request $request){
        $idr = $request->input('recibo_id');

        $recibo = Recibo::where('id',$idr)->first();
        $lineas = Linea::where('recibo_id',$recibo['id'] )->get();

        $usuario = User::where('id',$recibo['user_id'] )->first();
        $pago = Pago::where('recibo_id',$recibo['id'])->first();
        $patente = Patenteold::where('id', $lineas[0]['patente_id'])->first();
        $persona = Persona::where('id',$recibo['persona_id'])->first();

        $result['recibo'] = $recibo;
        $result['lineas'] = $lineas;
        $result['usuario'] = $usuario;
        $result['pago'] = $pago;
        $result['patente'] = $patente;
        $result['persona'] = $persona;
        return $result;
      }
      //-------------------------------------------------------
      public function pagarRecibo(Request $request){

        if($request->isJson()){
          $pago = $request->input('pago');

          $usuario = $request->input('usuario');


          $dbRecibo = Recibo::where('id',$pago['recibo_id'])->first();
          $dbRecibo['status'] = 'Pagada';
          $dbRecibo->save();
          $toUpdateLines = Linea::where('recibo_id',$pago['recibo_id'])->get();
          foreach ($toUpdateLines as $updateLine) {
            $updateLine['status'] = 'Pagada';
            $updateLine->save();
          }

          $totalLineas = Linea::where('recibo_id',$pago['recibo_id'])->sum('total');
          $dbLineas = Linea::where('recibo_id',$pago['recibo_id'])->get();

          $dbPatente = Patenteold::where('id',$dbLineas[0]['patente_id'])->first();

          $dbPersona = Persona::where('id',$dbRecibo['persona_id'])->first();

          //verificar si existe pago para ese recibo
          $dbPago = Pago::where('recibo_id', $pago['recibo_id'])->get();
          if(count($dbPago) == 0)
          {
            //guardar pago
            $dbPago = new Pago;
            $dbPago['banco'] = $pago['banco'];
            $dbPago['referencia'] = $pago['referencia'];
            $dbPago['tipo'] = $pago['tipo'];
            $dbPago['monto'] = $totalLineas;
            $dbPago['recibo_id'] = $pago['recibo_id'];
            $dbPago['user_id'] = $usuario['id'];
            $dbPago->save();
          }else{
            $dbPago = $dbPago[0];
          }
          $result['recibo'] = $dbRecibo;
          $result['lineas'] = $dbLineas;
          $result['usuario'] = $usuario;
          $result['pago'] = $dbPago;
          $result['patente'] = $dbPatente;
          $result['persona'] = $dbPersona;
          return $result;



        }else {
          return response()->json(['error'=>'no Aurorizado'],401);
        }
      }


      public function findLineaRecibo(Request $request){
        if($request->isJson()){

          $linea = Linea::where('recibo_id', $request->input('id'))->get();
          return $linea;

        }else {
          return response()->json(['error'=>'no Aurorizado'],401);
        }
      }
      //
    }
