<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pago;
class Recibo extends Model {



    protected $fillable = ["id","persona_id", "user_id","status","comentario"];



    public static $rules = [

    ];

    public static $messages = [


    ];

    public function pago(){
      return $this->hasOne("App\Pago","recibo_id");
    }


}
