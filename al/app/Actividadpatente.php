<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Actividadpatente extends Model {
    protected $table = 'actividad_patente';
    protected $fillable = ["patente_id", "actividad_id", "status"];

    protected $dates = [];

    public static $rules = [

    ];

    public static $messages = [

    ];



}
