<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo extends Model {

    protected $fillable = ["nombre", "descripcion", "status"];

    

    public static $rules = [
        "nombre" => "required|max:60",
        "descripcion" => "required|max:255"
    ];

    public static $messages = [
      "nombre.required" => "Tipo de actividad economica es requerido",
      "nombre.max" => "Tipo de actividad economica tiene mas caracteres de los esperados",
      "descripcion.required" => "La descripción es requerida",
      "descripcion.max" => "La descripción tiene mas caracteres de los esperados",

    ];


}
