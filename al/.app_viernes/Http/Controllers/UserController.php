<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\Rule;


class UserController extends Controller
{

   const MODEL = "App\User";
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function create(Request $request){
      if($request->isJson()){
        $m = self::MODEL;
        $request['api_token'] =  str_random(60);
        $request['validate_code'] =  str_random(64);
        $request['tipo'] = "cliente";
        $request['nivel'] = 'nivel';
        $this->validate($request, $m::$rules,$m::$messages);
        $user =  User::create($request->all());
        return response()->json($request->all());
      }
      return response()->json(['error'=>'no Aurorizado'],401);
    }


    public function createSys(Request $request){
      if($request->isJson()){
        $m = self::MODEL;
        $request['api_token'] =  str_random(60);
        $request['validate_code'] =  str_random(64);
        $request['tipo'] = "sistema";
        $this->validate($request, $m::$rules,$m::$messages);
        $user =  User::create($request->all());
        return response()->json($request->all());
      }
      return response()->json(['error'=>'no Aurorizado'],401);
    }


  public function updateSys(Request $request){
      if($request->isJson()){
        $m = self::MODEL;
        $rules = $m::$rulesSys;

        $rules['rif'] = $rules['rif'].','.$request->input('id');

        $this->validate($request, $rules,$m::$messagesSys);
        $user =  User::find($request->input('id'));

        $user['rif'] = $request->input('rif');
        $user['correo'] = $request->input('correo');
        $user['telefono'] = $request->input('telefono');
        $user['nivel'] = $request->input('nivel');
        $user['status'] = $request->input('status');

        $user->save();
        return response()->json($user);
      }
      return response()->json(['error'=>'no Aurorizado'],401);
    }

  public function inactiveSys(Request $request){
        if($request->isJson()){

          $user =  User::find($request->input('id'));

          $user['status'] = 'Inactivo';

          $user->save();
          return response()->json($user);
        }
        return response()->json(['error'=>'no Aurorizado'],401);
      }


   public function indexSys(Request $request){

     if($request->isJson()){
      $user =  User::where(function($q){
         $q->where('tipo','sistema');
       })->paginate(15);
       return $user;
     }else
      return response()->json(['error'=>'no Aurorizado'],401);
  }



    public function logout(Request $request) {

      $token = $request->headers->get('token');
      $user = User::where('api_token',$token)->first();
      $user['api_token'] =  str_random(60);
      $user->save();

    }

    public function loginSystem(Request $request){

        try {
          $data = $request->all();
          $user = User::where('rif',$data['rif'])->first();
          if($user && Hash::check($data['password'],$user['password'])){
            if (( $user['status'] == 'Activo') &&($user['tipo'] == 'sistema'))
            {
              return response()->json($user,200);
            }else {
              {
                return response()->json(['error'=>'no Aurorizado'],401);
              }
            }
          }else
          return response()->json(['error'=>'no Aurorizado1'],401);

        } catch (ModelNotFoundException $e) {
          return response()->json(['error'=>'No Encontrado'],406);
        }


    }

    public function login(Request $request){
    if($request->isJson()){
        try {
          $data = $request->all();
          $user = User::where('rif',$data['rif'])->first();
          if($user && Hash::check($data['password'],$user['password'])){
            if($user['tipo'] == 'cliente')
            {
              if (( $user['status'] == 'Activo') && ( $user['confirmado'] == '1'))
              return response()->json($user,200);
              if (( $user['status'] == 'Inactivo') && ( $user['confirmado'] == '0'))
              return response()->json(['error'=>'Su usuario no ha sido activado, verifique su correo y Actívelo!'],401);
            }else {
              {
                return response()->json(['error'=>'no Aurorizado2'],401);
              }
            }
          }else
          return response()->json(['error'=>'no Aurorizado1'],401);

        } catch (ModelNotFoundException $e) {
          return response()->json(['error'=>'No Encontrado'],406);
        }

      }else {
        return response()->json(['error'=>'no Aurorizado!'],401);
      }

    }


 public function seratInit(Request $request){

   if ($request->headers->get('api_token') == '')
     return  view('serat.index');
    else
      return 'si hay';

 }

    public function activar($code){

      try {

        $user = User::where('validate_code',$code)->first();
        if($user ){
          if (( $user['status'] == 'Inactivo') && ( $user['confirmado'] == '0')){
               $user['status'] = 'Activo';
               $user['confirmado'] = '1';
               $user['api_token'] =  str_random(60);
               $user->save();

               return response()->json(['error'=>'Su Usuario a sido Activado!'],200);
           }

           if (( $user['status'] == 'Activo') && ( $user['confirmado'] == '1')){
                return response()->json(['error'=>'Su Usuario ya fue activado anteriormente, ingrese normalmente al sistema!'],200);
            }else
             return response()->json(['error'=>'Su usuario no se pudo activar!'],401);

        }else
          return response()->json(['error'=>'no Aurorizado'],401);

      } catch (ModelNotFoundException $e) {
        return response()->json(['error'=>'No Encontrado'],406);
      }


    }
    //
}
