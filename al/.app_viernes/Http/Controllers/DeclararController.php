<?php

namespace App\Http\Controllers;


use App\Patenteold;
use App\Pago;
use App\Linea;
use App\Persona;
use App\Recibo;
use App\Utributaria;
use App\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class DeclararController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    //
  }

  public function findOrCreatePerson($patente,$user){
    try {

      $persona = Persona::where('rif',$patente['rif'])->firstOrFail();

      return $persona;
    } catch (ModelNotFoundException $e) {

      $persona = new Persona;
      $persona['nombre'] = $patente['nombre'];
      $persona['rif'] = $patente['rif'];
      $persona['direccion'] = $patente['direccion'];
      $persona['user_id'] = $user['id'];
      $persona->save();
      $patente['persona_id'] = $persona['id'];
      $patente->save();
      return $persona;
    }


  }
  //----------------------------------------------------------------------

  public function createTasa(Request $request){

    $persona = $request->input('persona');
    $lineas = $request->input('lineas');
    $usuario = $request->input('usuario');
    //get unidad tributarios vigente;
    $Utributaria = Utributaria::where('status','activa')->first();
    //crear recibo
    $dbRecibo = new Recibo;
    $dbRecibo['persona_id'] = $persona['id'];
    $dbRecibo['user_id'] = $usuario['id'];
    $dbRecibo->save();
    //return $lineas;
    foreach ($lineas as $linea) {

      $dbLinea = new Linea;

      $dbLinea['tasa_id'] = $linea['id'];
      $dbLinea['base'] = $linea['subtotal'];
      $dbLinea['unidad'] = $Utributaria['base'];
      $dbLinea['subTotal'] = $linea['subtotal'];
      $dbLinea['total'] = $linea['subtotal'];
      $dbLinea['user_id'] = $usuario['id'];
      $dbLinea['tipo'] = 'Tasa';
      $dbLinea['recibo_id'] = $dbRecibo['id'];
      $dbLinea->save();

    }


    $result['recibo'] = $dbRecibo;


    return $result;




    //return $dbPatente;


  }
  //----------------------------------------------------------------------

  //----------------------------------------------------------------------
  public function create(Request $request){


    $actividades = $request->input('actividad');
    $patente = $request->input('patente');
    $usuario = $request->input('usuario');
    $ltotal = $request->input('total');
    $lsub = $request->input('sub');
    $ldescuento = $request->input('descuento');
    $linteres = $request->input('interes');
    $lmulta = $request->input('multa');
    $lbase = $request->input('base');
    $fecha = $request->input('fecha');
    //buscar patente
    $dbPatente = Patenteold::find($patente['id']);
    //-buscar o crear persona ----------------
    $dbPersona = $this->findOrCreatePerson($dbPatente,$usuario);

    if(($dbPatente['rif']=='')||($dbPatente['rif'] != $patente['rif'])){
      $dbPatente['rif'] = $patente['rif'];
      $dbPatente->save();
      $dbPersona['rif'] = $patente['rif'];
      $dbPersona->save();
    }

    if(($dbPatente['alicuota']=='')||($dbPatente['alicuota'] != $patente['alicuota'])){
      $dbPatente['alicuota'] = $patente['alicuota'];
      $dbPatente->save();
    }

    //get unidad tributarios vigente;
    $Utributaria = Utributaria::where('status','activa')->first();




    foreach ($actividades as $acti) {
      if($lbase[$acti['id']] != null ){
          $lineaExiste =  Linea::where('iano',$fecha['iano'])
                        ->where('imes',$fecha['imes'])
                        ->where('actividad_id',$acti['id'])
                        ->where('patente_id',$patente['id'])->get();
                        if(count($lineaExiste)>0 )
                        {
                          return response()->json(['error'=>"Atencion, ya se encuentra un registro para la actividad economica en el periodo seleccionado"],406);
                        }
      }}




    //crear recibo
    $dbRecibo = new Recibo;
    $dbRecibo['persona_id'] = $dbPersona['id'];
    $dbRecibo['user_id'] = $usuario['id'];
    $dbRecibo->save();

    foreach ($actividades as $acti) {
      if($lbase[$acti['id']] != null ){
        $dbLinea = new Linea;
        $dbLinea['patente_id'] = $patente['id'];
        $dbLinea['actividad_id'] = $acti['id'];
        $dbLinea['base'] = $lbase[$acti['id']];
        $dbLinea['unidad'] = $Utributaria['base'];
        $dbLinea['imes'] = $fecha['imes'];
        $dbLinea['iano'] = $fecha['iano'];
        $dbLinea['hmes'] = $fecha['hmes'];
        $dbLinea['hano'] = $fecha['hano'];
        $dbLinea['descuento'] = $ldescuento[$acti['id']];
        $dbLinea['interes'] = $linteres[$acti['id']];
        $dbLinea['multa'] = $lmulta[$acti['id']];
        $dbLinea['subTotal'] = $lsub[$acti['id']];
        $dbLinea['total'] = $ltotal[$acti['id']];
        $dbLinea['alicuota'] = $acti['porcentaje'];
        $dbLinea['user_id'] = $usuario['id'];
        $dbLinea['tipo'] = 'Declaracion';
        $dbLinea['recibo_id'] = $dbRecibo['id'];

        $dbLinea->save();
      }
    }


    $result['patente'] = $dbPatente;


    return $result;




    //return $dbPatente;


  }

  public function findRecibos(Request $request){
    if($request->isJson()){

      $declaraciones = Recibo::where(function($q) use($request){
        if($request->input('status')!='')
        $q->where('status',$request->input('status'));
        if($request->input('id')!='')
        $q->where('persona_id', $request->input('id'));
      })->orderBy('id','desc')->paginate($request->input('paginate'));

      return $declaraciones;
    }else {
      return response()->json(['error'=>'no Aurorizado'],401);
    }
  }
  //----------------------------------------------------------
  public function reImprimirRecibo(Request $request){
    $idr = $request->input('recibo_id');

    $recibo = Recibo::where('id',$idr)->first();
    $lineas = Linea::where('recibo_id',$recibo['id'] )->get();

    $usuario = User::where('id',$recibo['user_id'] )->first();
    $pago = Pago::where('recibo_id',$recibo['id'])->first();
    $patente = Patenteold::where('id', $lineas[0]['patente_id'])->first();
    $persona = Persona::where('id',$recibo['persona_id'])->first();

    $result['recibo'] = $recibo;
    $result['lineas'] = $lineas;
    $result['usuario'] = $usuario;
    $result['pago'] = $pago;
    $result['patente'] = $patente;
    $result['persona'] = $persona;
    return $result;
  }
  //-------------------------------------------------------
  public function pagarRecibo(Request $request){

    if($request->isJson()){
      $pago = $request->input('pago');

      $usuario = $request->input('usuario');


      $dbRecibo = Recibo::where('id',$pago['recibo_id'])->first();
      $dbRecibo['status'] = 'Pagada';
      $dbRecibo->save();

      $totalLineas = Linea::where('recibo_id',$pago['recibo_id'])->sum('total');
      $dbLineas = Linea::where('recibo_id',$pago['recibo_id'])->get();

      $dbPatente = Patenteold::where('id',$dbLineas[0]['patente_id'])->first();

      $dbPersona = Persona::where('id',$dbRecibo['persona_id'])->first();

      //guardar pago

      $dbPago = new Pago;

      $dbPago['banco'] = $pago['banco'];
      $dbPago['referencia'] = $pago['referencia'];
      $dbPago['tipo'] = $pago['tipo'];
      $dbPago['monto'] = $totalLineas;
      $dbPago['recibo_id'] = $pago['recibo_id'];
      $dbPago['user_id'] = $usuario['id'];
      $dbPago->save();

      $result['recibo'] = $dbRecibo;
      $result['lineas'] = $dbLineas;
      $result['usuario'] = $usuario;
      $result['pago'] = $dbPago;
      $result['patente'] = $dbPatente;
      $result['persona'] = $dbPersona;
      return $result;


    }else {
      return response()->json(['error'=>'no Aurorizado'],401);
    }
  }


  public function findLineaRecibo(Request $request){
    if($request->isJson()){

      $linea = Linea::where('recibo_id', $request->input('id'))->get();
      return $linea;

    }else {
      return response()->json(['error'=>'no Aurorizado'],401);
    }
  }
  //
}
