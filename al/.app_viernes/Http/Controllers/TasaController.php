<?php

namespace App\Http\Controllers;

use App\Tasa;
use Illuminate\Http\Request;


class TasaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

   public function index(Request $request){

    if($request->isJson()){

     return Tasa::where('status','Activa')->get();
      }
   }


    //
}
