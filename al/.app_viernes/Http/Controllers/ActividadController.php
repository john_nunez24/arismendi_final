<?php

namespace App\Http\Controllers;

use App\Actividad;
use Illuminate\Http\Request;
use App\Actividadpatente;

class ActividadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

   public function getActividades(Request $request){

    if($request->isJson()){

     return Actividad::where('status','Activo')->get();
      }
   }

   public function asociarActividad(Request $request){

    if($request->isJson()){

       $patente = $request->input('patente_id');
       $actividad = $request->input('actividad_id');
       //$usuario = $request->input('usuario');

       $actividadPatente = new Actividadpatente;
       $actividadPatente['patente_id'] = $patente;
       $actividadPatente['actividad_id'] = $actividad;

       $actividadPatente->save();

       return $actividadPatente;

    }
   }
   public function findActividades(Request $request){

    if($request->isJson()){
       $patente = $request->input('id');
       $dbact = Actividadpatente::where('patente_id',$patente)->get();
        $result = [];

       foreach ($dbact as $act) {
      $result[] = Actividad::where('id',$act['actividad_id'])->first();

       }
return $result;
    }
   }
    //
}
