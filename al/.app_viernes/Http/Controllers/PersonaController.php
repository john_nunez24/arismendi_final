<?php

namespace App\Http\Controllers;

use App\Persona;
use App\User;
use App\Patenteold;
use App\Contacto;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class PersonaController extends Controller
{

  const MODEL = "App\Persona";
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    //
  }


  public function findPersona(Request $request){
    if($request->isJson()){

      return Persona::where(function($q)use($request){
        $param = $request->all();
        if($param['rif'] != "")
        $q->where('rif','like', "%".$param['rif']."%");
        if($param['id'] != "")
        $q->where('id',$param['id']);
      })->paginate(10);
    }else {
      return response()->json(['error'=>'no Aurorizado'],401);
    }
  }


  public function findPersonaOnly(Request $request){
    if($request->isJson()){
      try {
        $param = $request->all();

        $result =  Persona::where('rif','like', "%".$param['rif']."%")->firstOrFail();
        return $result;

      } catch (ModelNotFoundException $e) {
        return response()->json(['error'=>'No Encontrado'],404);
      }
      }else {

      return response()->json(['error'=>'no Aurorizado'],401);
    }
  }

  public function create(Request $request){
    $m = self::MODEL;


    $request['user_id'] = Auth::user()['id'];
    $this->validate($request, $m::$rules,$m::$messages);
    $persona =  Persona::create($request->all());

    if( $request->input('telefono') != ""){
      $contacto = new Contacto;
      $contacto->contacto = $request->input('telefono');
      $contacto->tipo = 'telefono';
      $contacto->persona_id = $persona['id'];
      $contacto->save();
      $persona['telefono'] = $contacto;
    }
    if( $request->input('correo') != ""){
      $contacto1 = new Contacto;
      $contacto1->contacto = $request->input('correo');
      $contacto1->tipo = 'correo';
      $contacto1->persona_id = $persona['id'];
      $contacto1->save();
      $persona['correo'] = $contacto1;
    }



    return response()->json($request->all());
  }
  //----------------------------------------------------------------
  public function index($token){
    try {

      $user = User::where('api_token',$token)->first();
      $persona = $user->persona()->firstOrFail();//$user->persona();
      $persona['rif'] = $user['rif'];
      $persona['telefono'] = Contacto::where('persona_id',$persona['id'])->where('tipo','telefono')->first();
      $persona['correo'] = Contacto::where('persona_id',$persona['id'])->where('tipo','correo')->first();
      return response()->json($persona,200);

    } catch (ModelNotFoundException $e) {
      return response()->json(['error'=>'no encontrado'],404);
    }
  }
  //----------------------------------------------------------------
  public function edit(Request $request){
    try {

      $user = User::where('api_token',$request->input('api_token'))->first();
      $persona = $user->persona()->firstOrFail();//$user->persona();
      $persona['nombre'] = $request->input('nombre');
      $persona['fecha'] = $request->input('fecha');
      $persona['tipo'] = $request->input('tipo');
      $persona['direccion'] = $request->input('direccion');
      $persona['direccion2'] = $request->input('direccion2');
      $persona['calle'] = $request->input('calle');
      $persona['zona'] = $request->input('zona');
      $persona['ciudad'] = $request->input('ciudad');
      $persona->save();

      $telefono = Contacto::find($request->input('telefono')['id']);
      $telefono['contacto'] = $request->input('telefono')['contacto'];
      $telefono->save();

      $correo = Contacto::find($request->input('correo')['id']);
      $correo['contacto'] = $request->input('correo')['contacto'];
      $correo->save();

      $persona['rif'] = $user['rif'];

      //  $persona['rif'] = 'holi';
      $persona['contactos'] = Contacto::where('persona_id',$persona['id'])->get();//$persona->contactos()->get();
      return response()->json($persona,200);


    } catch (ModelNotFoundException $e) {
      return response()->json(['error'=>'no encontrado'],404);
    }
  }


  public function findOrCreatePerson(Request $request){
    if($request->isJson()){
      try {

        $persona = $request->input('persona');

        $user= $request->input('usuario');

        $personaFind = Persona::where('rif',$persona['rif'])->firstOrFail();

        return $persona;
      } catch (ModelNotFoundException $e) {

        $personaNew = new Persona;
        $personaNew['nombre'] = $persona['nombre'];
        $personaNew['rif'] = $persona['rif'];
        $personaNew['direccion'] = $persona['direccion'];
        $personaNew['user_id'] = $user['id'];
        $personaNew->save();

        if( $persona['telefono'] != ""){
          $contacto = new Contacto;
          $contacto['contacto'] = $persona['telefono'];
          $contacto['tipo'] = 'telefono';
          $contacto['persona_id'] = $personaNew['id'];
          $contacto->save();

        }
        if( $persona['correo'] != ""){
          $contacto1 = new Contacto;
          $contacto1['contacto'] = $persona['correo'];
          $contacto1['tipo'] = 'correo';
          $contacto1['persona_id'] = $personaNew['id'];
          $contacto1->save();
        }
        return $personaNew;
      }
    }

  }
  //
}
