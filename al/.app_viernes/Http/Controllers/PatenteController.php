<?php

namespace App\Http\Controllers;
use App\Persona;
use App\User;
use App\Patente;
use App\Direccion;
use App\Patenteold;
use App\Tipo;
use Illuminate\Http\Request;

class PatenteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

   public function findPatente(Request $request){

     return Patenteold::where(function($q)use($request){
             $param = $request->all();
             if($param['tipo'] != ""){
               if($param['tipo']==="todo")
                  $q->where('id','>','0');
                 else
                  $q->where('tipo', $param['tipo']);
             }
            if($param['nombre'] != "")
               $q->where('nombre','like', "%".$param['nombre']."%");
            if($param['patente'] != "")
               $q->where('patente','like', "%".$param['patente']."%");
            if($param['direccion'] != "")
               $q->where('direccion','like', "%".$param['direccion']."%");

           })->paginate(10);
   }


    public function index($token){
      try {

          $user = User::where('api_token',$token)->first();
          $persona = $user->persona()->firstOrFail();
          $patentes = Patente::where('persona_id',$persona['id'])->get();
          foreach ($patentes as $patente) {
               $patente['tDetalle'] = Tipo::find($patente['tipo_id']);
               $patente['direccion'] = Direccion::find($patente['direccion_id']);
           }
          return response()->json($patentes,200);

      } catch (ModelNotFoundException $e) {
        return response()->json(['error'=>'no encontrado'],404);
      }
    }

    //
}
