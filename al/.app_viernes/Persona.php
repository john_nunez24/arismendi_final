<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Patente;


class Persona extends Model {

    protected $fillable = ["nombre",  "postal", "calle", "tipo", "zona", "direccion", "ciudad", "direccion2","user_id"];

    protected $dates = [];

    public static $rules = [
        "nombre" => "required",
        "calle" => "required",
        "tipo" => "required",
        "zona" => "required",
        "direccion" => "required",
        "ciudad" => "required",

    ];

    public static $messages = [
       'nombre.required' => 'La Razón Social y/o nombre son requeridos',
      // 'rif.required' => 'El Rif es requerido',
       'correo.required' => 'El correo es requerido',
       'calle.required' => 'La calle/Avenida es requerido',
       'tipo.required' => 'El tipo de persona es requerido',
       'zona.required' => 'La Zona/Sector/Urbanizacion es requerido',
       'direccion.required' => 'La Direccion es requerida',
       'ciudad.required' => 'La Ciudad/Pueblo/Localidad es requerida',

      // 'rif.unique' => 'El RIF ingresado ya se encuentra registrado',


    ];

    // Relationships
    public function user(){
        return $this->belongsTo('App\Persona');
    }

    public function contactos(){
      return hasMany('App\Contacto', 'persona_id');
    }

    public function patente(){
      return hasMany('App\Patente', 'persona_id');
    }
}
