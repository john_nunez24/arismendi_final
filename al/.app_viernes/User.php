<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;


class User extends Model implements AuthenticatableContract, AuthorizableContract
{
  use Authenticatable, Authorizable;

  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
    'rif','correo','telefono',  'password', 'api_token','validate_code','nivel','tipo','status'
  ];

  /**
  * The attributes excluded from the model's JSON form.
  *
  * @var array
  */
  protected $hidden = [
    'password','remember_token','status','validate_code','confirmado','created_at','updated_at'
  ];

  public static $rules = [
    "rif" => "required|unique:users,rif",
    "correo" => "required|max:60",
    "telefono" => "required|max:15",
    "password" => "required"
  ];



  public static $messages = [
    "rif.required" => "El RIF es un campo Obligatorio.",
    "rif.unique" => "El RIF indicado ya se encuentra registrado.",
    "correo.required" => "El correo es Obligatorio.",
    "password.required" => "La clave es Oblitoria." ,
    "telefono.required" => "El numero de telefono es Obligatorio.",
    "telefono.max" => "El teléfono tiene mas caracteres de los esperados.",
    "correo.max" => "El Correo tiene mas caracteres de los esperados"
  ];

  public static $rulesSys = [
    "rif" => "required|unique:users,rif",
    "correo" => "required|max:60"
  ];

  public static $messagesSys = [
    "rif.required" => "El usuario es un campo Obligatorio.",
    "rif.unique" => "El usuario indicado ya se encuentra registrado.",

  ];

  public function setPasswordAttribute($password)
  {
    $this->attributes['password'] =  (new BcryptHasher)->make($password);
  }

  public function setApiTokenAttribute()
  {
    $this->attributes['api_token'] =  str_random(60);
  }

  public function persona(){
      return $this->hasMany('App\Persona','user_id');
  }
}
