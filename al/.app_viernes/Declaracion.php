<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Declaracion extends Model {

    protected $table = "declaraciones";

    protected $fillable = ["monto", "imes", "iano",
                           "hmes","hano","descuento",
                           "interes","total","alicuota",
                           "multa","subTotal"];



    public static $rules = [

    ];

    public static $messages = [


    ];




}
