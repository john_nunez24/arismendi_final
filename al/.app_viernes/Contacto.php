<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacto extends Model {

    protected $fillable = ["contacto", "tipo","persona_id"];



    public static $rules = [
        "contacto" => "required",
        "tipo" => "required",

    ];

    public static $messages = [
      "contacto.required" => "El dato de contacto es requerido",
      "tipo.required" => "El tipo del contacto es requerido",

    ];

 public function persona(){
   return $this->belongsTo("App\Persona");
 }


}
